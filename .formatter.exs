# Used by "mix format"
[
  import_deps: [:open_api_spex],
  inputs: ["mix.exs", "{config,lib,test}/**/*.{ex,exs}"]
]
