<a name="1.7.3"></a>
## [1.7.3] - 2023-07-26

### Feat
- handle deletion when jobs are on error
- directly delete worker when stopping live
- update log levels
- pass logs from plug.logger as debug
- duplicate workflow with new event
- add workflow start parameters
- live_workflow_processing metrics
- set live workflow to processing when all jobs are processing

### Fix
- step trigger without completion of the previous one
- allow step failure for multiple inputs
- change status ordering by status id
- handle live restart from env var
- compute processing gauges on startup

<a name="1.7.2"></a>
## [1.7.2] - 2023-05-09
### Ci
- update RMQ service version
- Set credo to v1.6.7

### Feat
- aggregate workflow metrics by status
- aggregate job metrics by status
- status and progressions use millisecond datetime
- set persistence to publish messages to AMQP queues

### Fix
- delete deprecated workflow metric
- update metric description
- Handle post on notif properly
- No right needed to get workflow status
- required path keep first file work subdirectory if any
- forbid workflow creation with filenames containing whitespaces

### Refactor
- conform to metric naming conventions
- change labels for workflow instrumenter

<a name="1.7.1"></a>
## [1.7.1] - 2023-02-24
### Fix
- change query method to avoid crash with two durations
- add casing to handle old conditions for notification hooks

<a name="1.7.0"></a>
## [1.7.0] - 2023-02-13

### Feat
- add notification templates
- add workflow mode to step management
- allow failure for child workflow
- adding nested workflows
- update notification hook

### Fix
- wrong job id fetch causing backend crash
- prevent redundant triggers notification hooks when parallel jobs complete or error
- add limit to get_job_duration when multiple jobs durations for one job
- update route
- disallow error on job completed
- add protection around invalid notification hooks
- add default value around ip check live worker to protect from backend crash
- fix blacklist add and delete messages
- fix blacklist sync and get messages definition

### Refactor
- refactor job launching

### Chores
- cleaning legacy code

### Ci
- add doc check step

### Docs
- version exposition in doc and missing doc

<a name="1.6.1"></a>
## [1.6.1] - 2022-11-03
### Docs
- document api endpoints
- add open api schemas
- use open api spex instead of phoenix swagger

### Feat
- add the mvc structure for notification endpoints

### Fix
- change last status in jobs so that error and completed status have higher priorities than the rest
- correct retrieve status for notification hooks that was causing producing too many notifications
- add a distinct on the job duration query
- check if wildcard is present and not only one in list
- add protection around live jobs without workers
- remove jobs deletion on parent workflow logical deletion
- error to warning without live worker
- add nil progressions case for job duration

### Reorganisation
- rename notifications package and modules to notification_hooks
- move QueryFilter module to models package
- move step and statistics modules to new controllers package
- move table definition modules to new models package
- controllers become web_controllers
- move utility functions to specific controller


<a name="1.6.0"></a>
## [1.6.0] - 2022-09-13
### Ci
- add deps security audit
- bump postgres version and add postgres 12.11 test

### Docs
- document state machines
- put workflow state machine

### Feat
- bump to 1.6.0
- more explicit method name
- notification_manager
- views test
- updated schema:data for backend views
- Credentials AuthToken + tests completed/error
- notification_hook implementation + migrations
- change schema version from 1.9 to 1.10
- Be able to put a workflow in a delete mode
- add job status transition enforcement
- add job listing by id

### Fix
- error to warning without live worker
- add nil progressions case for job duration
- cast average duration as float for psql version compat
- put status to db after status transition check
- status change causing reject go through dlx
- order among group
- rebase problem
- remove requirements generation from destination_paths
- do not generate requirements in live mode
- retry job set job to queued status
- harmonize templated destination path generation
- error handling in live workflows
- live error status management
- send error if trying to update non-existing worker
- live worker deletion
- ignore transition from some state to same state
- typo for complete live job position
- add initialized status as deprecated
- queued state transition for live
- add nil queued and skipped transitions
- reintroduce running state as deprecated
- processing to processing transition
- enforce state machine in controller
- error handling in consumer for job state transition
- add a secure check to avoid crash while decoding
- simplify workflow creation payload return
- add job error to error state transition
- retrieve workflow durations from multiple workflow IDs

### Mix
- add dependencies audit step in checks alias

### Refactor
- split list job in internal and default

### Reorganisation
- rename notifications package and modules to notification_hooks
- move QueryFilter module to models package
- move step and statistics modules to new controllers package
- move table definition modules to new models package
- controllers become web_controllers
- move utility functions to specific controller

### Test
- test all possible case of templating with calculable string
- test job status transition enforcement
- exemple of internal list job use
- add test for jobs api
- set processing status to Jobs on setting progression to 0


<a name="1.5.0"></a>
## [1.5.0] - 2022-05-11
### Feat
- bump to 1.5.0
- bump credo to 1.6
- add data in teams error hook
- add changelog
- add changelog

### Fix
- add unicity constraint on workflow definition loading


<a name="1.4.1"></a>
## [1.4.1] - 2022-02-22
### Feat
- bump to 1.4.1
- bump to 1.4.1-rc1


<a name="1.4.0"></a>
## [1.4.0] - 2022-01-25
### Feat
- bump to 1.4.0
- bump to 1.4.0-rc4
- bump to 1.4.0-rc3
- restart live workflow when error
- add live workflow retry when error
- bump to 1.4.0-rc1
- add unused flag to allow role deletion
- bump workflow schema to 1.9
- add get roles functions
- add roles and rights as structure
- add allow failure to job

### Fix
- add wildcarded entities in right check
- allow failure routine fix
- fix credo filter piping
- correct merge conflicts
- adapt to new role structure
- add step manager in error consumer
- not consuming progression messages when job in error

### Test
- add test for get and delete roles
- modify test to adapt to role fetching from user
- new role structure
- allow failure to job


<a name="1.3.1"></a>
## [1.3.1] - 2021-12-16

<a name="1.3.0"></a>
## [1.3.0] - 2021-10-25
### Feat
- bump to 1.3.0-rc
- add uuid in workflow rendering
- add user uidd in description of job retry
- add existence check of uuid
- add user uuid to workflow structure

### Fix
- call step manager in worker consumer only for live jobs

### Test
- add uuid in render tests
- add tests for user uuid in retry
- check existence of uuid
- test user uuid on workflow structure


<a name="1.2.0"></a>
## [1.2.0] - 2021-08-27
### Feat
- bump version to 1.2.0
- trigger notification if job in error and add teams support
- launch workflow with given version

### Fix
- correct completed live workflow status
- fix non functional array paramater templatisation
- skip nil empty and blank source paths
- skip nil empty and blank source paths


<a name="1.1.0"></a>
## [1.1.0] - 2021-07-21
### Feat
- get workflow/:id full or simple
- get workflow/:id full or simple
- add stop live workflows in api

### Fix
- consume malfomed messages

### Refactor
- decrease complexity


<a name="1.0.0"></a>
## [1.0.0] - 2021-06-29
### CI
- add publish stage
- update Elixir pipeline path

### Feat
- default right_action is view
- add mix version target
- bump to 1.0.0-rc2
- add live workers endpoint to swagger
- add transitory status to avoid resend messages
- add stopped status and message consumption
- add stop to job
- add worker response exchange
- live support in notification
- add notifications for live
- handle ip palceholder replacement
- add live worker structure update at wm completion
- migration for live worker table
- live worker table
- query job by direct message queue name
- publish message with a user define exchange
- new sdk response consumer queues
- uid queue in stepflow and not in worker manager
- split create and update
- update view
- allow multiple parameters to be updated at once
- update parameters functions
- migration for update structure
- new migration
- add live support
- api endpoint for update
- update structure in database
- add update to status
- add is updatable field and update structure
- add message filtering
- new migration
- add live support
- add step live treatment
- add live support
- add get step parameter
- add live steps
- add get action and new statuses
- add get by function to get job
- add is live fields
- add is live boolean to workflow definition
- statistics with pending status
- search definitions with simple string

### Fix
- use id to sort statuses instead of timestamps
- use hex publish
- deletion policy for workflow status
- remove test for endpoint not existing anymore
- fix missing relations between tables for clean deletion
- handle correctly headers in amqp message

### Refacto
- remove one comment for credo

### Refactor
- split queue creation to own function
- decrease cc by splitting

### Style
- remove not needed imports
- modify level log
- format and clean

### Test
- correct tests
- stopped consumer
- end to end live workflow
- live worker
- test for list job queries
- use publish instead of consumer
- take into account changes by evolution
- new consumer queues
- test view
- update test inside workflow
- base test for update structure
- live workflow test
- test new statuses
- add pendign status


<a name="0.2.10"></a>
## [0.2.10] - 2021-04-07
### Feat
- support date and datetime string
- filter workflows based on workflow status state
- simple and full view of workflow definition
- update definitions endpoint
- check metrics enabled before incrementing metrics
- optional parameter 'identifiers' for /workflow/statistics
- workflows statistics endpoint v2
- workflow and job metrics
- workflow metrics
- increase readability error workflow definition
- workflow status represents its lifecycle
- optimize pattern matching to select progression 0
- create workflow status logic

### Test
- test increments of metrics


<a name="0.2.9"></a>
## [0.2.9] - 2021-03-09
### Fix
- setup metric controller


<a name="0.2.8"></a>
## 0.2.8 - 2021-03-08
### Ci
- fix workflow_definitions_test
- remove workflows creating bugs in test

### Feat
- check metrics enabled in StepFlow plug
- update elixir and otp
- set metrics at runtime
- add right in workflow view
- create right view
- enable metrics either with 'true' or true
- format code
- dont setup exporter when metrics are not enabled
- create a collector for workflow metrics
- ensure endpoint is managed by login
- add prometheus metrics number/duration of workflow per identifier
- send job in queue not found to error
- add epoch to keywords
- add progression in status
- preload status and progressions in start next
- add retry in queued status count
- add processing count
- add function to return forbidden access
- add tests on rights
- optimize sql query and remove standards
- add rights usage to workflow and definitions
- parse and load in bdd the workflow rights
- remove parameters of launch_workflow endpoint response
- drop worker_definitions then recreate it with parameters as map
- set worker_definitions.parameters as :map
- add source_paths drop from message
- add flush for never ending message into timeout queues
- add conditional workflows step possibility with elixir syntax

### Fix
- upgrade to ubuntu:20.04
- downgrade to otp 23.0
- check wether config exists
- filter param map with replace instead of creating new one
- correct loopholes in case management
- add flush for never ending message into timeout queues

### Hotfix
- retry a message with no queue has no progression

### Refactor
- take into account pr with queue not found
- less complexity
- reduce complexity by function splitting

### Style
- clarity
- move connexion closure
- clean
- clean code

### Test
- exclude elixir 1.10 with otp 23.0
- add share mode + explicit connection checkout for database
- test workflow view
- test right view
- add test for template with special parameters
- test for queue not found to job error
- collect workflow collector
- remaining test from merge updated
- modification of queue to take into account new queue functionning
- add count status function
- remove useless calls
- add retry step test
- add retry job macro
- generic create progression
- add macro to add progression
- update test for progression

[Unreleased]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.7.3...HEAD
[1.7.2]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.7.2...1.7.3
[1.7.2]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.7.1...1.7.2
[1.7.1]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.7.0...1.7.1
[1.7.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.6.1...1.7.0
[1.6.1]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.6.0...1.6.1
[1.6.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.5.0...1.6.0
[1.5.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.4.1...1.5.0
[1.4.1]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.4.0...1.4.1
[1.4.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.3.1...1.4.0
[1.3.1]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.3.0...1.3.1
[1.3.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.2.0...1.3.0
[1.2.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.1.0...1.2.0
[1.1.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/1.0.0...1.1.0
[1.0.0]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/0.2.10...1.0.0
[0.2.10]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/0.2.9...0.2.10
[0.2.9]: https://gitlab.com/media-cloud-ai/backend/ex_step_flow/compare/0.2.8...0.2.9
