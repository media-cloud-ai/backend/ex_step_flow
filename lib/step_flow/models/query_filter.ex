defmodule StepFlow.QueryFilter do
  @moduledoc false

  import Ecto.Query, warn: false

  def filter_query(query, params, key, field) do
    case StepFlow.Map.get_by_key_or_atom(params, key) do
      nil ->
        query

      value ->
        from(item in query, where: field(item, ^field) == ^value)
    end
  end

  def filter_query(query, params, key) do
    filter_query(query, params, key, key)
  end

  def filter_query_with_related_entry(
        query,
        params,
        key,
        query_item_identifier,
        entity_type,
        entity_field,
        entity_identifier
      ) do
    case StepFlow.Map.get_by_key_or_atom(params, key) do
      nil ->
        query

      values ->
        from(
          query_item in query,
          join:
            item in subquery(
              from(
                item in entity_type,
                order_by: [
                  desc: item.id,
                  desc: field(item, ^entity_identifier)
                ],
                distinct: [
                  desc: field(item, ^entity_identifier)
                ]
              )
            ),
          on: field(query_item, ^query_item_identifier) == field(item, ^entity_identifier),
          where: field(item, ^entity_field) in ^values
        )
    end
  end

  def apply_end_date_filter(query, params, key) do
    case StepFlow.Map.get_by_key_or_atom(params, key) do
      nil ->
        query

      date_value ->
        datetime =
          case NaiveDateTime.from_iso8601(date_value) do
            {:ok, date} ->
              date

            _ ->
              NaiveDateTime.new!(
                Date.from_iso8601!(date_value),
                Time.new!(23, 59, 59, 999_999)
              )
          end

        from(item in query, where: fragment("?::timestamp", item.inserted_at) <= ^datetime)
    end
  end

  def apply_start_date_filter(query, params, key) do
    case StepFlow.Map.get_by_key_or_atom(params, key) do
      nil ->
        query

      date_value ->
        datetime =
          case NaiveDateTime.from_iso8601(date_value) do
            {:ok, date} ->
              date

            _ ->
              NaiveDateTime.new!(
                Date.from_iso8601!(date_value),
                Time.new!(0, 0, 0)
              )
          end

        from(item in query, where: fragment("?::timestamp", item.inserted_at) >= ^datetime)
    end
  end
end
