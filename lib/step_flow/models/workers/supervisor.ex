defmodule StepFlow.Workers.Supervisor do
  require Logger
  use Supervisor

  @moduledoc false

  @doc false
  def child_spec(_) do
    %{
      id: StepFlow.Workers.Supervisor,
      start: {StepFlow.Workers.Supervisor, :start_link, []},
      type: :supervisor
    }
  end

  @doc false
  def start_link do
    Logger.info("#{__MODULE__} start_link")
    Supervisor.start_link(__MODULE__, [], name: __MODULE__)
  end

  @doc false
  def init(_) do
    Logger.info("#{__MODULE__} init")

    children = [
      StepFlow.Workers.WorkerStatusWatcher
    ]

    Supervisor.init(children, strategy: :one_for_one)
  end
end
