defmodule StepFlow.WorkflowDefinitions.WorkflowDefinition do
  @moduledoc """
  The WorkflowDefinition context.
  """
  use Ecto.Schema
  import Ecto.Changeset

  require Logger
  alias StepFlow.WorkflowDefinitions.WorkflowDefinition

  schema "step_flow_workflow_definition" do
    field(:schema_version, :string)
    field(:identifier, :string)
    field(:label, :string, default: "")
    field(:icon, :string, default: "")
    field(:version_major, :integer)
    field(:version_minor, :integer)
    field(:version_micro, :integer)
    field(:tags, {:array, :string}, default: [])
    field(:is_live, :boolean, default: false)
    field(:deleted, :boolean, default: false)
    field(:notification_hooks, {:array, :map})
    field(:steps, {:array, :map}, default: [])
    field(:start_parameters, {:array, :map}, default: [])
    field(:parameters, {:array, :map}, default: [])

    timestamps()
  end

  @doc false
  def changeset(%WorkflowDefinition{} = workflow_definition, attrs) do
    workflow_definition
    |> cast(attrs, [
      :schema_version,
      :identifier,
      :label,
      :icon,
      :version_major,
      :version_minor,
      :version_micro,
      :tags,
      :is_live,
      :deleted,
      :notification_hooks,
      :steps,
      :start_parameters,
      :parameters
    ])
    |> validate_required([
      :schema_version,
      :identifier,
      :version_major,
      :version_minor,
      :version_micro
    ])
    |> unique_constraint(
      :identifier,
      name: :workflow_identifier_index
    )
  end
end
