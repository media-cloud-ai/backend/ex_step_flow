defmodule StepFlow.Roles.Role do
  use Ecto.Schema
  import Ecto.Changeset

  require Logger
  alias StepFlow.Rights.Right
  alias StepFlow.Roles.Role

  @moduledoc false

  @derive {Jason.Encoder, only: [:name, :rights, :is_unused]}
  schema "step_flow_roles" do
    field(:name, :string)
    field(:rights, {:array, Right}, default: [])
    field(:is_unused, :boolean, default: true)

    timestamps()
  end

  @doc false
  def changeset(%Role{} = role, attrs) do
    role
    |> cast(attrs, [
      :name,
      :rights,
      :is_unused
    ])
    |> validate_required([:name, :rights])
  end
end
