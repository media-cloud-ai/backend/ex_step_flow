defmodule StepFlow.Jobs.Job do
  use Ecto.Schema
  import Ecto.Changeset
  alias StepFlow.Jobs.Job
  alias StepFlow.Jobs.Status
  alias StepFlow.LiveWorkers.LiveWorker
  alias StepFlow.Progressions.Progression
  alias StepFlow.Updates.Update
  alias StepFlow.Workflows.Workflow

  @moduledoc false

  schema "step_flow_jobs" do
    field(:name, :string)
    field(:step_id, :integer)
    field(:parameters, {:array, :map}, default: [])
    field(:is_live, :boolean, default: false)
    field(:is_updatable, :boolean, default: false)
    field(:allow_failure, :boolean, default: false)
    field(:deleted, :boolean, default: false)
    field(:in_black_list, :boolean, default: false)
    belongs_to(:workflow, Workflow, foreign_key: :workflow_id)
    has_many(:status, Status, on_delete: :delete_all)
    has_many(:progressions, Progression, on_delete: :delete_all)
    has_many(:updates, Update, on_delete: :delete_all)
    has_one(:live_worker, LiveWorker, on_delete: :delete_all)
    has_one(:child_workflow, Workflow, foreign_key: :parent_id, on_delete: :delete_all)
    field(:last_worker_instance_id, :string, default: "")

    timestamps()
  end

  @doc false
  def changeset(%Job{} = job, attrs) do
    job
    |> cast(attrs, [
      :name,
      :step_id,
      :parameters,
      :is_live,
      :is_updatable,
      :allow_failure,
      :in_black_list,
      :deleted,
      :last_worker_instance_id,
      :workflow_id
    ])
    |> foreign_key_constraint(:workflow_id)
    |> validate_required([:name, :step_id, :parameters, :workflow_id])
  end
end
