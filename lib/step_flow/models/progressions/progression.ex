defmodule StepFlow.Progressions.Progression do
  use Ecto.Schema
  import Ecto.Changeset
  alias StepFlow.Jobs.Job
  alias StepFlow.Progressions.Progression

  @moduledoc false

  schema "step_flow_progressions" do
    field(:datetime, :utc_datetime_usec)
    field(:docker_container_id, :string)
    field(:progression, :integer)
    field(:system_info, :map)
    belongs_to(:job, Job, foreign_key: :job_id)

    timestamps()
  end

  @doc false
  def changeset(%Progression{} = progression, attrs) do
    progression
    |> cast(attrs, [:datetime, :docker_container_id, :system_info, :job_id, :progression])
    |> foreign_key_constraint(:job_id)
    |> validate_required([:datetime, :docker_container_id, :job_id, :progression])
  end
end
