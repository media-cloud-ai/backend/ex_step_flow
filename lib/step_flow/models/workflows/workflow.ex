defmodule StepFlow.Workflows.Workflow do
  use Ecto.Schema
  import Ecto.Changeset

  alias StepFlow.Artifacts.Artifact
  alias StepFlow.Jobs.Job
  alias StepFlow.Workflows.Status
  alias StepFlow.Workflows.Workflow

  @moduledoc false

  schema "step_flow_workflow" do
    field(:schema_version, :string)
    field(:identifier, :string)
    field(:version_major, :integer)
    field(:version_minor, :integer)
    field(:version_micro, :integer)
    field(:tags, {:array, :string}, default: [])
    field(:is_live, :boolean, default: false)
    field(:deleted, :boolean, default: false)
    field(:notification_hooks, {:array, :map}, default: [])
    field(:reference, :string)
    field(:user_uuid, :string)
    field(:steps, {:array, :map}, default: [])
    field(:start_parameters, {:array, :map}, default: [])
    field(:parameters, {:array, :map}, default: [])
    has_many(:jobs, Job, on_delete: :delete_all)
    has_many(:artifacts, Artifact, on_delete: :delete_all)
    has_many(:status, Status, on_delete: :delete_all)
    belongs_to(:parent_job, Job, foreign_key: :parent_id, defaults: nil)

    timestamps()
  end

  @doc false
  def changeset(%Workflow{} = workflow, attrs) do
    workflow
    |> cast(attrs, [
      :schema_version,
      :identifier,
      :version_major,
      :version_minor,
      :version_micro,
      :tags,
      :is_live,
      :deleted,
      :notification_hooks,
      :start_parameters,
      :parameters,
      :parent_id,
      :reference,
      :user_uuid,
      :steps
    ])
    |> validate_required([
      :schema_version,
      :identifier,
      :version_major,
      :version_minor,
      :version_micro,
      :reference,
      :steps
    ])
  end
end
