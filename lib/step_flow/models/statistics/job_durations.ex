defmodule StepFlow.Statistics.JobDurations do
  use Ecto.Schema
  import Ecto.Changeset

  alias StepFlow.Jobs.Job
  alias StepFlow.Statistics.JobDurations

  @moduledoc false

  schema "step_flow_jobs_durations" do
    belongs_to(:job, Job, foreign_key: :job_id)
    field(:job_start, :utc_datetime)
    field(:job_end, :utc_datetime)
    field(:order_pending_duration, :float)
    field(:processing_duration, :float)
    field(:response_pending_duration, :float)
    field(:total_duration, :float)

    timestamps()
  end

  @doc false
  def changeset(%JobDurations{} = job_durations, attrs) do
    job_durations
    |> cast(attrs, [
      :job_id,
      :job_start,
      :job_end,
      :order_pending_duration,
      :processing_duration,
      :response_pending_duration,
      :total_duration
    ])
    |> foreign_key_constraint(:job_id)
    |> validate_required([
      :job_id,
      :job_start,
      :job_end,
      :order_pending_duration,
      :processing_duration,
      :response_pending_duration,
      :total_duration
    ])
  end
end
