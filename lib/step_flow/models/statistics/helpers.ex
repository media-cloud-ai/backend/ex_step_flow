defmodule StepFlow.Statistics.Helpers do
  @moduledoc """
  The Helper Step context.
  """

  def limit_duration_to_milliseconds(duration) when is_integer(duration) and duration < 0, do: 0

  def limit_duration_to_milliseconds(duration) when is_integer(duration), do: duration

  def limit_duration_to_milliseconds(duration) when is_float(duration) and duration < 0, do: 0.0

  def limit_duration_to_milliseconds(duration) when is_float(duration) do
    Float.round(duration, 3)
  end
end
