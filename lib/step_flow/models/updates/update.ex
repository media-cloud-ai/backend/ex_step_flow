defmodule StepFlow.Updates.Update do
  use Ecto.Schema
  import Ecto.Changeset
  alias StepFlow.Jobs.Job
  alias StepFlow.Updates.Update

  @moduledoc false

  schema "step_flow_updates" do
    field(:datetime, :utc_datetime)
    field(:parameters, {:array, :map})
    belongs_to(:job, Job, foreign_key: :job_id)

    timestamps()
  end

  @doc false
  def changeset(%Update{} = update, attrs) do
    update
    |> cast(attrs, [:datetime, :job_id, :parameters])
    |> foreign_key_constraint(:job_id)
    |> validate_required([:datetime, :job_id, :parameters])
  end

  @doc """
  Returns the last updated update of a list of updates.
  """
  def get_last_update(update) when is_list(update) do
    update
    |> Enum.sort(fn update_1, update_2 ->
      case NaiveDateTime.compare(update_1.updated_at, update_2.updated_at) do
        :lt -> true
        :gt -> false
        :eq -> update_1.id < update_2.id
      end
    end)
    |> List.last()
  end

  def get_last_update(%Update{} = update), do: update
  def get_last_update(_update), do: nil
end
