defmodule StepFlow.Controllers.BlackList do
  require Logger

  import Ecto.Query, warn: false

  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.Jobs.Job
  alias StepFlow.Jobs.Status
  alias StepFlow.NotificationHooks.NotificationHookManager
  alias StepFlow.Repo

  @moduledoc false

  def get! do
    from(job in Job, where: job.in_black_list == true, order_by: [desc: :id])
    |> Repo.all()
    |> Enum.map(fn job -> job.id end)
    |> Enum.sort()
  end

  def add!(job_id) when is_bitstring(job_id) do
    {job_id, _} = Integer.parse(job_id)
    add!(job_id)
  end

  def add!(job_id) do
    job = StepFlow.Jobs.get_job!(job_id)

    job
    |> Job.changeset(%{in_black_list: true})
    |> Repo.update!()

    get!()
  end

  def add_and_notify!(job_id) do
    black_list = add!(job_id)

    CommonEmitter.publish(
      "",
      get_json_add_message(job_id),
      [headers: [broadcast: "true"]],
      "direct_messaging"
    )

    black_list
  end

  def contains?(job_id) when is_bitstring(job_id) do
    {job_id, _} = Integer.parse(job_id)
    contains?(job_id)
  end

  def contains?(job_id) do
    get!()
    |> Enum.any?(fn id -> id == job_id end)
  end

  def delete!(job_id) when is_bitstring(job_id) do
    {job_id, _} = Integer.parse(job_id)
    delete!(job_id)
  end

  def delete!(job_id) do
    job = StepFlow.Jobs.get_job!(job_id)

    job
    |> Job.changeset(%{in_black_list: false})
    |> Repo.update!()

    {:ok, _status} = Status.set_job_status(job_id, :dropped)
    NotificationHookManager.notification_from_job(job_id)

    get!()
  end

  def delete_and_notify!(job_id) do
    black_list = delete!(job_id)

    CommonEmitter.publish(
      "",
      get_json_delete_message(job_id),
      [headers: [broadcast: "true"]],
      "direct_messaging"
    )

    black_list
  end

  def get_sync_message do
    %{
      type: "sync_black_list"
    }
  end

  def get_json_sync_message do
    Poison.encode!(get_sync_message())
  end

  def get_request_message do
    %{
      type: "get_black_list"
    }
  end

  def get_json_request_message do
    Poison.encode!(get_request_message())
  end

  def get_add_message(job_id) when is_bitstring(job_id) do
    {job_id, _} = Integer.parse(job_id)
    get_add_message(job_id)
  end

  def get_add_message(job_id) do
    %{
      type: "add_to_black_list",
      job_id: job_id
    }
  end

  def get_json_add_message(job_id) do
    Poison.encode!(get_add_message(job_id))
  end

  def get_delete_message(job_id) when is_bitstring(job_id) do
    {job_id, _} = Integer.parse(job_id)
    get_delete_message(job_id)
  end

  def get_delete_message(job_id) do
    %{
      type: "delete_from_black_list",
      job_id: job_id
    }
  end

  def get_json_delete_message(job_id) do
    Poison.encode!(get_delete_message(job_id))
  end
end
