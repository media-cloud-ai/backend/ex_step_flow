defmodule StepFlow.Controllers.Updates do
  @moduledoc false

  def update_parameter(job_parameters, [parameter | parameters]) do
    Enum.map(job_parameters, fn x ->
      if x["id"] == parameter.id do
        x
        |> Map.replace("value", parameter.value)
      else
        x
      end
    end)
    |> update_parameter(parameters)
  end

  def update_parameter(job_parameters, []), do: job_parameters
end
