defmodule StepFlow.Controllers.Progressions do
  @moduledoc false

  alias StepFlow.Progressions.Progression

  @doc """
  Returns the last updated progression of a list of progression.
  """
  def get_last_progression(progression) when is_list(progression) do
    progression
    |> Enum.sort(fn progression_1, progression_2 ->
      case NaiveDateTime.compare(progression_1.inserted_at, progression_2.inserted_at) do
        :lt -> true
        :gt -> false
        :eq -> progression_1.id < progression_2.id
      end
    end)
    |> List.last()
  end

  def get_last_progression(%Progression{} = progression), do: progression
  def get_last_progression(_progression), do: nil
end
