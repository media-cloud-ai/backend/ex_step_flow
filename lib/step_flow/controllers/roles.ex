defmodule StepFlow.Controllers.Roles do
  @moduledoc false

  @doc """
  Returns whether the roles allow to execute the action over the entity.
  """
  def has_right?(roles, entity, action) do
    Enum.concat(for %{rights: rights} <- roles, do: rights)
    |> Enum.filter(fn right ->
      is_matching_or_wildcard?(entity, right.entity) &&
        Enum.any?(right.action, fn right_action -> Enum.member?([action, "*"], right_action) end)
    end)
    |> Enum.any?()
  end

  @doc """
  Looks for the roles bound to the specified action to execute over the entity type, and returns the related rights.
  """
  def get_rights_for_entity_type_and_action(roles, entity_type, action) do
    Enum.concat(for %{rights: rights} <- roles, do: rights)
    |> Enum.filter(fn right ->
      right_entity_type =
        String.split(right.entity, "::")
        |> List.first()

      is_matching_or_wildcard?(entity_type, right_entity_type) &&
        Enum.any?(right.action, fn right_action -> Enum.member?([action, "*"], right_action) end)
    end)
  end

  defp is_matching_or_wildcard?(reference, object) do
    right_entity_type =
      String.split(reference, "::")
      |> List.first()

    right_entity_type_wildcarded = right_entity_type <> "::*"
    match?(^reference, object) || match?(^right_entity_type_wildcarded, object) || object == "*"
  end
end
