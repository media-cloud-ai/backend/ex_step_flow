defmodule StepFlow.Controllers.WorkerStatus do
  @moduledoc false

  @doc false
  defp get_instance_id_from_system_info(worker_status) do
    StepFlow.Map.get_by_key_or_atom(worker_status, :system_info)
    |> StepFlow.Map.get_by_key_or_atom(:docker_container_id)
  end

  @doc false
  def process_worker_status_message(%{:job => job_status, :worker => worker_status}) do
    worker_status =
      worker_status
      |> Map.put_new(:instance_id, get_instance_id_from_system_info(worker_status))

    worker_status
    |> Map.put(:current_job, job_status)
  end

  @doc false
  def process_worker_status_message(%{"job" => job_status, "worker" => worker_status}) do
    worker_status =
      worker_status
      |> Map.put_new("instance_id", get_instance_id_from_system_info(worker_status))

    worker_status
    |> Map.put("current_job", job_status)
  end

  def process_worker_status_message(%StepFlow.Progressions.Progression{} = message) do
    %{
      activity: "Busy",
      instance_id: message.docker_container_id,
      direct_messaging_queue_name: "direct_messaging_" <> message.docker_container_id,
      current_job: %{
        execution_duration: 0,
        job_id: message.job_id,
        status: "processing"
      }
    }
  end

  @doc false
  def process_worker_status_message(%{:activity => _activity} = message), do: message
  def process_worker_status_message(message) when message == %{}, do: message
end
