defmodule StepFlow.DurationStatisticsView do
  use StepFlow, :view
  alias StepFlow.DurationStatisticsView

  def render("index.json", %{duration_statistics: %{data: statistics, total: total}}) do
    %{
      data: render_many(statistics, DurationStatisticsView, "duration_statistics.json"),
      total: total
    }
  end

  def render("show.json", %{duration_statistics: statistics}) do
    render_one(statistics, DurationStatisticsView, "duration_statistics.json")
  end

  def render("duration_statistics.json", %{duration_statistics: statistics}) do
    view = %{
      durations: %{
        count: statistics.durations.count,
        average: statistics.durations.average,
        max: statistics.durations.max,
        min: statistics.durations.min
      },
      name: statistics.name
    }

    case Map.get(statistics, :version, nil) do
      nil -> view
      version -> Map.put(view, :version, version)
    end
  end
end
