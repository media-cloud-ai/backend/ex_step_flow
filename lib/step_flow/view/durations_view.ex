defmodule StepFlow.DurationsView do
  use StepFlow, :view
  alias StepFlow.DurationsView

  def render("index.json", %{durations: %{data: durations, total: _total}}) do
    %{data: render_many(durations, DurationsView, "durations.json")}
  end

  def render("show.json", %{durations: durations}) do
    render_one(durations, DurationsView, "durations.json")
  end

  def render("durations.json", %{durations: durations}) do
    %{
      workflow_id: Map.get(durations, :workflow_id),
      job_id: Map.get(durations, :job_id),
      order_pending: durations.order_pending,
      processing: durations.processing,
      response_pending: durations.response_pending,
      total: durations.total
    }
  end
end
