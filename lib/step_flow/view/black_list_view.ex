defmodule StepFlow.BlackListView do
  use StepFlow, :view

  def render("index.json", %{black_list: job_ids}) do
    %{
      data: Enum.sort(job_ids),
      total: length(job_ids)
    }
  end
end
