defmodule StepFlow.NotificationTemplateView do
  use StepFlow, :view
  alias StepFlow.NotificationTemplateView

  def render("index.json", %{
        notification_templates: %{data: notification_templates, total: total}
      }) do
    %{
      data:
        render_many(
          notification_templates,
          NotificationTemplateView,
          "notification_template.json"
        ),
      total: total
    }
  end

  def render("show.json", %{notification_template: notification_template}) do
    %{
      data:
        render_one(notification_template, NotificationTemplateView, "notification_template.json")
    }
  end

  def render("notification_template.json", %{notification_template: notification_template}) do
    %{
      id: notification_template.id,
      template_name: notification_template.template_name,
      template_headers: notification_template.template_headers,
      template_body: notification_template.template_body,
      inserted_at: notification_template.inserted_at,
      updated_at: notification_template.updated_at
    }
  end

  def render("error.json", %{errors: errors}) do
    %{
      errors: [
        %{
          reason: errors.reason,
          message: Map.get(errors, :message, "Incorrect parameters")
        }
      ]
    }
  end
end
