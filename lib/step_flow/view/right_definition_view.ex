defmodule StepFlow.RightDefinitionView do
  use StepFlow, :view

  def render("right_definition.json", %{right_definition: %{rights: rights, endpoints: endpoints}}) do
    %{
      rights: rights,
      endpoints: endpoints
    }
  end
end
