defmodule StepFlow.NotificationEndpointView do
  use StepFlow, :view
  alias StepFlow.NotificationEndpointView

  def render("index.json", %{
        notification_endpoints: %{data: notification_endpoints, total: total}
      }) do
    %{
      data:
        render_many(
          notification_endpoints,
          NotificationEndpointView,
          "notification_endpoint.json"
        ),
      total: total
    }
  end

  def render("show.json", %{notification_endpoint: notification_endpoint}) do
    %{
      data:
        render_one(notification_endpoint, NotificationEndpointView, "notification_endpoint.json")
    }
  end

  def render("notification_endpoint.json", %{notification_endpoint: notification_endpoint}) do
    %{
      id: notification_endpoint.id,
      endpoint_placeholder: notification_endpoint.endpoint_placeholder,
      endpoint_url: notification_endpoint.endpoint_url,
      endpoint_credentials: notification_endpoint.endpoint_credentials,
      inserted_at: notification_endpoint.inserted_at
    }
  end

  def render("error.json", %{errors: errors}) do
    %{
      errors: [
        %{
          reason: errors.reason,
          message: Map.get(errors, :message, "Incorrect parameters")
        }
      ]
    }
  end
end
