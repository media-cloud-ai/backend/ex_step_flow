defmodule StepFlow.WorkflowStatusView do
  use StepFlow, :view

  def render("state.json", %{workflow_status: status}) do
    %{
      id: status.id,
      state: status.state,
      inserted_at: status.inserted_at,
      description: status.description
    }
  end
end
