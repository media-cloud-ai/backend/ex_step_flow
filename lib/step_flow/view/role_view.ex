defmodule StepFlow.RoleView do
  use StepFlow, :view
  alias StepFlow.RoleView

  def render("index.json", %{roles: %{data: roles, total: total}}) do
    %{data: render_many(roles, RoleView, "role.json"), total: total}
  end

  def render("show.json", %{role: role}) do
    render_one(role, RoleView, "role.json")
  end

  def render("role.json", %{role: role}) do
    rights =
      if is_list(role.rights) do
        render_many(role.rights, StepFlow.RightView, "right.json")
      else
        []
      end

    %{
      id: role.id,
      name: role.name,
      rights: rights,
      is_unused: role.is_unused
    }
  end
end
