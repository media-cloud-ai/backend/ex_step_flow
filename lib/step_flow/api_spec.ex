defmodule StepFlow.ApiSpec do
  @moduledoc false

  alias OpenApiSpex.{Components, Info, OpenApi, Paths, SecurityScheme}
  @behaviour OpenApi

  @impl OpenApi
  def spec do
    %OpenApi{
      info: %Info{
        title: "Step Flow",
        version: Application.spec(:ex_backend)[:vsn] |> to_string()
      },
      paths: Paths.from_routes(get_api_routes()),
      components: %Components{
        securitySchemes: %{"authorization" => %SecurityScheme{type: "http", scheme: "bearer"}}
      }
    }
    |> OpenApiSpex.resolve_schema_modules()
  end

  defp get_api_routes do
    StepFlow.Router.__routes__()
    |> Enum.map(fn route ->
      %Phoenix.Router.Route{route | path: "/api/step_flow" <> route.path}
    end)
  end
end
