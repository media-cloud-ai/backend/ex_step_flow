defmodule StepFlow.Notification do
  @moduledoc """
  Send notifications to endpoint to be forwarded to websockets.
  """

  require Logger

  def send(topic, body) do
    configuration = Application.get_env(:step_flow, StepFlow)

    if Keyword.has_key?(configuration, :endpoint) do
      endpoint =
        configuration
        |> Keyword.get(:endpoint)

      case GenServer.whereis(endpoint) do
        nil ->
          Logger.warn(
            "The notification endpoint #{inspect(endpoint)} is not started yet, cannot send notification: topic=#{inspect(topic)}, body=#{inspect(body)}"
          )

        _pid ->
          do_send(endpoint, topic, body)
      end
    end
  end

  defp do_send(endpoint, topic, body) do
    case endpoint.broadcast("notifications:all", topic, %{
           body: body
         }) do
      {:error, message} ->
        Logger.error("Could not notify endpoint #{inspect(endpoint)}: #{message}")

      _ ->
        :ok
    end
  end
end
