defmodule StepFlow.Migration.All do
  @moduledoc false

  def apply_migrations do
    Ecto.Migrator.up(
      StepFlow.Repo,
      20_191_011_180_000,
      StepFlow.Migration.CreateWorkflow
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_191_011_180_100,
      StepFlow.Migration.CreateJobs
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_191_011_180_200,
      StepFlow.Migration.CreateStatus
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_191_011_180_300,
      StepFlow.Migration.CreateArtifacts
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_191_022_164_200,
      StepFlow.Migration.CreateWorkerDefinitions
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_191_022_164_200,
      StepFlow.Migration.CreateWorkerDefinitions
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_200_130_170_300,
      StepFlow.Migration.CreateProgressions
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_201_016_090_900,
      StepFlow.Migration.CreateWorkflowDefinition
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_201_020_140_300,
      StepFlow.Migration.ModifyWorkerDefinitionsParametersType
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_201_119_140_300,
      StepFlow.Migration.CreateWorkfkowRights
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_201_127_120_000,
      StepFlow.Migration.AddSchemaVersionForWorkflow
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_303_120_000,
      StepFlow.Migration.CreateWorkflowStatus
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_416_100_000,
      StepFlow.Migration.AddLiveParameter
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_416_100_100,
      StepFlow.Migration.AddDeletedParameter
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_416_110_000,
      StepFlow.Migration.AddUpdatableParameter
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_416_120_000,
      StepFlow.Migration.CreateUpdates
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_416_130_000,
      StepFlow.Migration.CreateLiveWorkerTable
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_426_230_000,
      StepFlow.Migration.ModifyWorkflowStatus
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_430_170_000,
      StepFlow.Migration.CreateWorkerStatus
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_210_917_120_000,
      StepFlow.Migration.AddUserUuidToWorkflow
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_211_018_160_000,
      StepFlow.Migration.AddWorkerInstanceIdToJob
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_211_020_120_000,
      StepFlow.Migration.AddAllowFailureToJob
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_211_028_120_000,
      StepFlow.Migration.CreateRoles
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_220_303_120_000,
      StepFlow.Migration.AddSystemInfoToProgress
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_220_511_170_000,
      StepFlow.Migration.AddNotificationHookWorkflow
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_220_519_150_000,
      StepFlow.Migration.AddWorkflowStatusDescription
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_220_519_170_000,
      StepFlow.Migration.CreateAndFillJobsDurationsTables
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_220_521_150_000,
      StepFlow.Migration.AddDeletedParameterJobs
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_220_522_150_000,
      StepFlow.Migration.CreateNotificationEndpoints
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_221_005_150_000,
      StepFlow.Migration.UpdateWorkerDescriptionValueLength
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_221_102_140_000,
      StepFlow.Migration.AddInBlackListToJob
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_221_116_170_000,
      StepFlow.Migration.AddParentWorkflowToWorkflow
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_222_003_150_000,
      StepFlow.Migration.CreateNotificationTemplates
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_230_209_130_000,
      StepFlow.Migration.UpdateProgressionTimestampsPrecision
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_230_222_120_000,
      StepFlow.Migration.AddDatetimeStatus
    )

    Ecto.Migrator.up(
      StepFlow.Repo,
      20_230_614_150_000,
      StepFlow.Migration.AddStartParameters
    )
  end
end
