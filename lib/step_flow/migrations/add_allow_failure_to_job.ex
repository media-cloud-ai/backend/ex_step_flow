defmodule StepFlow.Migration.AddAllowFailureToJob do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_jobs) do
      add(:allow_failure, :boolean, default: false)
    end
  end
end
