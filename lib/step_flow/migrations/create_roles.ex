defmodule StepFlow.Migration.CreateRoles do
  use Ecto.Migration
  @moduledoc false

  def change do
    create table(:step_flow_roles) do
      add(:name, :string)
      add(:rights, {:array, :map}, default: [])
      add(:is_unused, :boolean, default: true)

      timestamps()
    end
  end
end
