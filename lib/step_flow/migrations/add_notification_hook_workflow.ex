defmodule StepFlow.Migration.AddNotificationHookWorkflow do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_workflow_definition) do
      add(:notification_hooks, {:array, :map}, default: [])
    end

    alter table(:step_flow_workflow) do
      add(:notification_hooks, {:array, :map}, default: [])
    end
  end
end
