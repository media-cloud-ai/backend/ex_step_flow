defmodule StepFlow.Migration.UpdateProgressionTimestampsPrecision do
  @moduledoc false

  use Ecto.Migration

  def change do
    alter table(:step_flow_progressions) do
      modify(:datetime, :utc_datetime_usec)
    end
  end
end
