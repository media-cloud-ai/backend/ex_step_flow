defmodule StepFlow.Migration.AddDeletedParameter do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_workflow_definition) do
      add(:deleted, :boolean, default: false)
    end

    alter table(:step_flow_workflow) do
      add(:deleted, :boolean, default: false)
    end
  end
end
