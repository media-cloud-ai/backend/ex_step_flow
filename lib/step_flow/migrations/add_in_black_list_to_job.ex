defmodule StepFlow.Migration.AddInBlackListToJob do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_jobs) do
      add(:in_black_list, :boolean, default: false)
    end
  end
end
