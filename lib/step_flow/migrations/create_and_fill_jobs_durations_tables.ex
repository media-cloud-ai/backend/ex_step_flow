defmodule StepFlow.Migration.CreateAndFillJobsDurationsTables do
  use Ecto.Migration
  @moduledoc false

  def up do
    create table(:step_flow_jobs_durations) do
      add(:job_id, references(:step_flow_jobs, on_delete: :delete_all))
      add(:job_start, :utc_datetime)
      add(:job_end, :utc_datetime)
      add(:order_pending_duration, :float)
      add(:processing_duration, :float)
      add(:response_pending_duration, :float)
      add(:total_duration, :float)

      timestamps()
    end

    create(index(:step_flow_jobs_durations, [:job_id]))

    execute("
        CREATE TYPE timestamp_id_pair AS (inserted_at timestamp, job_id bigint);
    ")

    execute("
        CREATE TYPE duration_id_pair AS (duration real, job_id bigint);
    ")

    execute("
        CREATE TYPE first_and_last_progressions_timestamp AS (
            first_progression timestamp,
            last_progression timestamp,
            job_id bigint
        );
    ")

    execute("
        CREATE TYPE first_and_last_status_timestamp AS (
            first_status timestamp,
            last_status timestamp,
            job_id bigint
        );
    ")

    execute("
        CREATE TYPE job_duration AS (
              job_id bigint,
              job_start timestamp,
              job_end timestamp,
              order_pending_duration real,
              processing_duration real,
              response_pending_duration real,
              inserted_at timestamp,
              updated_at timestamp
          );
      ")

    execute(
      "CREATE OR REPLACE FUNCTION get_status_starting_from_last_retry() RETURNS SETOF timestamp_id_pair AS
              $BODY$
              BEGIN
              RETURN QUERY
              SELECT sfs.inserted_at, sfs.job_id
              FROM step_flow_status sfs
              LEFT JOIN (
                  SELECT job_id, MAX(inserted_at) as time_last_retry FROM step_flow_status WHERE state='retrying' GROUP BY job_id
              ) tsr on sfs.job_id = tsr.job_id
              WHERE sfs.inserted_at >= tsr.time_last_retry OR tsr.time_last_retry IS NULL;
              END;
              $BODY$
              LANGUAGE plpgsql;
          "
    )

    execute(
      "CREATE OR REPLACE FUNCTION get_progressions_starting_from_last_retry() RETURNS SETOF timestamp_id_pair AS
              $BODY$
              BEGIN
              RETURN QUERY
              SELECT sfp.inserted_at, sfp.job_id
              FROM step_flow_progressions sfp
              LEFT JOIN (
                  SELECT job_id, MAX(inserted_at) as time_last_retry FROM step_flow_status WHERE state='retrying' GROUP BY job_id
              ) tsr on sfp.job_id = tsr.job_id
              WHERE sfp.inserted_at >= tsr.time_last_retry OR tsr.time_last_retry IS NULL;
              END;
              $BODY$
              LANGUAGE plpgsql;
          "
    )

    execute(
      "CREATE OR REPLACE FUNCTION get_first_and_last_progressions_time() RETURNS SETOF first_and_last_progressions_timestamp AS
          $BODY$
          BEGIN
          RETURN QUERY WITH prog_corr AS (SELECT * from get_progressions_starting_from_last_retry())
              SELECT MIN(inserted_at), MAX(inserted_at), job_id FROM prog_corr GROUP BY job_id;
          END;
          $BODY$
          LANGUAGE plpgsql;
          "
    )

    execute(
      "CREATE OR REPLACE FUNCTION get_first_and_last_status_time() RETURNS SETOF first_and_last_status_timestamp AS
          $BODY$
          BEGIN
          RETURN QUERY WITH status_corr AS (SELECT * from get_status_starting_from_last_retry())
              SELECT MIN(inserted_at), MAX(inserted_at), job_id FROM status_corr GROUP BY job_id;
          END;
          $BODY$
          LANGUAGE plpgsql;
          "
    )

    execute(
      "CREATE OR REPLACE FUNCTION get_durations_from_timestamp(timestamp, timestamp) RETURNS real AS
          $BODY$
          BEGIN
          RETURN coalesce(extract(epoch from ($1 - $2))::real, 0);
          END;
          $BODY$
          LANGUAGE plpgsql;
        "
    )

    execute(
      "CREATE OR REPLACE FUNCTION extract_execution_durations() RETURNS SETOF duration_id_pair AS
          $BODY$
          BEGIN
          RETURN QUERY SELECT
              (s.description ->> 'execution_duration')::real as duration, s.job_id
              FROM step_flow_status s;
          END;
          $BODY$
          LANGUAGE plpgsql;
        "
    )

    execute(
      "CREATE OR REPLACE FUNCTION get_execution_durations() RETURNS SETOF duration_id_pair AS
          $BODY$
          BEGIN
          RETURN QUERY SELECT DISTINCT ON (job_id) duration, job_id
            FROM extract_execution_durations() ced ORDER BY ced.job_id, duration NULLS LAST;
          END;
          $BODY$
          LANGUAGE plpgsql;
        "
    )

    execute("
        CREATE OR REPLACE FUNCTION get_jobs_durations() RETURNS SETOF job_duration AS
              $BODY$
              BEGIN
              RETURN QUERY WITH cte AS (
                  SELECT jobs.id as job_id,
                      jobs.inserted_at as job_inserted_at,
                      jobs.updated_at as job_updated_at,
                      ed.duration as exec_duration,
                      st.last_status as last_status,
                      pt.last_progression as last_progression,
                      coalesce(pt.first_progression, st.first_status, jobs.inserted_at) as job_start,
                      coalesce(ROUND(ed.duration)*INTERVAL '1 seconds' +
                          coalesce(pt.first_progression, st.first_status, jobs.inserted_at),
                          pt.last_progression,
                          st.last_status,
                          coalesce(pt.first_progression, st.first_status, jobs.inserted_at)) as job_end
                  FROM step_flow_jobs as jobs
                  LEFT JOIN get_execution_durations() as ed on jobs.id = ed.job_id
                  LEFT JOIN get_first_and_last_progressions_time() as pt on jobs.id = pt.job_id
                  LEFT JOIN get_first_and_last_status_time() as st on jobs.id = st.job_id
              )
              SELECT
                  cte.job_id,
                  cte.job_start,
                  cte.job_end,
                  GREATEST(0,
                      get_durations_from_timestamp(
                          cte.job_start,
                          cte.job_inserted_at
                      )
                  ),
                  GREATEST(0,
                      coalesce(
                          cte.exec_duration,
                          get_durations_from_timestamp(
                              coalesce(cte.last_progression, cte.last_status),
                              cte.job_start
                          )
                      )
                  ),
                  GREATEST(0,
                      get_durations_from_timestamp(
                          cte.last_status,
                          cte.job_end
                      )
                  ),
                  cte.job_inserted_at,
                  cte.job_updated_at
              FROM cte;
              END;
              $BODY$
              LANGUAGE plpgsql;
          ")

    # We have to specify each column to activate auto_increment of the id
    execute("INSERT INTO step_flow_jobs_durations(
              job_id,
              job_start,
              job_end,
              order_pending_duration,
              processing_duration,
              response_pending_duration,
              total_duration,
              inserted_at,
              updated_at)
        SELECT
              job_id,
              job_start,
              job_end,
              order_pending_duration,
              processing_duration,
              response_pending_duration,
              order_pending_duration + processing_duration + response_pending_duration as total_duration,
              inserted_at,
              updated_at
        FROM get_jobs_durations();
        ")
  end

  def down do
    drop(table(:step_flow_jobs_durations))
    execute("DROP TYPE timestamp_id_pair CASCADE;")
    execute("DROP TYPE duration_id_pair CASCADE;")
    execute("DROP TYPE first_and_last_progressions_timestamp CASCADE;")
    execute("DROP TYPE first_and_last_status_timestamp CASCADE;")
    execute("DROP TYPE job_duration CASCADE;")
  end
end
