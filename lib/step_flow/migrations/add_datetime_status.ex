defmodule StepFlow.Migration.AddDatetimeStatus do
  @moduledoc false

  use Ecto.Migration

  def change do
    alter table(:step_flow_status) do
      add(:datetime, :utc_datetime_usec)
    end
  end
end
