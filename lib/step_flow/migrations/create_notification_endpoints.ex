defmodule StepFlow.Migration.CreateNotificationEndpoints do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:step_flow_notification_endpoints) do
      add(:endpoint_placeholder, :string)
      add(:endpoint_url, :string)
      add(:endpoint_credentials, :string, default: "")
      timestamps()
    end

    create(unique_index(:step_flow_notification_endpoints, [:endpoint_placeholder]))
  end
end
