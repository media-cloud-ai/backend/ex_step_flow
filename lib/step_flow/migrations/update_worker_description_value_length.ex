defmodule StepFlow.Migration.UpdateWorkerDescriptionValueLength do
  @moduledoc false

  use Ecto.Migration

  def change do
    alter table(:step_flow_worker_status) do
      modify(:description, :text)
    end
  end
end
