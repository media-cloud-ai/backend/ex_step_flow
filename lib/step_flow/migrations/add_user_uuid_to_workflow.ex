defmodule StepFlow.Migration.AddUserUuidToWorkflow do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_workflow) do
      add(:user_uuid, :string)
    end
  end
end
