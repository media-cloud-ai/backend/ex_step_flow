defmodule StepFlow.Migration.AddWorkflowStatusDescription do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_workflow_status) do
      add(:description, :map, default: %{})
    end
  end
end
