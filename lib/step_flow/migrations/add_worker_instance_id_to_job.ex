defmodule StepFlow.Migration.AddWorkerInstanceIdToJob do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_jobs) do
      add(:last_worker_instance_id, :string)
    end
  end
end
