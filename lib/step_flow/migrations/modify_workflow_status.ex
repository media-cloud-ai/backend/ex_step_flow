defmodule StepFlow.Migration.ModifyWorkflowStatus do
  use Ecto.Migration
  @moduledoc false

  def change do
    drop_if_exists(table(:step_flow_workflow_status))

    create table(:step_flow_workflow_status) do
      add(:state, :string)
      add(:status_id, references(:step_flow_status, on_delete: :delete_all), null: true)
      add(:workflow_id, references(:step_flow_workflow, on_delete: :delete_all))

      timestamps()
    end

    create(index(:step_flow_workflow_status, [:workflow_id]))
  end
end
