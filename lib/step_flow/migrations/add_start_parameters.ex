defmodule StepFlow.Migration.AddStartParameters do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_workflow) do
      add(:start_parameters, {:array, :map}, default: [])
    end
  end
end
