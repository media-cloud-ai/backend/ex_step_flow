defmodule StepFlow.Migration.AddDeletedParameterJobs do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_jobs) do
      add(:deleted, :boolean, default: false)
    end

    execute("
        UPDATE step_flow_jobs SET deleted=true where workflow_id in (SELECT id from step_flow_workflow where deleted=true);
      ")
  end
end
