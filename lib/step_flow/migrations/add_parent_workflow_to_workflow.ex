defmodule StepFlow.Migration.AddParentWorkflowToWorkflow do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_workflow) do
      add(:parent_id, references(:step_flow_jobs, on_delete: :nothing), null: true)
    end

    create(index(:step_flow_workflow, [:parent_id]))
  end
end
