defmodule StepFlow.Migration.CreateNotificationTemplates do
  @moduledoc false

  use Ecto.Migration

  def change do
    create table(:step_flow_notification_templates) do
      add(:template_name, :string)
      add(:template_headers, :text)
      add(:template_body, :text)
      timestamps()
    end

    create(unique_index(:step_flow_notification_templates, [:template_name]))
  end
end
