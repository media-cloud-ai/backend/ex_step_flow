defmodule StepFlow.Migration.AddSystemInfoToProgress do
  use Ecto.Migration
  @moduledoc false

  def change do
    alter table(:step_flow_progressions) do
      add(:system_info, :map, default: %{})
    end
  end
end
