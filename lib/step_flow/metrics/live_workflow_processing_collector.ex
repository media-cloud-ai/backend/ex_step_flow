defmodule StepFlow.Metrics.LiveWorkflowProcessingCollector do
  @moduledoc """
  Prometheus metrics collector for live workflows processing
  """
  use Prometheus.Collector
  alias StepFlow.Workflows
  require Logger

  def collect_mf(_registry, callback) do
    lives = Workflows.get_processing_workflow_live()

    callback.(
      create_gauge(
        :step_flow_live_workflows_processing,
        "Step flow live workflows processing by reference",
        lives
      )
    )

    :ok
  end

  def collect_metrics(:step_flow_live_workflows_processing, lives) do
    Prometheus.Model.gauge_metrics(
      Enum.map(lives, fn %{reference: reference, identifier: identifier, count: count} ->
        {[workflow_reference: reference, workflow_identifier: identifier], count}
      end)
    )
  end

  defp create_gauge(name, help, data) do
    Prometheus.Model.create_mf(name, help, :gauge, __MODULE__, data)
  end
end
