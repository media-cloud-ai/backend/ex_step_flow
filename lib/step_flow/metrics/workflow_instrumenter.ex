defmodule StepFlow.Metrics.WorkflowInstrumenter do
  @moduledoc """
  Prometheus metrics instrumenter to call workflow metric collectors
  """
  use Prometheus.Metric

  def setup do
    Prometheus.Registry.register_collector(StepFlow.Metrics.WorkflowCollector)
    Prometheus.Registry.register_collector(StepFlow.Metrics.LiveWorkflowProcessingCollector)

    Counter.declare(
      name: :step_flow_workflows_status_total,
      help: "Number of workflows by status.",
      labels: [:name, :status]
    )
  end

  def inc(:step_flow_workflows_status_total, workflow_name, workflow_status) do
    if StepFlow.Configuration.metrics_enabled?() do
      case workflow_status do
        :created_workflow ->
          Counter.inc(name: :step_flow_workflows_status_total, labels: [workflow_name, "created"])

        :completed ->
          Counter.inc(
            name: :step_flow_workflows_status_total,
            labels: [workflow_name, "completed"]
          )

        :error ->
          Counter.inc(name: :step_flow_workflows_status_total, labels: [workflow_name, "error"])

        :stopped ->
          Counter.inc(name: :step_flow_workflows_status_total, labels: [workflow_name, "stopped"])

        _ ->
          :ok
      end
    end
  end
end
