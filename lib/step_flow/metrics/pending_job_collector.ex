defmodule StepFlow.Metrics.JobsPendingCollector do
  @moduledoc """
  Prometheus metrics collector for pending jobs
  """
  use Prometheus.Collector
  alias StepFlow.Jobs
  require Logger

  def collect_mf(_registry, callback) do
    jobs = Jobs.get_pending_jobs_by_type()

    callback.(
      create_gauge(
        :step_flow_jobs_pending,
        "Step flow pending jobs by type",
        jobs
      )
    )

    :ok
  end

  def collect_metrics(:step_flow_jobs_pending, jobs) do
    Prometheus.Model.gauge_metrics(
      Enum.map(jobs, fn %{name: name, value: value} ->
        {[job_name: name], value}
      end)
    )
  end

  defp create_gauge(name, help, data) do
    Prometheus.Model.create_mf(name, help, :gauge, __MODULE__, data)
  end
end
