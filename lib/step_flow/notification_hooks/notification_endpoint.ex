defmodule StepFlow.NotificationHooks.NotificationEndpoint do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias StepFlow.NotificationHooks.NotificationEndpoint

  schema "step_flow_notification_endpoints" do
    field(:endpoint_placeholder, :string)
    field(:endpoint_url, :string)
    field(:endpoint_credentials, :string, default: "")
    timestamps()
  end

  @doc false
  def changeset(%NotificationEndpoint{} = notification_endpoint, attrs) do
    notification_endpoint
    |> cast(attrs, [:endpoint_placeholder, :endpoint_url, :endpoint_credentials])
    |> unique_constraint(:endpoint_placeholder)
    |> validate_required([:endpoint_placeholder, :endpoint_url])
  end
end
