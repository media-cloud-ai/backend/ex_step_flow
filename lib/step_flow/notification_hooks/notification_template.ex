defmodule StepFlow.NotificationHooks.NotificationTemplate do
  @moduledoc false

  use Ecto.Schema
  import Ecto.Changeset
  alias StepFlow.NotificationHooks.NotificationTemplate

  schema "step_flow_notification_templates" do
    field(:template_name, :string)
    field(:template_headers, :string)
    field(:template_body, :string)
    timestamps()
  end

  @doc false
  def changeset(%NotificationTemplate{} = notification_template, attrs) do
    notification_template
    |> cast(attrs, [:template_name, :template_headers, :template_body])
    |> unique_constraint(:template_name)
    |> validate_required([:template_name, :template_headers, :template_body])
  end
end
