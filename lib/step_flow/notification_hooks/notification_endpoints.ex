defmodule StepFlow.NotificationHooks.NotificationEndpoints do
  @moduledoc """
  The Notification endpoints context.
  """

  import Ecto.Query, warn: false
  alias StepFlow.NotificationHooks.NotificationEndpoint
  alias StepFlow.Repo

  defp force_integer(param) when is_bitstring(param) do
    param
    |> String.to_integer()
  end

  defp force_integer(param) do
    param
  end

  @doc """
  Returns the list of notification endpoints.

  ## Examples

      iex> list_notification_endpoints()
      [%NotificationEndpoint{}, ...]

  """
  def list_notification_endpoints(params \\ %{}) do
    page =
      Map.get(params, "page", 0)
      |> force_integer

    size =
      Map.get(params, "size", 100)
      |> force_integer

    offset = page * size

    query = from(notification_endpoint in NotificationEndpoint)

    query =
      case Map.get(params, "endpoint_placeholder") do
        nil ->
          query

        endpoint_placeholder ->
          from(notification_endpoint in query,
            where: notification_endpoint.endpoint_placeholder == ^endpoint_placeholder
          )
      end

    total_query = from(item in query, select: count(item.id))

    total =
      Repo.all(total_query)
      |> List.first()

    query =
      from(
        notification_endpoint in query,
        order_by: [desc: :endpoint_placeholder],
        offset: ^offset,
        limit: ^size
      )

    notification_endpoints = Repo.all(query)

    %{
      data: notification_endpoints,
      total: total,
      page: page,
      size: size
    }
  end

  @doc """
  Gets a single notification_endpoint.

  Raises `Ecto.NoResultsError` if the notification_endpoint does not exist.

  ## Examples

      iex> get_notification_endpoint!(123)
      %NotificationEndpoint{}

      iex> get_notification_endpoint!(456)
      ** (Ecto.NoResultsError)

  """
  def get_notification_endpoint!(id), do: Repo.get!(NotificationEndpoint, id)

  @doc """
  Gets a single notification_endpoint by endpoint_placeholder.

  ## Examples

      iex> get_notification_endpoint_by_placeholder("MY_PLACEHOLDER")
      %NotificationEndpoint{}

      iex> get_notification_endpoint_by_placeholder("BAD_PLACEHOLDER")
      nil

  """
  def get_notification_endpoint_by_placeholder(endpoint_placeholder),
    do: Repo.get_by(NotificationEndpoint, endpoint_placeholder: endpoint_placeholder)

  @doc """
  Creates a notification_endpoint.

  ## Examples

      iex> create_notification_endpoint(%
      {
        endpoint_placeholder: endpoint_placeholder_value,
        endpoint_url: endpoint_url_value,
        endpoint_credentials (optional): "username:password"
      })
      {:ok, %NotificationEndpoint{}}

      iex> create_notification_endpoint(%
      {
        endpoint_placeholder: bad_endpoint_placeholder_value,
        endpoint_url: endpoint_url_value,
        endpoint_credentials (optional): "username:password"
      })
      {:error, %Ecto.Changeset{}}

  """
  def create_notification_endpoint(attrs \\ %{}) do
    %NotificationEndpoint{}
    |> NotificationEndpoint.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a NotificationEndpoint.

  ## Examples

      iex> delete_notification_endpoint(notification_endpoint)
      {:ok, %NotificationEndpoint{}}

      iex> delete_notification_endpoint(notification_endpoint)
      {:error, %Ecto.Changeset{}}

  """
  def delete_notification_endpoint(%NotificationEndpoint{} = notification_endpoint) do
    Repo.delete(notification_endpoint)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking notification_endpoint changes.

  ## Examples

      iex> change_notification_endpoint(notification_endpoint)
      %Ecto.Changeset{source: %NotificationEndpoint{}}

  """
  def change_notification_endpoint(%NotificationEndpoint{} = notification_endpoint) do
    NotificationEndpoint.changeset(notification_endpoint, %{})
  end

  def update_notification_endpoint(%NotificationEndpoint{} = notification_endpoint, attrs) do
    notification_endpoint
    |> NotificationEndpoint.changeset(attrs)
    |> Repo.update()
  end
end
