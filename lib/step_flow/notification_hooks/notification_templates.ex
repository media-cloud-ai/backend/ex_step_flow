defmodule StepFlow.NotificationHooks.NotificationTemplates do
  @moduledoc """
  The Notification templates context.
  """

  import Ecto.Query, warn: false
  alias StepFlow.NotificationHooks.NotificationTemplate
  alias StepFlow.Repo

  defp force_integer(param) when is_bitstring(param) do
    param
    |> String.to_integer()
  end

  defp force_integer(param) do
    param
  end

  @doc """
  Returns the list of notification templates.

  ## Examples

      iex> list_notification_templates()
      [%NotificationTemplate{}, ...]

  """
  def list_notification_templates(params \\ %{}) do
    page =
      Map.get(params, "page", 0)
      |> force_integer

    size =
      Map.get(params, "size", 100)
      |> force_integer

    offset = page * size

    query = from(notification_template in NotificationTemplate)

    query =
      case Map.get(params, "template_name") do
        nil ->
          query

        template_name ->
          from(notification_template in query,
            where: notification_template.template_name == ^template_name
          )
      end

    total = Repo.aggregate(query, :count)

    query =
      from(
        notification_template in query,
        order_by: [desc: :template_name],
        offset: ^offset,
        limit: ^size
      )

    notification_templates = Repo.all(query)

    %{
      data: notification_templates,
      total: total,
      page: page,
      size: size
    }
  end

  @doc """
  Gets a single notification_template.

  Raises `Ecto.NoResultsError` if the notification_template does not exist.

  ## Examples

      iex> get_notification_template!(123)
      %NotificationTemplate{}

      iex> get_notification_template!(456)
      ** (Ecto.NoResultsError)

  """
  def get_notification_template!(id), do: Repo.get!(NotificationTemplate, id)

  @doc """
  Gets a single notification_template by template_name.

  ## Examples

      iex> get_notification_template_by_name("MY_NAME")
      %NotificationTemplate{}

      iex> get_notification_template_by_name("BAD_NAME")
      nil

  """
  def get_notification_template_by_name(template_name),
    do: Repo.get_by(NotificationTemplate, template_name: template_name)

  @doc """
  Creates a notification_template.

  ## Examples

      iex> create_notification_template(%
      {
        template_name: template_name_value,
        template_headers: template_headers_value,
        template_body: template_body_value
      })
      {:ok, %NotificationTemplate{}}

      iex> create_notification_template(%
      {
        template_name: duplicate_template_name_value,
        template_headers: template_headers_value,
        template_body: template_body_value
      })
      {:error, %Ecto.Changeset{}}

  """
  def create_notification_template(attrs \\ %{}) do
    %NotificationTemplate{}
    |> NotificationTemplate.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Deletes a NotificationTemplate.

  ## Examples

      iex> delete_notification_template(notification_template)
      {:ok, %NotificationTemplate{}}

      iex> delete_notification_template(notification_template)
      {:error, %Ecto.Changeset{}}

  """
  def delete_notification_template(%NotificationTemplate{} = notification_template) do
    Repo.delete(notification_template)
  end

  @doc """
  Updates NotificationTemplate.

  ## Examples

      iex> update_notification_template(notification_template, %{field: new_value})
      {:ok, %NotificationTemplate{}}

      iex> update_notification_template(notification_template, %{field: bad_value})
      {:error, %Ecto.Changeset{}}
  """

  def update_notification_template(%NotificationTemplate{} = notification_template, attrs) do
    notification_template
    |> NotificationTemplate.changeset(attrs)
    |> Repo.update()
  end
end
