defmodule StepFlow.Router do
  use StepFlow, :router

  pipeline :api do
    plug(Plug.Parsers,
      parsers: [:json],
      pass: ["application/json"],
      json_decoder: Jason
    )

    plug(OpenApiSpex.Plug.PutApiSpec, module: StepFlow.ApiSpec)
  end

  pipe_through(:api)

  get("/", StepFlow.WebController.Index, :index)

  # Roles and rights APIs
  resources("/roles", StepFlow.WebController.Role, except: [:new, :edit])
  get("/right_definitions", StepFlow.WebController.RightDefinition, :index)

  # Workflow definitions APIs
  resources("/definitions", StepFlow.WebController.WorkflowDefinition,
    except: [:new, :edit, :update]
  )

  delete(
    "/definitions",
    StepFlow.WebController.WorkflowDefinition,
    :delete_by_version_and_identifier
  )

  # Workflows APIs
  resources "/workflows", StepFlow.WebController.Workflow, except: [:new, :edit] do
    post("/events", StepFlow.WebController.WorkflowEvents, :handle)
  end

  get("/workflows_status", StepFlow.WebController.WorkflowStatus, :list)

  # DEPRECATED
  post("/launch_workflow", StepFlow.WebController.Workflow, :create)

  get("/workflows_statistics", StepFlow.WebController.Workflow, :statistics)

  # Statistics APIs
  get("/durations/jobs", StepFlow.WebController.Durations, :list_jobs_durations)
  get("/durations/workflows", StepFlow.WebController.Durations, :list_workflows_durations)

  get(
    "/statistics/durations/workflows",
    StepFlow.WebController.Statistics,
    :get_workflows_duration_statistics
  )

  get(
    "/statistics/durations/jobs",
    StepFlow.WebController.Statistics,
    :get_jobs_duration_statistics
  )

  # Jobs APIs
  resources("/jobs", StepFlow.WebController.Job, except: [:new, :edit, :delete, :update])
  resources("/blacklist", StepFlow.WebController.BlackList, except: [:show, :new, :edit, :update])

  # Workers APIs
  resources("/worker_definitions", StepFlow.WebController.WorkerDefinition,
    except: [:new, :edit, :delete, :update]
  )

  # Live Workers APIs
  get("/live_workers", StepFlow.WebController.LiveWorkers, :index)

  # Workers APIs
  resources("/workers", StepFlow.WebController.Workers, only: [:index, :show, :update])

  # Notification endpoints APIs
  resources("/notification_endpoints", StepFlow.WebController.NotificationEndpoint,
    except: [:edit, :new]
  )

  # Notification templates APIs
  resources("/notification_templates", StepFlow.WebController.NotificationTemplate,
    except: [:edit, :new]
  )

  # Metrics APIs
  get("/metrics", StepFlow.WebController.Metric, :index)

  # OpenAPI documentation
  get("/openapi", OpenApiSpex.Plug.RenderSpec, [])

  get("/*path", StepFlow.WebController.Index, :not_found)
end
