defmodule StepFlow.WebController.Statistics do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.Controllers.Statistics.Durations
  alias StepFlow.WebController.OpenApiSchemas

  @moduledoc false

  tags ["Statistics"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  operation :get_workflows_duration_statistics,
    summary: "List workflows duration statistics",
    description: "List durations statistics of all workflows",
    type: :object,
    responses: [
      ok: {"Durations", "application/json", OpenApiSchemas.Statistics.JobsDurationStatistics},
      forbidden: "Forbidden"
    ]

  def get_workflows_duration_statistics(
        %Plug.Conn{assigns: %{current_user: user}} = conn,
        params \\ %{}
      ) do
    duration_statistics =
      params
      |> Map.put("roles", user.roles)
      |> Durations.get_workflows_duration_statistics()

    conn
    |> put_view(StepFlow.DurationStatisticsView)
    |> render("index.json", duration_statistics: duration_statistics)
  end

  operation :get_jobs_duration_statistics,
    summary: "List jobs duration",
    description: "List durations of all jobs",
    type: :object,
    responses: [
      ok:
        {"Durations", "application/json", OpenApiSchemas.Statistics.WorkflowsDurationStatistics},
      forbidden: "Forbidden"
    ]

  def get_jobs_duration_statistics(
        %Plug.Conn{assigns: %{current_user: user}} = conn,
        params \\ %{}
      ) do
    duration_statistics =
      params
      |> Map.put("roles", user.roles)
      |> Durations.get_jobs_duration_statistics()

    conn
    |> put_view(StepFlow.DurationStatisticsView)
    |> render("index.json", duration_statistics: duration_statistics)
  end
end
