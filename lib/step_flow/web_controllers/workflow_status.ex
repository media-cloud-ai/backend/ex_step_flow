defmodule StepFlow.WebController.WorkflowStatus do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  require Logger

  alias OpenApiSpex.Schema
  alias StepFlow.Workflows.Status

  @moduledoc false

  tags ["Workflows"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(StepFlow.WebController.Fallback)

  operation :list,
    summary: "List workflows statuses",
    description: "List workflows statuses",
    type: :array,
    items: %Schema{type: :string},
    responses: [
      ok: "Statuses"
    ]

  def list(conn, _params) do
    status =
      Status.StateEnum.__valid_values__()
      |> Enum.filter(fn value -> is_bitstring(value) end)

    conn
    |> json(status)
  end
end
