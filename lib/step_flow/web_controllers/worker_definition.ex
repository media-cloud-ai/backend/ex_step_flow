defmodule StepFlow.WebController.WorkerDefinition do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.WebController.OpenApiSchemas
  alias StepFlow.WorkerDefinitions
  alias StepFlow.WorkerDefinitions.WorkerDefinition

  @moduledoc false

  tags ["Worker Definitions"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(StepFlow.WebController.Fallback)

  operation :index,
    summary: "List worker definitions",
    description: "List worker definitions",
    type: :object,
    responses: [
      ok: {"WorkerDefinitions", "application/json", OpenApiSchemas.Workers.WorkerDefinitions},
      forbidden: "Forbidden"
    ]

  def index(conn, params) do
    worker_definitions = WorkerDefinitions.list_worker_definitions(params)

    conn
    |> put_view(StepFlow.WorkerDefinitionView)
    |> render("index.json", worker_definitions: worker_definitions)
  end

  operation :create,
    summary: "Create worker definition",
    description: "Create a worker definition",
    type: :object,
    request_body:
      {"WorkerDefinition", "application/json", OpenApiSchemas.Workers.WorkerDefinition},
    responses: [
      ok: {"WorkerDefinition", "application/json", OpenApiSchemas.Workers.WorkerDefinition},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def create(conn, workflow_params) do
    case WorkerDefinitions.create_worker_definition(workflow_params) do
      {:ok, %WorkerDefinition{} = worker_definition} ->
        conn
        |> put_status(:created)
        |> put_view(StepFlow.WorkerDefinitionView)
        |> render("show.json", worker_definition: worker_definition)

      {:error, changeset} ->
        conn
        |> put_status(:unprocessable_entity)
        |> put_view(StepFlow.ChangesetView)
        |> render("error.json", changeset: changeset)
    end
  end

  operation :show,
    summary: "Get worker definition (identifier)",
    description: "Get a worker definition by identifier",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Worker identifier",
        type: :string,
        example: "my_worker"
      ]
    ],
    responses: [
      ok: {"WorkerDefinition", "application/json", OpenApiSchemas.Workers.WorkerDefinition},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def show(conn, %{"id" => id}) do
    worker_definition = WorkerDefinitions.get_worker_definition!(id)

    conn
    |> put_view(StepFlow.WorkerDefinitionView)
    |> render("show.json", worker_definition: worker_definition)
  end
end
