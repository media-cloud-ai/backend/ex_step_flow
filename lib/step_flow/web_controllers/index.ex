defmodule StepFlow.WebController.Index do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  @moduledoc false

  tags ["Home"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(ExBackendWeb.WebController.Fallback)

  operation :index,
    summary: "StepFlows Home",
    description: "Home entrypoint for StepFlow",
    type: :string,
    responses: [
      ok: "Success",
      forbidden: "Forbidden"
    ]

  def index(conn, _params) do
    {:ok, vsn} = :application.get_key(:step_flow, :vsn)

    conn
    |> put_resp_header("content-type", "application/json; charset=utf-8")
    |> send_resp(
      200,
      %{
        application: "Step Flow",
        version: "#{vsn}"
      }
      |> Jason.encode!()
    )
  end

  operation :not_found, false

  def not_found(conn, _params) do
    send_resp(conn, 404, "Not found")
  end
end
