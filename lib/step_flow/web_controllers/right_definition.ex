defmodule StepFlow.WebController.RightDefinition do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.Rights.Right
  alias StepFlow.WebController.OpenApiSchemas

  @moduledoc false

  tags ["Rights"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(ExBackendWeb.WebController.Fallback)

  operation :index,
    summary: "List rights definition",
    description: "List rights definition",
    type: :object,
    responses: [
      ok: {"RightsDefinition", "application/json", OpenApiSchemas.Rights.RightDefinition},
      forbidden: "Forbidden"
    ]

  def index(conn, _params) do
    rights =
      Right.RightEnum.__valid_values__()
      |> Enum.filter(fn value -> is_bitstring(value) end)

    endpoints =
      Right.EndpointEnum.__valid_values__()
      |> Enum.filter(fn value -> is_bitstring(value) end)

    conn
    |> put_view(StepFlow.RightDefinitionView)
    |> render("right_definition.json", right_definition: %{rights: rights, endpoints: endpoints})
  end
end
