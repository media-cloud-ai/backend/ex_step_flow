defmodule StepFlow.WebController.Durations do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.Controllers.Statistics.Durations
  alias StepFlow.WebController.OpenApiSchemas

  @moduledoc false

  tags ["Durations"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(ExBackendWeb.WebController.Fallback)

  operation :list_jobs_durations,
    summary: "List jobs duration",
    description: "List durations of all jobs (seconds)",
    type: :object,
    responses: [
      ok: {"Durations", "application/json", OpenApiSchemas.Durations.Durations},
      forbidden: "Forbidden"
    ]

  def list_jobs_durations(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    durations =
      params
      |> Map.put("roles", user.roles)
      |> Durations.list_durations_for_jobs()

    conn
    |> put_view(StepFlow.DurationsView)
    |> render("index.json", durations: durations)
  end

  operation :list_workflows_durations,
    summary: "List workflows duration",
    description: "List durations of all workflows",
    type: :object,
    responses: [
      ok: {"Durations", "application/json", OpenApiSchemas.Durations.Durations},
      forbidden: "Forbidden"
    ]

  def list_workflows_durations(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    durations =
      params
      |> Map.put("roles", user.roles)
      |> Durations.list_durations_for_workflows()

    conn
    |> put_view(StepFlow.DurationsView)
    |> render("index.json", durations: durations)
  end
end
