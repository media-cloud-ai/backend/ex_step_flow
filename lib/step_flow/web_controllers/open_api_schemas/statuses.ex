defmodule StepFlow.WebController.OpenApiSchemas.Statuses do
  @moduledoc false

  alias OpenApiSpex.Schema
  alias StepFlow.WebController.OpenApiSchemas.Parameters.Parameters

  defmodule StatusDescription do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "StatusDescription",
      description: "A StepFlow status description",
      type: :object,
      properties: %{
        execution_duration: %Schema{type: :number, description: "Execution duration (seconds)"},
        parameters: Parameters.schema(),
        job_id: %Schema{type: :integer, description: "Job ID"},
        status: %Schema{type: :string, description: "Status"},
        destination_paths: %Schema{
          type: :array,
          description: "List of destination paths",
          items: %Schema{type: :string}
        }
      },
      example: %{
        "destination_paths" => [],
        "execution_duration" => 0.527596715,
        "job_id" => 174_774,
        "parameters" => [],
        "status" => "completed"
      }
    })
  end

  defmodule Status do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Status",
      description: "A StepFlow status",
      type: :object,
      properties: %{
        description: StatusDescription.schema(),
        inserted_at: %Schema{type: :string, description: "Date of insertion"},
        id: %Schema{type: :integer, description: "Status ID"},
        state: %Schema{type: :string, description: "State"}
      },
      example: %{
        "description" => %{
          "destination_paths" => [],
          "execution_duration" => 0.527596715,
          "job_id" => 174_774,
          "parameters" => [],
          "status" => "completed"
        },
        "id" => 266_702,
        "inserted_at" => "2022-10-12T08:12:26",
        "state" => "completed"
      }
    })
  end

  defmodule Statuses do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Statuses",
      description: "A collection of Statuses",
      type: :array,
      items: Status.schema(),
      example: [
        %{
          "description" => %{
            "destination_paths" => [],
            "execution_duration" => 0.527596715,
            "job_id" => 174_774,
            "parameters" => [],
            "status" => "completed"
          },
          "id" => 266_702,
          "inserted_at" => "2022-10-12T08:12:26",
          "state" => "completed"
        }
      ]
    })
  end
end
