defmodule StepFlow.WebController.OpenApiSchemas.Notifications do
  @moduledoc false

  alias OpenApiSpex.Schema

  defmodule NotificationEndpoint do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "NotificationEndpoint",
      description: "A StepFlow Notification Endpoint",
      type: :object,
      properties: %{
        endpoint_placeholder: %Schema{type: :string, description: "Endpoint placeholder"},
        endpoint_url: %Schema{type: :string, description: "Endpoint url"},
        endpoint_credentials: %Schema{type: :string, description: "Endpoint credentials"}
      },
      example: %{
        "endpoint_placeholder" => "Test Hook",
        "endpoint_url" => "https://test.ai",
        "endpoint_credentials" => "xxxxxxxx"
      }
    })
  end

  defmodule NotificationEndpoints do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "NotificationEndpoints",
      description: "A collection of Notification Endpoints",
      type: :array,
      items: NotificationEndpoint.schema(),
      example: [
        %{
          "endpoint_placeholder" => "Test Hook",
          "endpoint_url" => "https://test.ai",
          "endpoint_credentials" => "xxxxxxxx"
        }
      ]
    })
  end

  defmodule NotificationTemplate do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Notification Template",
      description: "A StepFlow Notification Template",
      type: :object,
      properties: %{
        template_name: %Schema{type: :string, description: "Template name"},
        template_headers: %Schema{type: :json, description: "Template headers"},
        template_body: %Schema{type: :json, description: "Template body"}
      },
      example: %{
        "template_name" => "Test template",
        "template_headers" => "[{\"content-type\" : \"application/json\"}]",
        "template_body" => "{}"
      }
    })
  end

  defmodule NotificationTemplates do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Notification Templates",
      description: "A collection of Notification Templates",
      type: :array,
      items: NotificationTemplate.schema(),
      example: [
        %{
          "template_name" => "Test template",
          "template_headers" => "[{\"content-type\" : \"application/json\"}]",
          "template_body" => "{}"
        }
      ]
    })
  end
end
