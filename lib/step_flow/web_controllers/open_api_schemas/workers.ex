defmodule StepFlow.WebController.OpenApiSchemas.Workers do
  @moduledoc false

  alias OpenApiSpex.Schema

  defmodule SystemInfo do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "SystemInformation",
      description: "Worker system information",
      type: :object,
      properties: %{
        docker_container_id: %Schema{type: :string, description: "Docker container ID"},
        number_of_processors: %Schema{type: :integer, description: "Number of processors"},
        total_memory: %Schema{type: :integer, description: "Total memory"},
        total_swap: %Schema{type: :integer, description: "Total swap"},
        used_memory: %Schema{type: :integer, description: "Used memory"},
        used_swap: %Schema{type: :integer, description: "Used swap"}
      },
      example: %{
        "docker_container_id" => "4fd5943e5fba",
        "number_of_processors" => 8,
        "total_memory" => 32_727_478,
        "total_swap" => 0,
        "used_memory" => 1_797_716,
        "used_swap" => 0
      }
    })
  end

  defmodule Worker do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "LiveWorker",
      description: "A StepFlow Live Worker",
      type: :object,
      properties: %{
        activity: %Schema{type: :string, description: "Worker activity"},
        current_job: %Schema{type: :string, description: "Worker current job"},
        description: %Schema{type: :integer, description: "Worker description"},
        direct_messaging_queue_name: %Schema{
          type: :string,
          description: "Direct Messaging queue name"
        },
        inserted_at: %Schema{type: :string, description: "Insertion date"},
        instance_id: %Schema{type: :string, description: "Instance ID"},
        label: %Schema{type: :string, description: "Labels"},
        sdk_version: %Schema{type: :string, description: "Worker SDK version"},
        short_description: %Schema{type: :string, description: "Short worker description"},
        system_info: SystemInfo.schema(),
        updated_at: %Schema{type: :string, description: "Update date"},
        version: %Schema{type: :string, description: "Worker version"}
      },
      example: %{
        "activity" => "Idle",
        "current_job" => nil,
        "description" => "Manipulate files on the storage linked to the worker\n.",
        "direct_messaging_queue_name" => "direct_messaging_4fd5943e5fba",
        "inserted_at" => "2022-10-19T14:39:08Z",
        "instance_id" => "4fd5943e5fba",
        "label" => "File System",
        "queue_name" => "job_file_system",
        "sdk_version" => "1.1.1",
        "short_description" => "Interact with File System: list, copy, delete",
        "system_info" => %{
          "docker_container_id" => "4fd5943e5fba",
          "number_of_processors" => 8,
          "total_memory" => 32_727_478,
          "total_swap" => 0,
          "used_memory" => 1_797_716,
          "used_swap" => 0
        },
        "updated_at" => "2022-10-19T14:39:12Z",
        "version" => "0.4.0"
      }
    })
  end

  defmodule Workers do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Workers",
      description: "A StepFlow Workers",
      type: :array,
      items: Worker.schema(),
      example: [
        %{
          "activity" => "Idle",
          "current_job" => nil,
          "description" => "Manipulate files on the storage linked to the worker\n.",
          "direct_messaging_queue_name" => "direct_messaging_4fd5943e5fba",
          "inserted_at" => "2022-10-19T14:39:08Z",
          "instance_id" => "4fd5943e5fba",
          "label" => "File System",
          "queue_name" => "job_file_system",
          "sdk_version" => "1.1.1",
          "short_description" => "Interact with File System: list, copy, delete",
          "system_info" => %{
            "docker_container_id" => "4fd5943e5fba",
            "number_of_processors" => 8,
            "total_memory" => 32_727_478,
            "total_swap" => 0,
            "used_memory" => 1_797_716,
            "used_swap" => 0
          },
          "updated_at" => "2022-10-19T14:39:12Z",
          "version" => "0.4.0"
        }
      ]
    })
  end

  defmodule LiveWorker do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "LiveWorker",
      description: "A StepFlow Live Worker",
      type: :object,
      properties: %{
        creation_date: %Schema{type: :string, description: "Creation date"},
        direct_messaging_queue_name: %Schema{
          type: :string,
          description: "Direct Messaging queue name"
        },
        id: %Schema{type: :integer, description: "Live Worker ID"},
        inserted_at: %Schema{type: :string, description: "Insertion date"},
        instance_id: %Schema{type: :string, description: "Instance ID"},
        ips: %Schema{type: :array, description: "Instance IPs", items: %Schema{type: :string}},
        ports: %Schema{
          type: :array,
          description: "Instance binded ports",
          items: %Schema{type: :integer}
        },
        termination_date: %Schema{type: :string, description: "Termination date"}
      },
      example: %{
        "creation_date" => "2022-10-06T07:56:26Z",
        "direct_messaging_queue_name" => "93bc17f9-eb28-4a90-9685-e50c334bd53e",
        "id" => 9983,
        "inserted_at" => "2022-10-06T07:56:21",
        "instance_id" => "6efff150ab91",
        "ips" => [
          "10.233.106.15"
        ],
        "ports" => [
          1234,
          6666
        ],
        "termination_date" => nil
      }
    })
  end

  defmodule LiveWorkers do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "LiveWorkers",
      description: "A StepFlow Live Workers",
      type: :array,
      items: LiveWorker.schema(),
      example: [
        %{
          "creation_date" => "2022-10-06T07:56:26Z",
          "direct_messaging_queue_name" => "93bc17f9-eb28-4a90-9685-e50c334bd53e",
          "id" => 9983,
          "inserted_at" => "2022-10-06T07:56:21",
          "instance_id" => "6efff150ab91",
          "ips" => [
            "10.233.106.15"
          ],
          "ports" => [
            1234,
            6666
          ],
          "termination_date" => nil
        }
      ]
    })
  end

  defmodule WorkerParameters do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkerParameters",
      description: "A StepFlow Worker Parameters",
      type: :object,
      properties: %{
        additionalProperties: %Schema{type: :object, description: "Worker additional properties"},
        properties: %Schema{type: :object, description: "Worker properties"},
        required: %Schema{type: :string, description: "Required"},
        title: %Schema{type: :string, description: "Parameter title"}
      },
      example: %{
        "$schema" => "http://json-schema.org/draft-07/schema#",
        "additionalProperties" => %{
          "type" => "string"
        },
        "properties" => %{
          "command_template" => %{
            "type" => "string"
          },
          "destination_paths" => %{
            "default" => [],
            "items" => %{
              "type" => "string"
            },
            "type" => "array"
          },
          "exec_dir" => %{
            "type" => [
              "string",
              "nil"
            ]
          },
          "requirements" => %{
            "additionalProperties" => %{
              "items" => %{
                "type" => "string"
              },
              "type" => "array"
            },
            "type" => [
              "object",
              "nil"
            ]
          },
          "source_paths" => %{
            "default" => [],
            "items" => %{
              "type" => "string"
            },
            "type" => "array"
          }
        },
        "required" => [
          "command_template"
        ],
        "title" => "CommandLineWorkerParameters",
        "type" => "object"
      }
    })
  end

  defmodule WorkerDefinition do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkerDefinition",
      description: "A StepFlow Worker Definition",
      type: :object,
      properties: %{
        created_at: %Schema{type: :string, description: "Workers activity"},
        description: %Schema{type: :integer, description: "Workers description"},
        id: %Schema{type: :integer, description: "Worker Definition ID"},
        label: %Schema{type: :string, description: "Labels"},
        parameters: WorkerParameters.schema(),
        queue_name: %Schema{type: :string, description: "Workers queue name"},
        short_description: %Schema{type: :string, description: "Short workers description"},
        version: %Schema{type: :string, description: "Workers version"}
      },
      example: %{
        "created_at" => "2022-05-30T15:52:15",
        "description" =>
          "Run any command line available in the command line of the worker.\nProvide a template parameter, other parameters will be replaced before running.",
        "id" => 64,
        "label" => "Command Line",
        "parameters" => %{
          "$schema" => "http://json-schema.org/draft-07/schema#",
          "additionalProperties" => %{
            "type" => "string"
          },
          "properties" => %{
            "command_template" => %{
              "type" => "string"
            },
            "destination_paths" => %{
              "default" => [],
              "items" => %{
                "type" => "string"
              },
              "type" => "array"
            },
            "exec_dir" => %{
              "type" => [
                "string",
                "nil"
              ]
            },
            "requirements" => %{
              "additionalProperties" => %{
                "items" => %{
                  "type" => "string"
                },
                "type" => "array"
              },
              "type" => [
                "object",
                "nil"
              ]
            },
            "source_paths" => %{
              "default" => [],
              "items" => %{
                "type" => "string"
              },
              "type" => "array"
            }
          },
          "required" => [
            "command_template"
          ],
          "title" => "CommandLineWorkerParameters",
          "type" => "object"
        },
        "queue_name" => "job_my_worker",
        "short_description" => "Execute command lines on workers",
        "version" => "0.2.2"
      }
    })
  end

  defmodule WorkerDefinitions do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkerDefinitions",
      description: "A StepFlow Worker Definitions",
      type: :array,
      items: WorkerDefinition.schema(),
      example: [
        %{
          "created_at" => "2022-05-30T15:52:15",
          "description" =>
            "Run any command line available in the command line of the worker.\nProvide a template parameter, other parameters will be replaced before running.",
          "id" => 64,
          "label" => "Command Line",
          "parameters" => %{
            "$schema" => "http://json-schema.org/draft-07/schema#",
            "additionalProperties" => %{
              "type" => "string"
            },
            "properties" => %{
              "command_template" => %{
                "type" => "string"
              },
              "destination_paths" => %{
                "default" => [],
                "items" => %{
                  "type" => "string"
                },
                "type" => "array"
              },
              "exec_dir" => %{
                "type" => [
                  "string",
                  "nil"
                ]
              },
              "requirements" => %{
                "additionalProperties" => %{
                  "items" => %{
                    "type" => "string"
                  },
                  "type" => "array"
                },
                "type" => [
                  "object",
                  "nil"
                ]
              },
              "source_paths" => %{
                "default" => [],
                "items" => %{
                  "type" => "string"
                },
                "type" => "array"
              }
            },
            "required" => [
              "command_template"
            ],
            "title" => "CommandLineWorkerParameters",
            "type" => "object"
          },
          "queue_name" => "job_my_worker",
          "short_description" => "Execute command lines on workers",
          "version" => "0.2.2"
        }
      ]
    })
  end
end
