defmodule StepFlow.WebController.OpenApiSchemas.Jobs do
  @moduledoc false

  alias OpenApiSpex.Schema
  alias StepFlow.WebController.OpenApiSchemas.Parameters.Parameters
  alias StepFlow.WebController.OpenApiSchemas.Progressions.Progressions
  alias StepFlow.WebController.OpenApiSchemas.Statuses.Statuses

  defmodule Job do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Job",
      description: "A StepFlow job",
      type: :object,
      properties: %{
        id: %Schema{type: :integer, description: "Jobs ID"},
        inserted_at: %Schema{type: :string, description: "Date of insertion"},
        last_worker_instance_id: %Schema{type: :string, description: "Last worker instance ID"},
        name: %Schema{type: :string, description: "Job name"},
        params: Parameters.schema(),
        progressions: Progressions.schema(),
        status: Statuses.schema(),
        step_id: %Schema{type: :integer, description: "Step ID related to the job"},
        updated_at: %Schema{type: :string, description: "Date of last update"},
        workflow_id: %Schema{type: :integer, description: "Workflow ID related to the job"}
      },
      example: %{
        "id" => 174_775,
        "inserted_at" => "2022-10-12T08:12:17",
        "last_worker_instance_id" => "",
        "name" => "job_transfer",
        "params" => [
          %{
            "id" => "source_path",
            "type" => "string",
            "value" => "/data/21313/my_file"
          },
          %{
            "id" => "requirements",
            "type" => "requirements",
            "value" => %{
              "paths" => []
            }
          },
          %{
            "id" => "destination_paths",
            "type" => "array_of_strings",
            "value" => []
          }
        ]
      }
    })
  end

  defmodule Jobs do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Jobs",
      description: "A collection of Jobs",
      type: :array,
      items: Job.schema(),
      example: [
        %{
          "id" => 174_775,
          "inserted_at" => "2022-10-12T08:12:17",
          "last_worker_instance_id" => "",
          "name" => "job_transfer",
          "params" => [
            %{
              "id" => "source_path",
              "type" => "string",
              "value" => "/data/21313/my_file"
            },
            %{
              "id" => "requirements",
              "type" => "requirements",
              "value" => %{
                "paths" => []
              }
            },
            %{
              "id" => "destination_paths",
              "type" => "array_of_strings",
              "value" => []
            }
          ]
        }
      ]
    })
  end
end
