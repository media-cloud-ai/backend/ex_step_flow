defmodule StepFlow.WebController.OpenApiSchemas.Rights do
  @moduledoc false

  alias OpenApiSpex.Schema

  defmodule Right do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Right",
      description: "A StepFlow right",
      type: :object,
      properties: %{
        action: %Schema{
          type: :array,
          description: "List of actions",
          items: %Schema{type: :string}
        },
        entity: %Schema{type: :string, description: "Entity the rights apply to"}
      },
      example: %{
        "action" => [
          "create",
          "delete",
          "view"
        ],
        "entity" => "workflow::*"
      }
    })
  end

  defmodule Rights do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Rights",
      description: "A collection of Rights",
      type: :array,
      items: Right.schema(),
      example: [
        %{
          "action" => [
            "create",
            "delete",
            "view"
          ],
          "entity" => "workflow::*"
        }
      ]
    })
  end

  defmodule RightDefinition do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "RightsDefinition",
      description: "A list of rights and endpoints",
      type: :object,
      properties: %{
        rights: %Schema{
          type: :array,
          description: "List of rights",
          items: %Schema{type: :string}
        },
        endpoints: %Schema{
          type: :array,
          description: "List of endpoints",
          items: %Schema{type: :string}
        }
      },
      example: %{
        "rights" => [
          "abort",
          "create",
          "delete",
          "retry",
          "stop",
          "update",
          "view"
        ],
        "endpoints" => [
          "endpoint::jobs",
          "endpoint::live_workers",
          "endpoint::metrics",
          "endpoint::statistics",
          "endpoint::workers",
          "endpoint::worker_definitions",
          "endpoint::workflow",
          "endpoint::workflow_definition"
        ]
      }
    })
  end
end
