defmodule StepFlow.WebController.OpenApiSchemas.Workflows do
  @moduledoc false

  alias OpenApiSpex.Schema
  alias StepFlow.WebController.OpenApiSchemas.Jobs.Jobs
  alias StepFlow.WebController.OpenApiSchemas.Notifications.NotificationEndpoints
  alias StepFlow.WebController.OpenApiSchemas.Parameters.Parameters
  alias StepFlow.WebController.OpenApiSchemas.Parameters.StartParameters
  alias StepFlow.WebController.OpenApiSchemas.Statuses.Status

  defmodule Artifact do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Artifact",
      description: "A StepFlow workflow artifact",
      type: :object,
      properties: %{
        id: %Schema{type: :integer, description: "Artefact ID"},
        inserted_at: %Schema{type: :string, description: "Date of insertion"},
        resources: %Schema{type: :string, description: "Resources"}
      },
      example: %{
        "id" => 12_539,
        "inserted_at" => "2022-10-19T12:13:33",
        "resources" => %{}
      }
    })
  end

  defmodule Artifacts do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Artifacts",
      description: "A collection of Artifacts",
      type: :array,
      items: Artifact.schema(),
      example: [
        %{
          "id" => 12_539,
          "inserted_at" => "2022-10-19T12:13:33",
          "resources" => %{}
        }
      ]
    })
  end

  defmodule Workflow do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Workflow",
      description: "A StepFlow workflow",
      type: :object,
      properties: %{
        artifacts: Artifacts.schema(),
        created_at: %Schema{type: :string, description: "Date of creation"},
        deleted: %Schema{type: :boolean, description: "Is the workflow deleted"},
        id: %Schema{type: :integer, description: "Workflow ID"},
        identifier: %Schema{type: :string, description: "Workflow identifier"},
        is_live: %Schema{type: :boolean, description: "Is the workflow live"},
        jobs: Jobs.schema(),
        notification_hooks: NotificationEndpoints.schema(),
        parameters: Parameters.schema(),
        reference: %Schema{type: :string, description: "Workflow reference"},
        schema_version: %Schema{type: :string, description: "Workflow schema version"},
        status: Status.schema(),
        # steps: Steps.schema(),
        version_major: %Schema{type: :integer, description: "Workflow major version"},
        version_micro: %Schema{type: :integer, description: "Workflow micro version"},
        version_minor: %Schema{type: :integer, description: "Workflow minor version"},
        user_uuid: %Schema{
          type: :string,
          description: "UUID of the user who has created the workflow"
        },
        tags: %Schema{type: :array, description: "List of tags", items: %Schema{type: :string}}
      },
      example: %{
        "artifacts" => [],
        "created_at" => "2022-10-12T08:12:26",
        "deleted" => false,
        "id" => 5536,
        "identifier" => "my_workflow",
        "is_live" => false,
        "jobs" => [
          %{
            "id" => 174_775,
            "inserted_at" => "2022-10-12T08:12:17",
            "last_worker_instance_id" => "",
            "name" => "job_transfer",
            "params" => [
              %{
                "id" => "source_path",
                "type" => "string",
                "value" => "/data/21313/my_file"
              },
              %{
                "id" => "requirements",
                "type" => "requirements",
                "value" => %{
                  "paths" => []
                }
              },
              %{
                "id" => "destination_paths",
                "type" => "array_of_strings",
                "value" => []
              }
            ],
            "progressions" => [
              %{
                "datetime" => "2022-10-12T08:12:26Z",
                "docker_container_id" => "9cadac8db3f2",
                "id" => 11_434_918,
                "job_id" => 174_775,
                "progression" => 100
              }
            ],
            "status" => [
              %{
                "description" => %{
                  "destination_paths" => [],
                  "execution_duration" => 0.569668866,
                  "job_id" => 174_775,
                  "parameters" => [],
                  "status" => "completed"
                },
                "id" => 266_703,
                "inserted_at" => "2022-10-12T08:12:26",
                "state" => "completed"
              }
            ],
            "step_id" => 5,
            "updated_at" => "2022-10-12T08:12:26",
            "workflow_id" => 21_313
          }
        ],
        "notification_hooks" => [],
        "parameters" => [
          %{
            "id" => "source",
            "type" => "string"
          }
        ],
        "reference" => "My Workflow",
        "schema_version" => "1.9",
        "status" => [
          %{
            "description" => %{},
            "id" => 258_649,
            "inserted_at" => "2022-10-12T08:12:26",
            "state" => "pending"
          }
        ],
        "steps" => [
          %{
            "jobs" => %{
              "completed" => 1,
              "errors" => 0,
              "paused" => 0,
              "processing" => 0,
              "queued" => 0,
              "skipped" => 0,
              "stopped" => 0,
              "total" => 1
            },
            "status" => "completed",
            "icon" => "file_download",
            "id" => 0,
            "label" => "Download source elements",
            "name" => "job_transfer",
            "parameters" => [
              %{
                "id" => "source_paths",
                "type" => "array_of_templates",
                "value" => [
                  "{video_source_filename}"
                ]
              }
            ]
          }
        ],
        "tags" => [
          "my",
          "workflow"
        ],
        "user_uuid" => "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
        "version_major" => 1,
        "version_micro" => 8,
        "version_minor" => 1
      }
    })
  end

  defmodule WorkflowRedux do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Workflow",
      description: "A StepFlow workflow",
      type: :object,
      properties: %{
        id: %Schema{type: :integer, description: "Workflow ID"},
        identifier: %Schema{type: :string, description: "Workflow identifier"},
        version_major: %Schema{type: :integer, description: "Workflow major version"},
        version_micro: %Schema{type: :integer, description: "Workflow micro version"},
        version_minor: %Schema{type: :integer, description: "Workflow minor version"},
        tags: %Schema{type: :array, description: "List of tags", items: %Schema{type: :string}}
      },
      example: %{
        "id" => 5536,
        "identifier" => "my_workflow",
        "tags" => [
          "my",
          "workflow"
        ],
        "version_major" => 1,
        "version_micro" => 8,
        "version_minor" => 1
      }
    })
  end

  defmodule Workflows do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Workflows",
      description: "A collection of Workflows",
      type: :array,
      items: Workflow.schema(),
      example: [
        %{
          "artifacts" => [],
          "created_at" => "2022-10-12T08:12:26",
          "deleted" => false,
          "id" => 5536,
          "identifier" => "my_workflow",
          "is_live" => false,
          "jobs" => [
            %{
              "id" => 174_775,
              "inserted_at" => "2022-10-12T08:12:17",
              "last_worker_instance_id" => "",
              "name" => "job_transfer",
              "params" => [
                %{
                  "id" => "source_path",
                  "type" => "string",
                  "value" => "/data/21313/my_file"
                },
                %{
                  "id" => "requirements",
                  "type" => "requirements",
                  "value" => %{
                    "paths" => []
                  }
                },
                %{
                  "id" => "destination_paths",
                  "type" => "array_of_strings",
                  "value" => []
                }
              ],
              "progressions" => [
                %{
                  "datetime" => "2022-10-12T08:12:26Z",
                  "docker_container_id" => "9cadac8db3f2",
                  "id" => 11_434_918,
                  "job_id" => 174_775,
                  "progression" => 100
                }
              ],
              "status" => [
                %{
                  "description" => %{
                    "destination_paths" => [],
                    "execution_duration" => 0.569668866,
                    "job_id" => 174_775,
                    "parameters" => [],
                    "status" => "completed"
                  },
                  "id" => 266_703,
                  "inserted_at" => "2022-10-12T08:12:26",
                  "state" => "completed"
                }
              ],
              "step_id" => 5,
              "updated_at" => "2022-10-12T08:12:26",
              "workflow_id" => 21_313
            }
          ],
          "notification_hooks" => [],
          "parameters" => [
            %{
              "id" => "source",
              "type" => "string"
            }
          ],
          "reference" => "My Workflow",
          "schema_version" => "1.9",
          "status" => [
            %{
              "description" => %{},
              "id" => 258_649,
              "inserted_at" => "2022-10-12T08:12:26",
              "state" => "pending"
            }
          ],
          "steps" => [
            %{
              "jobs" => %{
                "completed" => 1,
                "errors" => 0,
                "paused" => 0,
                "processing" => 0,
                "queued" => 0,
                "skipped" => 0,
                "stopped" => 0,
                "total" => 1
              },
              "status" => "completed",
              "icon" => "file_download",
              "id" => 0,
              "label" => "Download source elements",
              "name" => "job_transfer",
              "parameters" => [
                %{
                  "id" => "source_paths",
                  "type" => "array_of_templates",
                  "value" => [
                    "{video_source_filename}"
                  ]
                }
              ]
            }
          ],
          "tags" => [
            "my",
            "workflow"
          ],
          "user_uuid" => "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
          "version_major" => 1,
          "version_micro" => 8,
          "version_minor" => 1
        }
      ]
    })
  end

  defmodule WorkflowParams do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkflowParameters",
      description: "Parameters to create a workflow",
      type: :object,
      properties: %{
        workflow_identifier: %Schema{
          type: :string,
          description: "Workflow identifier"
        },
        parameters: Parameters.schema(),
        reference: %Schema{type: :string, description: "Workflow reference"},
        version_major: %Schema{type: :string, description: "Workflow major version"},
        version_minor: %Schema{type: :string, description: "Workflow minor version"},
        version_micro: %Schema{type: :string, description: "Workflow micro version"}
      },
      example: %{
        "workflow_identifier" => "my_workflow",
        "parameters" => %{
          "source_filename" => "toto.mp4"
        },
        "reference" => "Demo Workflow",
        "version_major" => 1,
        "version_minor" => 1,
        "version_micro" => 1
      }
    })
  end

  defmodule StatisticsParams do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "StatisticsParameters",
      description: "Parameters to get workflow statistics (seconds)",
      type: :object,
      properties: %{
        start_date: %Schema{type: :string, description: "Workflow after date"},
        end_date: %Schema{type: :string, description: "Workflow before date"},
        time_interval: %Schema{type: :integer, description: "Statistic time interval"},
        identifiers: %Schema{
          type: :array,
          description: "Workflow identifier",
          items: %Schema{type: :string}
        }
      },
      example: %{
        "start_date" => "2022-10-10T16:46:17",
        "end_date" => "2022-10-19T16:46:17",
        "time_interval" => 1,
        "identifiers" => []
      }
    })
  end

  defmodule WorkflowDefinition do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkflowDefinition",
      description: "A StepFlow workflow definition",
      type: :object,
      properties: %{
        icon: %Schema{type: :string, description: "Workflow icon"},
        id: %Schema{type: :integer, description: "Workflow ID"},
        identifier: %Schema{type: :string, description: "Workflow identifier"},
        is_live: %Schema{type: :boolean, description: "Is the workflow live"},
        label: %Schema{type: :string, description: "Workflow label"},
        parameters: Parameters.schema(),
        schema_version: %Schema{type: :string, description: "Workflow schema version"},
        start_parameters: StartParameters.schema(),
        version_major: %Schema{type: :integer, description: "Workflow major version"},
        version_micro: %Schema{type: :integer, description: "Workflow micro version"},
        version_minor: %Schema{type: :integer, description: "Workflow minor version"},
        tags: %Schema{type: :array, description: "List of tags", items: %Schema{type: :string}}
      },
      example: %{
        "icon" => "photo_size_select_small",
        "id" => 5536,
        "identifier" => "my_workflow",
        "is_live" => false,
        "label" => "My Workflow",
        "parameters" => [
          %{
            "id" => "source",
            "type" => "string"
          }
        ],
        "schema_version" => "1.9",
        "start_parameters" => [
          %{
            "accept" => ".mp4",
            "id" => "source",
            "label" => "Video Source file",
            "type" => "file"
          }
        ],
        "tags" => [
          "my",
          "workflow"
        ],
        "version_major" => 1,
        "version_micro" => 8,
        "version_minor" => 1
      }
    })
  end

  defmodule WorkflowDefinitions do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkflowDefinitions",
      description: "A collection of Workflow Definitions",
      type: :array,
      items: WorkflowDefinition.schema(),
      example: [
        %{
          "icon" => "photo_size_select_small",
          "id" => 5536,
          "identifier" => "my_workflow",
          "is_live" => false,
          "label" => "My Workflow",
          "parameters" => [
            %{
              "id" => "source",
              "type" => "string"
            }
          ],
          "schema_version" => "1.9",
          "start_parameters" => [
            %{
              "accept" => ".mp4",
              "id" => "source",
              "label" => "Video Source file",
              "type" => "file"
            }
          ],
          "tags" => [
            "my",
            "workflow"
          ],
          "version_major" => 1,
          "version_micro" => 8,
          "version_minor" => 1
        }
      ]
    })
  end

  defmodule EventBody do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "EventBody",
      description: "A StepFlow event body",
      type: :object,
      properties: %{
        event: %Schema{
          type: :array,
          description: "Event type",
          enum: [:abort, :pause, :resume, :update, :retry, :stop, :delete]
        }
      },
      example: %{
        "event" => "stop"
      }
    })
  end
end
