defmodule StepFlow.WebController.OpenApiSchemas.Statistics do
  @moduledoc false

  alias OpenApiSpex.Schema
  alias StepFlow.WebController.OpenApiSchemas.Durations.Duration

  defmodule Bin do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "DataBin",
      description: "Data bin covering given timerange",
      type: :object,
      properties: %{
        bin: %Schema{type: :integer, description: "Workflows in bin"},
        completed: %Schema{type: :integer, description: "Completed workflows"},
        error: %Schema{type: :integer, description: "Workflows in error"},
        pending: %Schema{type: :integer, description: "Pending workflows"},
        processing: %Schema{type: :integer, description: "Processing workflows"},
        end_date: %Schema{type: :string, description: "Timerange end"},
        start_date: %Schema{type: :string, description: "Timerange start"}
      },
      example: %{
        "bin" => 2,
        "completed" => 0,
        "end_date" => "2022-10-13 14:56:36",
        "error" => 1,
        "pending" => 1,
        "processing" => 0,
        "start_date" => "2022-10-13 14:32:36"
      }
    })
  end

  defmodule Bins do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Bins",
      description: "A collection of Bins",
      type: :array,
      items: Bin.schema(),
      example: [
        %{
          "bin" => 2,
          "completed" => 0,
          "end_date" => "2022-10-13 14:56:36",
          "error" => 1,
          "pending" => 1,
          "processing" => 0,
          "start_date" => "2022-10-13 14:32:36"
        }
      ]
    })
  end

  defmodule DurationStatistics do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "DurationStatistics",
      description: "Duration statistics (seconds)",
      type: :object,
      properties: %{
        average: Duration.schema(),
        max: Duration.schema(),
        min: Duration.schema(),
        count: %Schema{type: :integer, description: "Number of elements"}
      },
      example: %{
        "average" => %{
          "order_pending" => 3_904.333,
          "processing" => 50.355,
          "response_pending" => 30.833,
          "total" => 3_736.0
        },
        "count" => 6,
        "max" => %{
          "order_pending" => 16_459.0,
          "processing" => 109.129,
          "response_pending" => 51.0,
          "total" => 16_480.0
        },
        "min" => %{
          "order_pending" => 321.0,
          "processing" => 15.0,
          "response_pending" => 0.0,
          "total" => 282.0
        }
      }
    })
  end

  defmodule WorkflowStatistics do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkflowsStatistics",
      description: "A StepFlow statistics of workflows",
      type: :object,
      properties: %{
        completed: %Schema{type: :integer, description: "Completed workflows"},
        error: %Schema{type: :integer, description: "Workflows in error"},
        pending: %Schema{type: :integer, description: "Pending workflows"},
        processing: %Schema{type: :integer, description: "Processing workflows"},
        bins: Bins.schema()
      },
      example: %{
        "bins" => [
          %{
            "bin" => 2,
            "completed" => 0,
            "end_date" => "2022-10-13 14:56:36",
            "error" => 1,
            "pending" => 1,
            "processing" => 0,
            "start_date" => "2022-10-13 14:32:36"
          }
        ],
        "completed" => 0,
        "error" => 6,
        "pending" => 3,
        "processing" => 0
      }
    })
  end

  defmodule WorkflowsDurationStatistics do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "WorkflowsDurationStatistics",
      description: "Duration statistics for workflows (seconds)",
      type: :object,
      properties: %{
        duration: DurationStatistics.schema(),
        name: %Schema{type: :string, description: "Workflows name"},
        version: %Schema{type: :string, description: "Workflows version"}
      },
      example: %{
        "durations" => %{
          "average" => %{
            "order_pending" => 3_904.333,
            "processing" => 50.355,
            "response_pending" => 30.833,
            "total" => 3_736.0
          },
          "count" => 6,
          "max" => %{
            "order_pending" => 16_459.0,
            "processing" => 109.129,
            "response_pending" => 51.0,
            "total" => 16_480.0
          },
          "min" => %{
            "order_pending" => 321.0,
            "processing" => 15.0,
            "response_pending" => 0.0,
            "total" => 282.0
          }
        },
        "name" => "workflow",
        "version" => "1.1.1"
      }
    })
  end

  defmodule JobsDurationStatistics do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "JobsDurationStatistics",
      description: "Duration statistics for jobs (seconds)",
      type: :object,
      properties: %{
        duration: DurationStatistics.schema(),
        name: %Schema{type: :string, description: "Jobs name"}
      },
      example: %{
        "durations" => %{
          "average" => %{
            "order_pending" => 3_904.333,
            "processing" => 50.355,
            "response_pending" => 30.833,
            "total" => 3_736.0
          },
          "count" => 6,
          "max" => %{
            "order_pending" => 16_459.0,
            "processing" => 109.129,
            "response_pending" => 51.0,
            "total" => 16_480.0
          },
          "min" => %{
            "order_pending" => 321.0,
            "processing" => 15.0,
            "response_pending" => 0.0,
            "total" => 282.0
          }
        },
        "name" => "job"
      }
    })
  end
end
