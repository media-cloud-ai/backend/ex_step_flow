defmodule StepFlow.WebController.OpenApiSchemas.Durations do
  @moduledoc false

  alias OpenApiSpex.Schema

  defmodule Duration do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Duration",
      description: "A StepFlow duration (seconds)",
      type: :object,
      properties: %{
        workflow_id: %Schema{type: :integer, description: "Workflow ID"},
        job_id: %Schema{type: :integer, description: "Job ID"},
        order_pending: %Schema{type: :number, description: "Pending duration during order"},
        processing: %Schema{type: :number, description: "Processing duration"},
        response_pending: %Schema{type: :number, description: "Pending duration during response"},
        total: %Schema{type: :number, description: "Total duration"}
      },
      example: %{
        "job_id" => nil,
        "order_pending" => 616.0,
        "processing" => 864.196,
        "response_pending" => 11.0,
        "total" => 1_433,
        "workflow_id" => 21_374
      }
    })
  end

  defmodule Durations do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Durations",
      description: "A collection of Durations",
      type: :array,
      items: Duration.schema(),
      example: [
        %{
          "job_id" => nil,
          "order_pending" => 616.0,
          "processing" => 864.196,
          "response_pending" => 11.0,
          "total" => 1_433,
          "workflow_id" => 21_374
        }
      ]
    })
  end
end
