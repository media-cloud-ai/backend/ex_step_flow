defmodule StepFlow.WebController.OpenApiSchemas.Roles do
  @moduledoc false

  alias OpenApiSpex.Schema
  alias StepFlow.WebController.OpenApiSchemas.Rights.Rights

  defmodule Role do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Role",
      description: "A StepFlow role",
      type: :object,
      properties: %{
        id: %Schema{type: :integer, description: "Role ID"},
        name: %Schema{type: :string, description: "Role name"},
        rights: Rights.schema(),
        is_unused: %Schema{type: :boolean, description: "Is the role used"}
      },
      example: %{
        "id" => 2,
        "is_unused" => true,
        "name" => "editor",
        "rights" => [
          %{
            "action" => [
              "create",
              "delete",
              "view"
            ],
            "entity" => "workflow::*"
          },
          %{
            "action" => [
              "view"
            ],
            "entity" => "workers::*"
          }
        ]
      }
    })
  end

  defmodule Roles do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Roles",
      description: "A collection of Roles",
      type: :array,
      items: Role.schema(),
      example: [
        %{
          "id" => 2,
          "is_unused" => true,
          "name" => "editor",
          "rights" => [
            %{
              "action" => [
                "create",
                "delete",
                "view"
              ],
              "entity" => "workflow::*"
            },
            %{
              "action" => [
                "view"
              ],
              "entity" => "workers::*"
            }
          ]
        }
      ]
    })
  end
end
