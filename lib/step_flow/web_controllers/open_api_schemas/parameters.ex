defmodule StepFlow.WebController.OpenApiSchemas.Parameters do
  @moduledoc false

  alias OpenApiSpex.Schema

  defmodule Parameter do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Parameter",
      description: "A StepFlow parameter",
      type: :object,
      properties: %{
        id: %Schema{type: :string, description: "Parameter name"},
        type: %Schema{type: :string, description: "Parameter type"},
        value: %Schema{
          type: :string,
          description: "Parameter value (type depends on parameter type)"
        }
      },
      example: %{
        "id" => "source",
        "type" => "string",
        "value" => "example"
      }
    })
  end

  defmodule Parameters do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Parameters",
      description: "A collection of Parameters",
      type: :array,
      items: Parameter.schema(),
      example: [
        %{
          "id" => "source",
          "type" => "string",
          "value" => "toto"
        }
      ]
    })
  end

  defmodule StartParameter do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Startparameter",
      description: "A StepFlow start parameter",
      type: :object,
      properties: %{
        accept: %Schema{type: :string, description: "File extensions"},
        id: %Schema{type: :string, description: "Parameter name"},
        label: %Schema{type: :string, description: "Parameter label"},
        type: %Schema{type: :string, description: "Parameter type of object"}
      },
      example: %{
        "accept" => ".mp4",
        "id" => "source",
        "label" => "file",
        "type" => "file"
      }
    })
  end

  defmodule StartParameters do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "StartParameters",
      description: "A collection of StartParameters",
      type: :array,
      items: StartParameter.schema(),
      example: [
        %{
          "id" => "source",
          "type" => "string",
          "value" => "toto"
        }
      ]
    })
  end
end
