defmodule StepFlow.WebController.OpenApiSchemas.Progressions do
  @moduledoc false

  alias OpenApiSpex.Schema

  defmodule Progression do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Progression",
      description: "A StepFlow progression",
      type: :object,
      properties: %{
        datetime: %Schema{type: :string, description: "Progression timestamp"},
        docker_container_id: %Schema{type: :string, description: "Docker container ID"},
        id: %Schema{type: :integer, description: "Progression ID"},
        job_id: %Schema{type: :integer, description: "Job ID"},
        progression: %Schema{type: :integer, description: "Progression"}
      },
      example: %{
        "datetime" => "2022-10-12T08:12:25Z",
        "docker_container_id" => "9cadac8db3f2",
        "id" => 11_434_910,
        "job_id" => 174_774,
        "progression" => 100
      }
    })
  end

  defmodule Progressions do
    @moduledoc false
    require OpenApiSpex

    OpenApiSpex.schema(%{
      title: "Progressions",
      description: "A collection of Progressions",
      type: :array,
      items: Progression.schema()
    })
  end
end
