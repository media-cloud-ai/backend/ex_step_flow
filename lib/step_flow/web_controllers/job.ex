defmodule StepFlow.WebController.Job do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.Jobs
  alias StepFlow.Repo
  alias StepFlow.WebController.Helpers
  alias StepFlow.WebController.OpenApiSchemas
  alias StepFlow.Workflows

  @moduledoc false

  tags ["Jobs"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(ExBackendWeb.WebController.Fallback)

  operation :index,
    summary: "List jobs",
    description: "List jobs",
    type: :object,
    parameters: [
      workflow_id: [
        in: :query,
        description: "Workflow identifier",
        type: :string,
        example: "my_workflow"
      ],
      job_type: [
        in: :query,
        description: "Job type",
        type: :string,
        example: "transfer"
      ],
      name: [
        in: :query,
        description: "Job name",
        type: :string,
        example: "my_job"
      ],
      step_id: [
        in: :query,
        description: "Step ID",
        type: :integer,
        example: 2
      ],
      status: [
        in: :query,
        description: "Status",
        type: :string,
        example: "completed"
      ]
    ],
    responses: [
      ok: {"Jobs", "application/json", OpenApiSchemas.Jobs.Jobs},
      forbidden: "Forbidden"
    ]

  def index(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    jobs =
      params
      |> Map.put("roles", user.roles)
      |> Jobs.list_jobs()

    conn
    |> put_view(StepFlow.JobView)
    |> render("index.json", jobs: jobs)
  end

  def index(conn, _) do
    conn
    |> put_status(:forbidden)
    |> put_view(StepFlow.JobView)
    |> render("error.json",
      errors: %{reason: "Forbidden to view jobs."}
    )
  end

  operation :show,
    summary: "Show job",
    description: "Show job by id",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Job ID",
        type: :integer,
        example: 1
      ]
    ],
    responses: [
      ok: {"Job", "application/json", OpenApiSchemas.Jobs.Job},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def show(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    job =
      Jobs.get_job!(id)
      |> Repo.preload([:status, child_workflow: [:jobs, :artifacts, :status]])

    workflow = Workflows.get_workflow_for_job!(id)

    if Helpers.has_right?("workflow::" <> workflow.identifier, user, "view") do
      conn
      |> put_view(StepFlow.JobView)
      |> render("show.json", job: job)
    else
      conn
      |> put_status(:forbidden)
      |> put_view(StepFlow.JobView)
      |> render("error.json",
        errors: %{reason: "Forbidden to view this job."}
      )
    end
  end

  def show(conn, _) do
    conn
    |> put_status(:forbidden)
    |> put_view(StepFlow.WorkflowDefinitionView)
    |> render("error.json",
      errors: %{reason: "Forbidden to show workflow with this identifier"}
    )
  end

  operation :create, false
end
