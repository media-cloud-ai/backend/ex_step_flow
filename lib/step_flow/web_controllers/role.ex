defmodule StepFlow.WebController.Role do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.Roles
  alias StepFlow.WebController.Helpers
  alias StepFlow.WebController.OpenApiSchemas

  @moduledoc false

  tags ["Roles"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(ExBackendWeb.WebController.Fallback)

  defp forbidden(conn) do
    conn
    |> put_status(:forbidden)
    |> json(%{status: "error", message: "Forbidden access to roles"})
  end

  operation :index,
    summary: "List roles",
    description: "List roles",
    type: :object,
    responses: [
      ok: {"Roles", "application/json", OpenApiSchemas.Roles.Roles},
      forbidden: "Forbidden"
    ]

  def index(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    if Helpers.has_right?("roles::*", user, "view") do
      roles = Roles.list_roles(params)

      conn
      |> put_view(StepFlow.RoleView)
      |> render("index.json", roles: roles)
    else
      conn
      |> forbidden()
    end
  end

  def index(conn, _) do
    conn
    |> forbidden()
  end

  defp show_per_id(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    role = Roles.get_role(id)

    if role != nil && Helpers.has_right?("roles::" <> role.name, user, "view") do
      conn
      |> put_view(StepFlow.RoleView)
      |> render("show.json", role: role)
    else
      conn
      |> forbidden()
    end
  end

  defp show_per_name(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => name}) do
    if Helpers.has_right?("roles::" <> name, user, "view") do
      role = Roles.get_by(%{"name" => name})

      conn
      |> put_view(StepFlow.RoleView)
      |> render("show.json", role: role)
    else
      conn
      |> forbidden()
    end
  end

  operation :show,
    summary: "Show role",
    description: "Show role by id or by name",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Role ID or name (string)",
        type: :integer,
        example: 1
      ]
    ],
    responses: [
      ok: {"Role", "application/json", OpenApiSchemas.Roles.Role},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def show(%Plug.Conn{assigns: %{current_user: _user}} = conn, %{"id" => identifier}) do
    case Integer.parse(identifier) do
      {id, ""} -> show_per_id(conn, %{"id" => id})
      _ -> show_per_name(conn, %{"id" => identifier})
    end
  end

  def show(conn, _) do
    conn
    |> forbidden()
  end

  operation :create, false

  def create(%Plug.Conn{assigns: %{current_user: user}} = conn, param) do
    if Helpers.has_right?("roles::*", user, "create") do
      {:ok, role} = Roles.create_role(param)

      conn
      |> put_view(StepFlow.RoleView)
      |> render("show.json", role: role)
    else
      conn
      |> forbidden()
    end
  end

  def create(conn, _) do
    conn
    |> forbidden()
  end

  operation :update, false

  def update(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id} = param) do
    role =
      case Integer.parse(id) do
        {id, ""} -> Roles.get_role(id)
        _ -> nil
      end

    if role != nil && Helpers.has_right?("roles::" <> role.name, user, "update") do
      {:ok, role} =
        role
        |> Roles.update_role(param)

      conn
      |> put_view(StepFlow.RoleView)
      |> render("show.json", role: role)
    else
      conn
      |> forbidden()
    end
  end

  def update(conn, _) do
    conn
    |> forbidden()
  end

  operation :delete,
    summary: "Delete role",
    description: "Delete role by id or by name",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Role ID or name (string)",
        type: :integer,
        example: 1
      ]
    ],
    responses: [
      no_content: "No Content",
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    role =
      case Integer.parse(id) do
        {id, ""} -> Roles.get_role(id)
        _ -> nil
      end

    if role != nil && Helpers.has_right?("roles::" <> role.name, user, "delete") do
      {:ok, role} =
        role
        |> Roles.delete_role()

      conn
      |> put_view(StepFlow.RoleView)
      |> render("show.json", role: role)
    else
      conn
      |> forbidden()
    end
  end

  def delete(conn, _) do
    conn
    |> forbidden()
  end
end
