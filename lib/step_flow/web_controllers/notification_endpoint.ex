defmodule StepFlow.WebController.NotificationEndpoint do
  @moduledoc false
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.NotificationHooks.NotificationEndpoint
  alias StepFlow.NotificationHooks.NotificationEndpoints
  alias StepFlow.WebController.Helpers
  alias StepFlow.WebController.OpenApiSchemas

  action_fallback(StepFlow.WebController.Fallback)

  tags ["Notification Endpoints"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  operation :index,
    summary: "List notification endpoints",
    description: "List notification endpoints",
    type: :object,
    responses: [
      ok:
        {"NotificationEndpoints", "application/json",
         OpenApiSchemas.Notifications.NotificationEndpoints},
      forbidden: "Forbidden"
    ]

  def index(conn, params) do
    notification_endpoints = NotificationEndpoints.list_notification_endpoints(params)

    conn
    |> put_view(StepFlow.NotificationEndpointView)
    |> render("index.json", notification_endpoints: notification_endpoints)
  end

  operation :create,
    summary: "Create notification endpoint",
    description: "Create a notification endpoint",
    type: :object,
    request_body:
      {"NotificationEndpoint", "application/json",
       OpenApiSchemas.Notifications.NotificationEndpoint},
    responses: [
      ok:
        {"NotificationEndpoint", "application/json",
         OpenApiSchemas.Notifications.NotificationEndpoint},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def create(%Plug.Conn{assigns: %{current_user: user}} = conn, notification_endpoint_params) do
    if Helpers.has_right?(
         "notification_endpoint::" <>
           StepFlow.Map.get_by_key_or_atom(notification_endpoint_params, :endpoint_placeholder),
         user,
         "create"
       ) do
      case NotificationEndpoints.create_notification_endpoint(notification_endpoint_params) do
        {:ok, %NotificationEndpoint{} = notification_endpoint} ->
          conn
          |> put_status(:created)
          |> put_view(StepFlow.NotificationEndpointView)
          |> render("show.json", notification_endpoint: notification_endpoint)

        {:error, _changeset} ->
          conn
          |> put_status(:bad_request)
          |> put_view(StepFlow.NotificationEndpointView)
          |> render("error.json",
            errors: %{
              reason: "Cannot create a Notification Endpoint with this endpoint placeholder."
            }
          )
      end
    else
      conn
      |> put_status(:forbidden)
      |> put_view(StepFlow.NotificationEndpointView)
      |> render("error.json",
        errors: %{reason: "Cannot create a Notification Endpoint with this user."}
      )
    end
  end

  operation :show,
    summary: "Get notification endpoint",
    description: "Get a notification endpoint",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Endpoint placeholder",
        type: :string,
        example: "my_endpoint"
      ]
    ],
    responses: [
      ok:
        {"NotificationEndpoint", "application/json",
         OpenApiSchemas.Notifications.NotificationEndpoint},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def show(conn, %{"id" => endpoint_placeholder}) do
    case NotificationEndpoints.get_notification_endpoint_by_placeholder(endpoint_placeholder) do
      nil ->
        conn
        |> put_status(:not_found)
        |> put_view(StepFlow.NotificationEndpointView)
        |> render("error.json",
          errors: %{
            reason:
              "There is no notification endpoint with this placeholder: " <> endpoint_placeholder
          }
        )

      notification_endpoint ->
        conn
        |> put_view(StepFlow.NotificationEndpointView)
        |> render("show.json", notification_endpoint: notification_endpoint)
    end
  end

  operation :update,
    summary: "Edit notification endpoint",
    description: "Edit notification endpoint",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Endpoint identifier",
        type: :string,
        example: "my_endpoint"
      ]
    ],
    request_body:
      {"Notification Body", "application/json", OpenApiSchemas.Notifications.NotificationEndpoint},
    responses: [
      ok: {"Notification", "application/json", OpenApiSchemas.Notifications.NotificationEndpoint},
      forbidden: "Forbidden"
    ]

  def update(conn, %{"id" => identifier, "notification_endpoint" => params}) do
    notification_endpoint = NotificationEndpoints.get_notification_endpoint!(identifier)

    case NotificationEndpoints.update_notification_endpoint(notification_endpoint, params) do
      {:ok, %NotificationEndpoint{} = notification_endpoint} ->
        conn
        |> put_status(:created)
        |> put_view(StepFlow.NotificationEndpointView)
        |> render("show.json", notification_endpoint: notification_endpoint)

      {:error, _} ->
        conn
        |> put_status(:unprocessable_entity)
        |> put_view(StepFlow.NotificationEndpointView)
        |> render("error.json",
          errors: %{reason: "Cannot change a Notification Endpoint."}
        )
    end
  end

  operation :delete,
    summary: "Delete notification endpoint",
    description: "Delete a notification endpoint by placeholder",
    type: :object,
    parameters: [
      endpoint_placeholder: [
        in: :path,
        description: "Endpoint placeholder",
        type: :string,
        example: "my_endpoint"
      ]
    ],
    responses: [
      no_content: "No Content",
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => endpoint_placeholder}) do
    case NotificationEndpoints.get_notification_endpoint_by_placeholder(endpoint_placeholder) do
      nil ->
        conn
        |> put_status(:not_found)
        |> put_view(StepFlow.NotificationEndpointView)
        |> render("error.json",
          errors: %{
            reason:
              "Cannot delete, there is no notification endpoint with this placeholder: " <>
                endpoint_placeholder
          }
        )

      notification_endpoint ->
        if Helpers.has_right?("notification_endpoint::*", user, "delete") do
          case NotificationEndpoints.delete_notification_endpoint(notification_endpoint) do
            {:ok, %NotificationEndpoint{}} ->
              send_resp(conn, :no_content, "")
          end
        else
          conn
          |> put_status(:forbidden)
          |> put_view(StepFlow.NotificationEndpointView)
          |> render("error.json",
            errors: %{reason: "Cannot delete a Notification Endpoint with this user."}
          )
        end
    end
  end
end
