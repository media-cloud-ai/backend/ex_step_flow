defmodule StepFlow.WebController.Workers do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  require Logger

  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.WebController.OpenApiSchemas
  alias StepFlow.Workers.WorkerStatuses

  @moduledoc false

  tags ["Workers"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(StepFlow.WebController.Fallback)

  operation :index,
    summary: "List Workers",
    description: "List Workers",
    type: :object,
    responses: [
      ok: {"Workers", "application/json", OpenApiSchemas.Workers.Workers},
      forbidden: "Forbidden"
    ]

  def index(conn, params) do
    Logger.debug("[#{__MODULE__}] List worker statuses request: #{inspect(params)}")

    worker_statuses = WorkerStatuses.list_worker_statuses(params)

    conn
    |> put_view(StepFlow.WorkerStatusView)
    |> render("index.json", worker_statuses)
  end

  operation :show,
    summary: "Show worker",
    description: "Show worker by instance id",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Worker instance ID",
        type: :string,
        example: "abdcdefgh1234"
      ]
    ],
    responses: [
      ok: {"Worker", "application/json", OpenApiSchemas.Workers.Worker},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def show(conn, %{"id" => instance_id}) do
    Logger.debug("[#{__MODULE__}] Show worker status request: #{inspect(instance_id)}")

    case WorkerStatuses.get_worker_status(instance_id) do
      nil ->
        conn
        |> send_resp(404, "No such a worker could be found. ")

      worker_status ->
        conn
        |> put_view(StepFlow.WorkerStatusView)
        |> render("worker_status.json", %{worker_status: worker_status})
    end
  end

  operation :update,
    summary: "Update worker",
    description: "Update worker",
    type: :object,
    parameters: [
      id: [
        in: :path,
        description: "Worker instance id",
        type: :string,
        example: "abdcdefgh1234"
      ]
    ],
    request_body: {"Parameters", "application/json", OpenApiSchemas.Parameters.Parameters},
    responses: [
      ok: {"Worker", "application/json", OpenApiSchemas.Workers.Worker},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def update(conn, params) do
    Logger.debug("[#{__MODULE__}] Put order message request: #{inspect(params)}")

    instance_id = Map.get(params, "id")

    case WorkerStatuses.get_worker_status(instance_id) do
      nil ->
        conn
        |> send_resp(404, "No such worker could be found. ")

      _worker_status ->
        order_message =
          Map.delete(params, "id")
          |> Jason.encode!()

        Logger.info(
          "[#{__MODULE__}] Send order to worker #{inspect(instance_id)}: #{inspect(order_message)}"
        )

        CommonEmitter.publish(
          "",
          order_message,
          [headers: [instance_id: instance_id]],
          "direct_messaging"
        )

        conn
        |> send_resp(204, "")
    end
  end
end
