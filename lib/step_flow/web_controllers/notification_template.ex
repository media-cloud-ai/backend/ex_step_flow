defmodule StepFlow.WebController.NotificationTemplate do
  @moduledoc false
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.NotificationHooks.NotificationTemplate
  alias StepFlow.NotificationHooks.NotificationTemplates
  alias StepFlow.WebController.Helpers
  alias StepFlow.WebController.OpenApiSchemas

  action_fallback(StepFlow.WebController.Fallback)

  tags ["Notification Templates"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  operation :index,
    summary: "List notification templates",
    description: "List notification templates",
    type: :object,
    responses: [
      ok:
        {"Notification Templates", "application/json",
         OpenApiSchemas.Notifications.NotificationTemplates},
      forbidden: "Forbidden"
    ]

  def index(%Plug.Conn{assigns: %{current_user: user}} = conn, params) do
    if Helpers.has_right?(
         "notification_template::*",
         user,
         "view"
       ) do
      notification_templates = NotificationTemplates.list_notification_templates(params)

      conn
      |> put_view(StepFlow.NotificationTemplateView)
      |> render("index.json", notification_templates: notification_templates)
    else
      conn
      |> put_status(:forbidden)
      |> put_view(StepFlow.NotificationTemplateView)
      |> render("error.json",
        errors: %{reason: "Cannot list Notification Templates with this user."}
      )
    end
  end

  operation :create,
    summary: "Create notification template",
    description: "Create a notification template",
    type: :object,
    request_body:
      {"Notification Template", "application/json",
       OpenApiSchemas.Notifications.NotificationTemplate},
    responses: [
      ok:
        {"Notification Template", "application/json",
         OpenApiSchemas.Notifications.NotificationTemplate},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def create(%Plug.Conn{assigns: %{current_user: user}} = conn, notification_template_params) do
    if Helpers.has_right?(
         "notification_template::" <>
           StepFlow.Map.get_by_key_or_atom(notification_template_params, :template_name),
         user,
         "create"
       ) do
      case NotificationTemplates.create_notification_template(notification_template_params) do
        {:ok, %NotificationTemplate{} = notification_template} ->
          conn
          |> put_status(:created)
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("show.json", notification_template: notification_template)

        {:error, _changeset} ->
          conn
          |> put_status(:unprocessable_entity)
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("error.json",
            errors: %{
              reason: "Cannot create a Notification Template with this template name."
            }
          )
      end
    else
      conn
      |> put_status(:forbidden)
      |> put_view(StepFlow.NotificationTemplateView)
      |> render("error.json",
        errors: %{reason: "Cannot create a Notification Template with this user."}
      )
    end
  end

  operation :show,
    summary: "Get notification template",
    description: "Get a notification template by template name",
    type: :object,
    parameters: [
      template_name: [
        in: :path,
        description: "Template name",
        type: :string,
        example: "my_name"
      ]
    ],
    responses: [
      ok:
        {"Notification Template", "application/json",
         OpenApiSchemas.Notifications.NotificationTemplate},
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def show(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => template_name}) do
    if Helpers.has_right?(
         "notification_template::" <> template_name,
         user,
         "view"
       ) do
      case NotificationTemplates.get_notification_template_by_name(template_name) do
        nil ->
          conn
          |> put_status(:not_found)
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("error.json",
            errors: %{
              reason: "There is no notification template with this name: " <> template_name
            }
          )

        notification_template ->
          conn
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("show.json", notification_template: notification_template)
      end
    else
      conn
      |> put_status(:forbidden)
      |> put_view(StepFlow.NotificationTemplateView)
      |> render("error.json",
        errors: %{reason: "Cannot view this Notification Template with this user."}
      )
    end
  end

  operation :update,
    summary: "Edit notification template",
    description: "Edit notification template by template name",
    type: :object,
    request_body:
      {"Notification Body", "application/json", OpenApiSchemas.Notifications.NotificationEndpoint},
    responses: [
      ok: {"Notification", "application/json", OpenApiSchemas.Notifications.NotificationEndpoint},
      forbidden: "Forbidden"
    ]

  def update(%Plug.Conn{assigns: %{current_user: user}} = conn, %{
        "id" => template_name,
        "notification_template" => params
      }) do
    notification_template = NotificationTemplates.get_notification_template_by_name(template_name)

    if Helpers.has_right?(
         "notification_template::" <>
           StepFlow.Map.get_by_key_or_atom(notification_template, :template_name),
         user,
         "update"
       ) do
      case NotificationTemplates.update_notification_template(notification_template, params) do
        {:ok, %NotificationTemplate{} = notification_template} ->
          conn
          |> put_status(200)
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("show.json", notification_template: notification_template)

        {:error, _} ->
          conn
          |> put_status(:unprocessable_entity)
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("error.json",
            errors: %{reason: "Cannot change a Notification Template."}
          )
      end
    else
      conn
      |> put_status(:forbidden)
      |> put_view(StepFlow.NotificationTemplateView)
      |> render("error.json",
        errors: %{reason: "Cannot create a Notification Template with this user."}
      )
    end
  end

  operation :delete,
    summary: "Delete notification template",
    description: "Delete a notification template by template name",
    type: :object,
    parameters: [
      template_name: [
        in: :path,
        description: "Template name",
        type: :string,
        example: "my_name"
      ]
    ],
    responses: [
      no_content: "No Content",
      forbidden: "Forbidden",
      not_found: "Not Found"
    ]

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => template_name}) do
    case NotificationTemplates.get_notification_template_by_name(template_name) do
      nil ->
        conn
        |> put_status(:not_found)
        |> put_view(StepFlow.NotificationTemplateView)
        |> render("error.json",
          errors: %{
            reason:
              "Cannot delete, there is no notification template with this name: " <>
                template_name
          }
        )

      notification_template ->
        if Helpers.has_right?("notification_template::*", user, "delete") do
          case NotificationTemplates.delete_notification_template(notification_template) do
            {:ok, %NotificationTemplate{}} ->
              send_resp(conn, :no_content, "")
          end
        else
          conn
          |> put_status(:forbidden)
          |> put_view(StepFlow.NotificationTemplateView)
          |> render("error.json",
            errors: %{reason: "Cannot delete a Notification Template with this user."}
          )
        end
    end
  end
end
