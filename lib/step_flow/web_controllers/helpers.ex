defmodule StepFlow.WebController.Helpers do
  @moduledoc """
  The Helper Controller context.
  """

  alias StepFlow.Roles

  @doc """
  Check whether a user is allowed to use a rightable structure
  """
  def has_right?(_rightable, nil, _action), do: false

  def has_right?(rightable, user, action) do
    Roles.get_roles(user.roles)
    |> StepFlow.Controllers.Roles.has_right?(rightable, action)
  end
end
