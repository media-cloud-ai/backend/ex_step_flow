defmodule StepFlow.WebController.LiveWorkers do
  use StepFlow, :controller
  use OpenApiSpex.ControllerSpecs

  alias StepFlow.LiveWorkers
  alias StepFlow.WebController.OpenApiSchemas

  @moduledoc false

  tags ["Workers"]
  security [%{"authorization" => %OpenApiSpex.SecurityScheme{type: "http", scheme: "bearer"}}]

  action_fallback(StepFlow.WebController.Fallback)

  operation :index,
    summary: "List live workers",
    description: "List live workers",
    type: :object,
    responses: [
      ok: {"LiveWorkers", "application/json", OpenApiSchemas.Workers.LiveWorkers},
      forbidden: "Forbidden"
    ]

  def index(conn, params) do
    live_workers = LiveWorkers.list_live_workers(params)

    conn
    |> put_view(StepFlow.LiveWorkersView)
    |> render("index.json", live_workers: live_workers)
  end
end
