defmodule StepFlow.WebController.BlackList do
  use StepFlow, :controller

  @moduledoc false

  alias StepFlow.Controllers.BlackList
  alias StepFlow.WebController.Helpers

  def index(%Plug.Conn{assigns: %{current_user: user}} = conn, _params) do
    if Helpers.has_right?("blacklist::*", user, "view") do
      black_list = BlackList.get!()

      conn
      |> put_view(StepFlow.BlackListView)
      |> render("index.json", black_list: black_list)
    else
      forbidden(conn)
    end
  end

  def index(conn, _) do
    forbidden(conn)
  end

  def create(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    if Helpers.has_right?("blacklist::*", user, "create") do
      black_list = BlackList.add_and_notify!(id)

      conn
      |> put_view(StepFlow.BlackListView)
      |> render("index.json", black_list: black_list)
    else
      forbidden(conn)
    end
  end

  def create(conn, _) do
    forbidden(conn)
  end

  def delete(%Plug.Conn{assigns: %{current_user: user}} = conn, %{"id" => id}) do
    if Helpers.has_right?("blacklist::*", user, "delete") do
      black_list = BlackList.delete_and_notify!(id)

      conn
      |> put_view(StepFlow.BlackListView)
      |> render("index.json", black_list: black_list)
    else
      forbidden(conn)
    end
  end

  def delete(conn, _) do
    forbidden(conn)
  end

  def forbidden(conn) do
    send_resp(conn, 403, "Forbidden")
  end
end
