defmodule StepFlow.ProcessManager do
  @moduledoc false

  use Supervisor

  require Logger

  alias StepFlow.Repo.Checker

  @doc false
  def child_spec(_) do
    %{
      id: StepFlow.ProcessManager,
      start: {StepFlow.ProcessManager, :start_link, []},
      type: :supervisor
    }
  end

  @doc false
  def start_link do
    Logger.info("#{__MODULE__} start_link")
    supervisor = Supervisor.start_link(__MODULE__, [], name: __MODULE__)

    if Checker.repo_running?() do
      start_children()
    end

    supervisor
  end

  @doc false
  def init(_) do
    Logger.info("#{__MODULE__} init")
    Supervisor.init([], strategy: :one_for_one)
  end

  def start_children do
    children = [
      StepFlow.Workers.Supervisor,
      StepFlow.Workflows.StepManager,
      StepFlow.Workflows.WorkflowsWatcher
    ]

    children =
      case Application.get_env(:step_flow, StepFlow.Endpoint) do
        nil ->
          children

        _ ->
          children ++
            [
              {Phoenix.PubSub, [name: StepFlow.PubSub, adapter: Phoenix.PubSub.PG2]},
              StepFlow.Endpoint
            ]
      end

    children =
      StepFlow.Configuration.get_slack_token()
      |> case do
        nil ->
          children

        slack_token ->
          Logger.info("Starting Slack Bot")

          List.insert_at(
            children,
            -1,
            %{
              id: Slack.Bot,
              start: {
                Slack.Bot,
                :start_link,
                [
                  StepFlow.SlackBot,
                  [],
                  slack_token,
                  %{
                    keepalive: 10_000,
                    name: :step_flow_slack_bot,
                    scope: "identify,incoming-webhook"
                  }
                ],
                restart: :transient
              }
            }
          )
      end

    children
    |> Enum.each(fn child ->
      Logger.info("#{__MODULE__} start child: #{inspect(child)}")

      case get_child_pid(StepFlow.ProcessManager, child) do
        nil ->
          {:ok, _pid} = Supervisor.start_child(StepFlow.ProcessManager, child)

        :undefined ->
          {:ok, _pid} = Supervisor.restart_child(StepFlow.ProcessManager, child)

        :restarting ->
          Logger.info("#{inspect(child)}} #{__MODULE__} child already restarting...")

        pid ->
          Logger.info("#{inspect(child)}} #{__MODULE__} child already running: #{inspect(pid)}")
      end
    end)
  end

  def stop_children do
    Supervisor.which_children(StepFlow.ProcessManager)
    |> Enum.each(fn {id, pid, _type, _modules} ->
      Logger.info("#{__MODULE__} terminate child: #{id}, #{inspect(pid)}")
      Supervisor.terminate_child(StepFlow.ProcessManager, id)
    end)
  end

  def get_child_pid(supervisor, child) do
    case get_child(supervisor, child) do
      nil -> nil
      {_id, pid, _type, _modules} -> pid
    end
  end

  def get_child(supervisor, child) do
    Supervisor.which_children(supervisor)
    |> Enum.find(fn {id, _pid, _type, _} -> id == child end)
  end
end
