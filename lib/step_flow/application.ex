defmodule StepFlow.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application
  alias StepFlow.Metrics
  alias StepFlow.Migration
  alias StepFlow.WebController.Metric
  alias StepFlow.WorkflowDefinitions
  require Logger

  def start(_type, _args) do
    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: StepFlow.Supervisor]

    supervisor =
      Supervisor.start_link(
        [],
        opts
      )

    # Starts the database supervisor and set it up
    {:ok, _pid} = Supervisor.start_child(StepFlow.Supervisor, StepFlow.Repo)

    Migration.All.apply_migrations()

    WorkflowDefinitions.load_workflows_in_database()

    # Uncomment the following line to dump the Application supervision tree into the logs regularly
    # Supervisor.start_child(StepFlow.Supervisor, StepFlow.Repo.SupervisionTree)

    # Once the database is ready, starts the other processes supervisor
    {:ok, _pid} = Supervisor.start_child(StepFlow.Supervisor, StepFlow.Repo.Checker)
    {:ok, _pid} = Supervisor.start_child(StepFlow.Supervisor, StepFlow.ProcessManager)

    # Start prometheus exporter and instrumenters
    if StepFlow.Configuration.metrics_enabled?() do
      Metrics.WorkflowInstrumenter.setup()
      Metrics.JobInstrumenter.setup()
      Metric.setup()
    end

    # AMQP supervisor and workers will be started last
    {:ok, _pid} = Supervisor.start_child(StepFlow.Supervisor, StepFlow.Amqp.Supervisor)

    supervisor
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  def config_change(changed, _new, removed) do
    StepFlow.Endpoint.config_change(changed, removed)
    :ok
  end
end
