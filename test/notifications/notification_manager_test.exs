defmodule StepFlow.NotificationManagerTest do
  use ExUnit.Case
  use Plug.Test
  import FakeServer

  alias Ecto.Adapters.SQL.Sandbox
  alias FakeServer.Response
  alias StepFlow.NotificationHooks.NotificationEndpoints
  alias StepFlow.NotificationHooks.NotificationHookManager
  alias StepFlow.NotificationHooks.NotificationTemplates
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 1)
    end)

    :ok
  end

  describe "workflows" do
    @valid_attrs %{
      schema_version: "1.11",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["coucou.mov"]
            }
          ]
        }
      ],
      user_uuid: "super_toto"
    }

    def workflow_fixture(attrs \\ %{}) do
      {:ok, workflow} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Workflows.create_workflow()

      workflow
    end

    @tag capture_log: true
    test_with_server "notification_workflow_status/2 completed wf success without credentials" do
      route("/notification/test_hook", fn query ->
        if Map.get(query.headers, "content-type") == "application/json" do
          Response.ok!(~s({"status": "ok"}))
        else
          Response.no_content!()
        end
      end)

      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "test_hook",
        endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
      })

      NotificationTemplates.create_notification_template(%{
        template_name: "test_template",
        template_body: "{
            \"test\": {}
          }",
        template_headers: "[{\"content-type\" : \"application/json\"}]"
      })

      workflow =
        workflow_fixture(
          notification_hooks: [
            %{
              endpoint: "test_hook",
              label: "label_test",
              conditions: %{
                "workflow_completed" => "test_template",
                "workflow_error" => "test_template"
              }
            }
          ]
        )

      workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "completed")

      timestamp =
        NaiveDateTime.utc_now()
        |> NaiveDateTime.to_string()
        |> NaiveDateTime.from_iso8601!()
        |> NaiveDateTime.truncate(:second)

      assert [
               %{
                 "endpoint" => "test_hook",
                 "label" => "label_test",
                 "status" => [
                   %{
                     "condition" => "workflow_completed",
                     "response" => 200,
                     "timestamp" => timestamp
                   }
                 ],
                 "conditions" => %{
                   "workflow_completed" => "test_template",
                   "workflow_error" => "test_template"
                 }
               }
             ] == workflow.notification_hooks
    end

    @tag capture_log: true
    test_with_server "notification_workflow_status/2 completed wf success with credentials" do
      route("/notification/test_hook", fn query ->
        if Map.get(query.headers, "content-type") == "application/json" do
          if Map.get(query.headers, "authorization") ==
               "Basic " <> Base.encode64("good_cred:good_cred_password") do
            Response.ok!(~s({"status": "ok"}))
          else
            Response.unauthorized!()
          end
        else
          Response.no_content!()
        end
      end)

      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "test_hook",
        endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
      })

      NotificationTemplates.create_notification_template(%{
        template_name: "test_template",
        template_body: "{
            \"test\": {}
          }",
        template_headers: "[{\"content-type\" : \"application/json\"}]"
      })

      workflow =
        workflow_fixture(
          notification_hooks: [
            %{
              endpoint: "test_hook",
              label: "label_test",
              conditions: %{
                "workflow_completed" => "test_template",
                "workflow_error" => "test_template"
              }
            }
          ]
        )

      workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "completed")

      timestamp =
        NaiveDateTime.utc_now()
        |> NaiveDateTime.to_string()
        |> NaiveDateTime.from_iso8601!()
        |> NaiveDateTime.truncate(:second)

      assert [
               %{
                 "endpoint" => "test_hook",
                 "label" => "label_test",
                 "status" => [
                   %{
                     "condition" => "workflow_completed",
                     "response" => 401,
                     "timestamp" => timestamp
                   }
                 ],
                 "conditions" => %{
                   "workflow_completed" => "test_template",
                   "workflow_error" => "test_template"
                 }
               }
             ] == workflow.notification_hooks

      NotificationEndpoints.get_notification_endpoint_by_placeholder("test_hook")
      |> NotificationEndpoints.delete_notification_endpoint()

      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "test_hook",
        endpoint_url: "http://#{FakeServer.address()}/notification/test_hook",
        endpoint_credentials: "good_cred:good_cred_password"
      })

      NotificationTemplates.create_notification_template(%{
        template_name: "test_template",
        template_body: "{
            \"test\": {}
          }",
        template_headers: "[{\"content-type\" : \"application/json\"}]"
      })

      workflow =
        workflow_fixture(
          notification_hooks: [
            %{
              endpoint: "test_hook",
              label: "label_test",
              conditions: %{
                "workflow_completed" => "test_template",
                "workflow_error" => "test_template"
              }
            }
          ]
        )

      workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "completed")

      timestamp =
        NaiveDateTime.utc_now()
        |> NaiveDateTime.to_string()
        |> NaiveDateTime.from_iso8601!()
        |> NaiveDateTime.truncate(:second)

      assert [
               %{
                 "endpoint" => "test_hook",
                 "label" => "label_test",
                 "status" => [
                   %{
                     "condition" => "workflow_completed",
                     "response" => 200,
                     "timestamp" => timestamp
                   }
                 ],
                 "conditions" => %{
                   "workflow_completed" => "test_template",
                   "workflow_error" => "test_template"
                 }
               }
             ] == workflow.notification_hooks
    end

    @tag capture_log: true
    test_with_server "notification_workflow_status/2 completed wf error without credentials" do
      route("/notification/test_hook", fn query ->
        if Map.get(query.headers, "content-type") == "application/json" do
          Response.ok!(~s({"status": "ok"}))
        else
          Response.no_content!()
        end
      end)

      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "test_hook",
        endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
      })

      NotificationTemplates.create_notification_template(%{
        template_name: "test_template",
        template_body: "{
            \"test\": {}
          }",
        template_headers: "[{\"content-type\" : \"application/json\"}]"
      })

      workflow =
        workflow_fixture(
          notification_hooks: [
            %{
              endpoint: "test_hook",
              label: "label_test",
              conditions: %{
                "workflow_completed" => "test_template",
                "workflow_error" => "test_template"
              }
            }
          ]
        )

      workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

      timestamp =
        NaiveDateTime.utc_now()
        |> NaiveDateTime.to_string()
        |> NaiveDateTime.from_iso8601!()
        |> NaiveDateTime.truncate(:second)

      assert [
               %{
                 "endpoint" => "test_hook",
                 "label" => "label_test",
                 "status" => [
                   %{
                     "condition" => "workflow_error",
                     "response" => 200,
                     "timestamp" => timestamp
                   }
                 ],
                 "conditions" => %{
                   "workflow_completed" => "test_template",
                   "workflow_error" => "test_template"
                 }
               }
             ] == workflow.notification_hooks
    end

    @tag capture_log: true
    test_with_server "notification_workflow_status/2 completed multiples notification hooks on error" do
      route("/notification/test_hook", fn query ->
        if Map.get(query.headers, "content-type") == "application/json" do
          Response.ok!(~s({"status": "ok"}))
        else
          Response.no_content!()
        end
      end)

      route("/notification/test_hook2", fn query ->
        if Map.get(query.headers, "content-type") == "application/json" do
          Response.ok!(~s({"status": "ok"}))
        else
          Response.no_content!()
        end
      end)

      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "test_hook",
        endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
      })

      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "test_hook2",
        endpoint_url: "http://#{FakeServer.address()}/notification/test_hook2"
      })

      NotificationTemplates.create_notification_template(%{
        template_name: "test_template",
        template_body: "{
            \"test\": {}
          }",
        template_headers: "[{\"content-type\" : \"application/json\"}]"
      })

      workflow =
        workflow_fixture(
          notification_hooks: [
            %{
              endpoint: "test_hook",
              label: "label_test",
              conditions: %{
                "workflow_completed" => "test_template",
                "workflow_error" => "test_template"
              }
            },
            %{
              endpoint: "test_hook2",
              label: "label_test2",
              conditions: %{
                "workflow_completed" => "test_template",
                "workflow_error" => "test_template"
              }
            }
          ]
        )

      workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

      timestamp =
        NaiveDateTime.utc_now()
        |> NaiveDateTime.to_string()
        |> NaiveDateTime.from_iso8601!()
        |> NaiveDateTime.truncate(:second)

      assert [
               %{
                 "conditions" => %{
                   "workflow_completed" => "test_template",
                   "workflow_error" => "test_template"
                 },
                 "endpoint" => "test_hook",
                 "label" => "label_test",
                 "status" => [
                   %{
                     "condition" => "workflow_error",
                     "response" => 200,
                     "timestamp" => timestamp
                   }
                 ]
               },
               %{
                 "conditions" => %{
                   "workflow_completed" => "test_template",
                   "workflow_error" => "test_template"
                 },
                 "endpoint" => "test_hook2",
                 "label" => "label_test2",
                 "status" => [
                   %{
                     "condition" => "workflow_error",
                     "response" => 200,
                     "timestamp" => timestamp
                   }
                 ]
               }
             ] == workflow.notification_hooks
    end
  end

  @tag capture_log: true
  test "notification_workflow_status/2 url no response server" do
    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook_empty",
      endpoint_url: "test"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template",
      template_body: "{
          \"test\": {}
        }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook_empty",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template",
              "workflow_error" => "test_template"
            }
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

    assert [
             %{
               "conditions" => %{
                 "workflow_completed" => "test_template",
                 "workflow_error" => "test_template"
               },
               "endpoint" => "test_hook_empty",
               "label" => "label_test"
             }
           ] == workflow.notification_hooks
  end

  @tag capture_log: true
  test "notification_workflow_status/2 url uncorrect template headers" do
    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook_empty",
      endpoint_url: "test"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template_uncorrect_headers",
      template_body: "{
          \"test\": {}
        }",
      template_headers: "[{\"content-type\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook_empty",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template_uncorrect_headers",
              "workflow_error" => "test_template_uncorrect_headers"
            }
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

    assert workflow.notification_hooks == [
             %{
               "endpoint" => "test_hook_empty",
               "label" => "label_test",
               "conditions" => %{
                 "workflow_completed" => "test_template_uncorrect_headers",
                 "workflow_error" => "test_template_uncorrect_headers"
               }
             }
           ]
  end

  @tag capture_log: true
  test "notification_workflow_status/2 url uncorrect template body" do
    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook_empty",
      endpoint_url: "test"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template_uncorrect_body",
      template_body: "{
          \"test\"
        }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook_empty",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template_uncorrect_body",
              "workflow_error" => "test_template_uncorrect_body"
            }
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

    assert workflow.notification_hooks == [
             %{
               "endpoint" => "test_hook_empty",
               "label" => "label_test",
               "conditions" => %{
                 "workflow_completed" => "test_template_uncorrect_body",
                 "workflow_error" => "test_template_uncorrect_body"
               }
             }
           ]
  end

  @tag capture_log: true
  test "notification_workflow_status/2 url correct parameter template body" do
    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook_empty",
      endpoint_url: "test"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template_uncorrect_body",
      template_body: "{
          \"test\" : \"#workflow.id#\"
        }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook_empty",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template_uncorrect_body",
              "workflow_error" => "test_template_uncorrect_body"
            }
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

    assert workflow.notification_hooks == [
             %{
               "endpoint" => "test_hook_empty",
               "label" => "label_test",
               "conditions" => %{
                 "workflow_completed" => "test_template_uncorrect_body",
                 "workflow_error" => "test_template_uncorrect_body"
               }
             }
           ]
  end

  @tag capture_log: true
  test "notification_workflow_status/2 url unvalid parameter template body" do
    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook_empty",
      endpoint_url: "test"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template_uncorrect_body",
      template_body: "{
          \"test\" : \"#System.get_env(ROOT_PASSWORD)#\"
        }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook_empty",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template_uncorrect_body",
              "workflow_error" => "test_template_uncorrect_body"
            }
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

    assert workflow.notification_hooks == [
             %{
               "endpoint" => "test_hook_empty",
               "label" => "label_test",
               "conditions" => %{
                 "workflow_completed" => "test_template_uncorrect_body",
                 "workflow_error" => "test_template_uncorrect_body"
               }
             }
           ]
  end

  @tag capture_log: true
  test "notification_workflow_status/2 url unvalid condition template" do
    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook_empty",
            label: "label_test",
            conditions: [
              "workflow_completed"
            ]
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "error")

    assert !workflow.valid?

    assert workflow.errors == [
             notification_hooks: {"is invalid", [type: {:array, :map}, validation: :cast]}
           ]
  end

  @tag capture_log: true
  test_with_server "duplicating workflow does not conserve hook status" do
    route("/notification/test_hook", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook",
      endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template",
      template_body: "{
            \"test\": {}
          }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template",
              "workflow_error" => "test_template"
            }
          }
        ]
      )

    workflow = NotificationHookManager.manage_notification_status(workflow.id, nil, "completed")

    timestamp =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.to_string()
      |> NaiveDateTime.from_iso8601!()
      |> NaiveDateTime.truncate(:second)

    assert [
             %{
               "endpoint" => "test_hook",
               "label" => "label_test",
               "status" => [
                 %{
                   "condition" => "workflow_completed",
                   "response" => 200,
                   "timestamp" => timestamp
                 }
               ],
               "conditions" => %{
                 "workflow_completed" => "test_template",
                 "workflow_error" => "test_template"
               }
             }
           ] == workflow.notification_hooks

    {:ok, duplicated_workflow_id} = Workflows.duplicate_workflow(workflow.id, "A1B2C3")

    duplicated_workflow = Workflows.get_workflow!(duplicated_workflow_id)

    hook =
      StepFlow.Map.get_by_key_or_atom(duplicated_workflow, :notification_hooks) |> List.first()

    assert StepFlow.Map.get_by_key_or_atom(hook, "status") == nil
  end
end
