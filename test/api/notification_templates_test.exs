defmodule StepFlow.Api.NotificationTemplatesTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.NotificationHooks.NotificationTemplates
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router

  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)

    :ok
  end

  describe "notification_templates" do
    @administrator %{
      name: "administrator",
      rights: [%Right{entity: "notification_template::*", action: ["*"]}]
    }
    @unauthorized_user %{
      name: "unauthorized",
      rights: [%Right{entity: "roles::*", action: ["view"]}]
    }

    defp role_fixture(role) do
      {:ok, role} = Roles.create_role(role)

      role
    end

    test "GET /notification_templates" do
      _role = role_fixture(@administrator)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      notification_template_2 = %{
        template_name: "notification_template_2",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      {result, _} = NotificationTemplates.create_notification_template(notification_template_2)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, body} =
        conn(:get, "/notification_templates")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      assert body |> Jason.decode!() |> StepFlow.Map.get_by_key_or_atom(:total) == 2
    end

    test "SHOW /notification_templates/:template_name valid" do
      _role = role_fixture(@administrator)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, body} =
        conn(:get, "/notification_templates/notification_template_1")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200

      response = body |> Jason.decode!() |> StepFlow.Map.get_by_key_or_atom(:data)

      assert response
             |> StepFlow.Map.get_by_key_or_atom(:template_name) ==
               "notification_template_1"

      assert response
             |> StepFlow.Map.get_by_key_or_atom(:template_headers) == "{'test' : 'test'}"

      assert response
             |> StepFlow.Map.get_by_key_or_atom(:template_body) == "{'test': 'test'}"
    end

    test "SHOW /notification_templates/:template_name not valid" do
      _role = role_fixture(@administrator)

      {status, _headers, _body} =
        conn(:get, "/notification_templates/notification_template_1")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 404
    end

    test "SHOW /notification_templates/:template_name user not valid" do
      _role = role_fixture(@unauthorized_user)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:get, "/notification_templates/notification_template_1")
        |> assign(:current_user, %{
          roles: ["unauthorized"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "CREATE /notification_templates with authorized user" do
      _role = role_fixture(@administrator)

      {status, _headers, _body} =
        conn(:post, "/notification_templates", %{
          template_name: "notification_template_1",
          template_headers: "{\'test\' : \'test\'}",
          template_body: "{\'test\': \'test\'}"
        })
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 201

      notification_template =
        NotificationTemplates.get_notification_template_by_name("notification_template_1")

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_name) == "notification_template_1"

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_headers) == "{'test' : 'test'}"

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_body) == "{'test': 'test'}"
    end

    test "CREATE /notification_templates with unauthorized user" do
      _role = role_fixture(@unauthorized_user)

      {status, _headers, _body} =
        conn(:post, "/notification_templates", %{
          template_name: "notification_template_1",
          template_headers: "{\'test\' : \'test\'}",
          template_body: "{\'test\': \'test\'}"
        })
        |> assign(:current_user, %{
          roles: ["unauthorized"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "UPDATE /notification_templates/:template_name with authorized user" do
      _role = role_fixture(@administrator)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:put, "/notification_templates/notification_template_1", %{
          notification_template: %{
            template_headers: "{\'test2\' : \'test\'}",
            template_body: "{\'test2\': \'test\'}"
          }
        })
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200

      notification_template =
        NotificationTemplates.get_notification_template_by_name("notification_template_1")

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_name) == "notification_template_1"

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_headers) == "{'test2' : 'test'}"

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_body) == "{'test2': 'test'}"
    end

    test "UPDATE /notification_templates/:template_name with unauthorized user" do
      _role = role_fixture(@unauthorized_user)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:put, "/notification_templates/notification_template_1", %{
          notification_template: %{
            template_headers: "{\'test2\' : \'test\'}",
            template_body: "{\'test2\': \'test\'}"
          }
        })
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403

      notification_template =
        NotificationTemplates.get_notification_template_by_name("notification_template_1")

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_name) == "notification_template_1"

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_headers) == "{'test' : 'test'}"

      assert notification_template
             |> StepFlow.Map.get_by_key_or_atom(:template_body) == "{'test': 'test'}"
    end

    test "DELETE /notification_templates/:template_name with authorized user" do
      _role = role_fixture(@administrator)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:delete, "/notification_templates/notification_template_1")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 204
    end

    test "DELETE /notification_templates/:template_name with unauthorized user" do
      _role = role_fixture(@unauthorized_user)

      notification_template_1 = %{
        template_name: "notification_template_1",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:delete, "/notification_templates/notification_template_1")
        |> assign(:current_user, %{
          roles: ["unauthorized"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "DELETE /notification_templates/:template_name invalid name" do
      _role = role_fixture(@administrator)

      {status, _headers, _body} =
        conn(:delete, "/notification_templates/notification_template_1")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 404
    end
  end
end
