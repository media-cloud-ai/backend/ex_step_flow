defmodule StepFlow.Api.BlackListTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Jobs
  alias StepFlow.Jobs.Status
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router
  alias StepFlow.Workflows
  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  @full_rights_user %{
    name: "full_rights",
    rights: [
      %Right{
        entity: "blacklist::*",
        action: ["view", "create", "delete"]
      }
    ]
  }

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    steps: [],
    rights: [
      %{
        action: "create",
        groups: ["administrator"]
      }
    ]
  }

  defp create_job(workflow_id) do
    Jobs.create_job(%{
      name: "job_test",
      step_id: 0,
      workflow_id: workflow_id,
      parameters: []
    })
  end

  test "/blacklist" do
    {:ok, _role} = Roles.create_role(@full_rights_user)

    {_, workflow} = Workflows.create_workflow(@workflow)

    job_ids =
      for _ <- 0..10 do
        {_, job} = create_job(workflow.id)
        {:ok, _status} = Status.set_job_status(job.id, :queued)

        job.id
      end

    # GET empty black list
    {status, _headers, body} =
      conn(:get, "/blacklist")
      |> assign(:current_user, %{
        roles: ["full_rights"],
        uuid: "onetwothreefour"
      })
      |> Router.call(@opts)
      |> sent_resp

    assert status == 200
    assert %{"data" => [], "total" => 0} == body |> Jason.decode!()

    # POST add job IDs to black list
    job_ids
    |> Enum.with_index()
    |> Enum.each(fn {job_id, i} ->
      {status, _headers, body} =
        conn(:post, "/blacklist", %{id: job_id})
        |> assign(:current_user, %{
          roles: ["full_rights"],
          uuid: "onetwothreefour"
        })
        |> Router.call(@opts)
        |> sent_resp

      expected_total = i + 1
      expected_data = Enum.take(job_ids, expected_total)

      assert status == 200
      assert %{"data" => expected_data, "total" => expected_total} == body |> Jason.decode!()
    end)

    # GET black list
    {status, _headers, body} =
      conn(:get, "/blacklist")
      |> assign(:current_user, %{
        roles: ["full_rights"],
        uuid: "onetwothreefour"
      })
      |> Router.call(@opts)
      |> sent_resp

    assert status == 200
    assert %{"data" => job_ids, "total" => length(job_ids)} == body |> Jason.decode!()

    # DELETE add job IDs to black list
    job_ids
    |> Enum.with_index()
    |> Enum.each(fn {job_id, i} ->
      {status, _headers, body} =
        conn(:delete, "/blacklist/" <> Integer.to_string(job_id))
        |> assign(:current_user, %{
          roles: ["full_rights"],
          uuid: "onetwothreefour"
        })
        |> Router.call(@opts)
        |> sent_resp

      expected_total = length(job_ids) - i - 1
      expected_data = Enum.take(job_ids, -expected_total)

      assert status == 200
      assert %{"data" => expected_data, "total" => expected_total} == body |> Jason.decode!()
    end)
  end
end
