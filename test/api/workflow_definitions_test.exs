defmodule StepFlow.Api.WorkflowDefinitionsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router
  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  describe "workflow_definition" do
    @administrator %{
      name: "administrator",
      rights: [%Right{entity: "workflow::*", action: ["view"]}]
    }

    @unauthorized_user %{
      roles: []
    }

    defp role_fixture(role) do
      {:ok, role} = Roles.create_role(role)

      role
    end

    @tag capture_log: true
    test "GET /definitions with authorized user" do
      role_fixture(%{
        name: "toto",
        rights: [
          %Right{entity: "workflow::simple_workflow", action: ["view"]}
        ]
      })

      {status, _headers, body} =
        conn(:get, "/definitions", %{})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()
      assert Map.get(response, "total") == 2
    end

    @tag capture_log: true
    test "GET /definitions with fully-authorized user" do
      role_fixture(%{
        name: "admin",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, body} =
        conn(:get, "/definitions", %{})
        |> assign(:current_user, %{
          roles: ["admin"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()
      assert Map.get(response, "total") == 4
    end

    @tag capture_log: true
    test "GET /definitions with unauthorized user" do
      {status, _headers, body} =
        conn(:get, "/definitions")
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()
      assert Map.get(response, "total") == 0
    end

    @tag capture_log: true
    test "GET /definitions/simple_workflow with authorized user" do
      role_fixture(%{
        name: "toto",
        rights: [
          %Right{entity: "workflow::simple_workflow", action: ["view"]}
        ]
      })

      {status, _headers, body} =
        conn(:get, "/definitions/simple_workflow")
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()

      assert response["data"]["identifier"] == "simple_workflow"
      assert response["data"]["version_major"] == 0
      assert response["data"]["version_minor"] == 1
      assert response["data"]["version_micro"] == 0
      assert response["data"]["tags"] == ["speech_to_text"]
    end

    @tag capture_log: true
    test "GET /definitions/simple_workflow with unauthorized user" do
      {status, _headers, body} =
        conn(:get, "/definitions/simple_workflow")
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
      response = body |> Jason.decode!()

      assert response == %{
               "errors" => [
                 %{
                   "message" => "Incorrect parameters",
                   "reason" => "Forbidden to access workflow definition with this identifier"
                 }
               ]
             }
    end

    @tag capture_log: true
    test "GET /definitions/empty_workflow.json" do
      role_fixture(@administrator)

      {status, _headers, body} =
        conn(:get, "/definitions/empty_workflow.json")
        |> assign(:current_user, roles: ["administrator"])
        |> Router.call(@opts)
        |> sent_resp

      assert status == 422
      response = body |> Jason.decode!()

      assert response == %{
               "errors" => [
                 %{
                   "message" => "Incorrect parameters",
                   "reason" => "Unable to locate workflow with this identifier"
                 }
               ]
             }
    end

    @tag capture_log: true
    test "DELETE /definitions/[ID of simple_workflow def] with authorized user" do
      role_fixture(%{
        name: "administrator",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, body} =
        conn(:get, "/definitions/simple_workflow")
        |> assign(:current_user, %{
          roles: ["administrator"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()
      assert response["data"]["identifier"] == "simple_workflow"
      simple_workflow_id = response["data"]["id"]

      {status, _headers, body} =
        conn(:delete, "/definitions/#{simple_workflow_id}")
        |> assign(:current_user, %{
          roles: ["administrator"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()

      assert response == %{
               "message" => "Workflow definition with ID #{simple_workflow_id} marked as deleted"
             }
    end

    @tag capture_log: true
    test "DELETE /definitions/ with parameters with authorized user" do
      role_fixture(%{
        name: "administrator",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, body} =
        conn(:get, "/definitions/simple_workflow")
        |> assign(:current_user, %{
          roles: ["administrator"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()
      assert response["data"]["identifier"] == "simple_workflow"
      simple_workflow_id = response["data"]["id"]

      {status, _headers, body} =
        conn(:delete, "/definitions",
          workflow_definition: %{
            identifier: "simple_workflow",
            version_major: 0,
            version_minor: 1,
            version_micro: 0
          }
        )
        |> assign(:current_user, %{
          roles: ["administrator"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      response = body |> Jason.decode!()

      assert response == %{
               "message" => "Workflow definition with ID #{simple_workflow_id} marked as deleted"
             }
    end

    @tag capture_log: true
    test "DELETE /definitions/[incorrect ID] with incorrect ID" do
      role_fixture(%{
        name: "administrator",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, body} =
        conn(:delete, "/definitions/0")
        |> assign(:current_user, %{
          roles: ["administrator"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 422
      response = body |> Jason.decode!()

      assert response == %{
               "errors" => [
                 %{
                   "message" => "Incorrect parameters",
                   "reason" => "Unable to find Workflow definition with ID 0"
                 }
               ]
             }
    end

    @tag capture_log: true
    test "DELETE /definitions/ with incorrect parameters" do
      role_fixture(%{
        name: "administrator",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, body} =
        conn(:delete, "/definitions",
          workflow_definition: %{
            identifier: "simple_workflow",
            version_major: 9,
            version_minor: 9,
            version_micro: 9
          }
        )
        |> assign(:current_user, %{
          roles: ["administrator"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 422
      response = body |> Jason.decode!()

      assert response == %{
               "errors" => [
                 %{
                   "message" => "Incorrect parameters",
                   "reason" =>
                     "Unable to locate workflow definition with these version and identifier"
                 }
               ]
             }
    end
  end
end
