defmodule StepFlow.Api.WorkflowEventsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Jobs
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router
  alias StepFlow.Workflows
  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})

    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  describe "workflow_events" do
    @unauthorized_user %{
      roles: []
    }

    def workflow_fixture do
      {:ok, workflow} =
        Workflows.create_workflow(%{
          schema_version: "1.9",
          identifier: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
          reference: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
          version_major: 1,
          version_minor: 2,
          version_micro: 3
        })

      workflow
    end

    def workflow_fixture_live do
      {:ok, workflow} =
        Workflows.create_workflow(%{
          schema_version: "1.9",
          is_live: true,
          identifier: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
          reference: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
          version_major: 1,
          version_minor: 2,
          version_micro: 3
        })

      workflow
    end

    defp role_fixture(role) do
      {:ok, role} = Roles.create_role(role)

      role
    end

    test "POST /workflows/:id/events event is not supported" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "toto",
        rights: []
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{
          event: "event_that_does_not_exists"
        })
        |> assign(:current_user, %{roles: ["toto"]})
        |> Router.call(@opts)
        |> sent_resp

      assert status == 422
    end

    test "POST /workflows/:id/events abort valid with authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["abort"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "abort"})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events abort valid with fully-authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "admin",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "abort"})
        |> assign(:current_user, %{
          roles: ["admin"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events abort valid with unauthorized user" do
      workflow = workflow_fixture()

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "abort"})
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "abort"})
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "POST /workflows/:id/events invalid pause with authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["pause"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "pause"})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 422
    end

    test "POST /workflows/:id/events pause with invalid post_action authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["pause"]
          }
        ]
      })

      trigger_date_time =
        DateTime.utc_now()
        |> DateTime.add(100_000, :second)
        |> DateTime.to_unix()

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{
          event: "pause",
          post_action: "whatever",
          trigger_at: trigger_date_time
        })
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 404
    end

    test "POST /workflows/:id/events pause valid with authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["pause"]
          }
        ]
      })

      trigger_date_time =
        DateTime.utc_now()
        |> DateTime.add(100_000, :second)
        |> DateTime.to_unix()

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{
          event: "pause",
          post_action: "resume",
          trigger_at: trigger_date_time
        })
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events pause valid with fully-authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "admin",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      trigger_date_time =
        DateTime.utc_now()
        |> DateTime.add(100_000, :second)
        |> DateTime.to_unix()

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{
          event: "pause",
          post_action: "resume",
          trigger_at: trigger_date_time
        })
        |> assign(:current_user, %{
          roles: ["admin"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events pause valid with unauthorized user" do
      workflow = workflow_fixture()

      trigger_date_time =
        DateTime.utc_now()
        |> DateTime.add(100_000, :second)
        |> DateTime.to_unix()

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{
          event: "pause",
          post_action: "resume",
          trigger_at: trigger_date_time
        })
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "POST /workflows/:id/events resume valid with authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["resume"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "resume"})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events resume valid with fully-authorized user" do
      workflow = workflow_fixture()

      role_fixture(%{
        name: "admin",
        rights: [
          %Right{entity: "*", action: ["*"]}
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "resume"})
        |> assign(:current_user, %{
          roles: ["admin"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events resume valid with unauthorized user" do
      workflow = workflow_fixture()

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "resume"})
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "POST /workflows/:id/events retry valid (on failed job) with authorized user" do
      workflow = workflow_fixture()
      step_id = 0
      action = "job_test"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      {:ok, job_status} = Jobs.Status.set_job_status(job.id, :error)
      Workflows.Status.set_workflow_status(workflow.id, :error, job_status)

      :timer.sleep(1000)

      # Retry workflow job

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["retry"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "retry", job_id: job.id})
        |> assign(:current_user, %{
          roles: ["toto"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200

      job = Jobs.get_job_with_status!(job.id)
      last_job_status = StepFlow.Controllers.Jobs.get_last_status(job.status)
      last_workflow_status = Workflows.Status.get_last_workflow_status(workflow.id)

      assert last_job_status.state == :queued
      assert last_job_status.description == %{}
      assert last_workflow_status.state == :processing
    end

    test "POST /workflows/:workflow_id/events retry valid (on failed job) with authorized user" do
      workflow = workflow_fixture()
      action = "job_test"

      job_params = %{
        name: action,
        step_id: 0,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job_0} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      job_params = %{
        name: action,
        step_id: 1,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job_1} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      {:ok, job_status} = Jobs.Status.set_job_status(job_0.id, :error)
      Workflows.Status.set_workflow_status(workflow.id, :error, job_status)

      {:ok, job_status} = Jobs.Status.set_job_status(job_1.id, :error)
      Workflows.Status.set_workflow_status(workflow.id, :error, job_status)

      :timer.sleep(1000)

      # Retry workflow job

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["retry"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{
          event: "retry"
        })
        |> assign(:current_user, %{
          roles: ["toto"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200

      job_0 = Jobs.get_job_with_status!(job_0.id)
      last_job_status = StepFlow.Controllers.Jobs.get_last_status(job_0.status)
      last_workflow_status = Workflows.Status.get_last_workflow_status(workflow.id)

      assert last_job_status.state == :queued
      assert last_job_status.description == %{}
      assert last_workflow_status.state == :processing

      job_1 = Jobs.get_job_with_status!(job_1.id)
      last_job_status = StepFlow.Controllers.Jobs.get_last_status(job_1.status)
      last_workflow_status = Workflows.Status.get_last_workflow_status(workflow.id)

      assert last_job_status.state == :queued
      assert last_job_status.description == %{}
      assert last_workflow_status.state == :processing
    end

    test "POST /workflows/:id/events retry valid (on failed job) with unauthorized user" do
      workflow = workflow_fixture()
      step_id = 0
      action = "test_action"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      {:ok, job_status} = Jobs.Status.set_job_status(job.id, :error)
      Workflows.Status.set_workflow_status(workflow.id, :error, job_status)

      :timer.sleep(1000)

      # Retry workflow job

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "retry", job_id: job.id})
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403

      # Retry without user uuid

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["retry"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "retry", job_id: job.id})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "POST /workflows/:id/events retry invalid (on non-failed job)" do
      workflow = workflow_fixture()
      step_id = 0
      action = "test_action"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      Jobs.Status.set_job_status(job.id, :queued)
      Jobs.Status.set_job_status(job.id, :processing)
      {:ok, job_status} = Jobs.Status.set_job_status(job.id, :error)
      Workflows.Status.set_workflow_status(workflow.id, :error, job_status)
      Jobs.Status.set_job_status(job.id, :retrying)
      Jobs.Status.set_job_status(job.id, :queued)
      Jobs.Status.set_job_status(job.id, :processing)

      # Retry workflow job

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["retry"]
          }
        ]
      })

      {status, _headers, body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "retry", job_id: job.id})
        |> assign(:current_user, %{
          roles: ["toto"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
      assert body == "{\"message\":\"Illegal operation\",\"status\":\"error\"}"
    end

    test "POST /workflows/:id/events delete valid with authorized user" do
      workflow = workflow_fixture()
      step_id = 0
      action = "test_action"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, _job} = StepFlow.Jobs.create_job(job_params)
      assert result == :ok
      {result, _job} = StepFlow.Jobs.create_job(job_params)
      assert result == :ok

      # Abort workflow

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["delete"]
          }
        ]
      })

      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "delete"})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events delete valid with unauthorized user" do
      workflow = workflow_fixture()
      step_id = 0
      action = "test_action"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, _job} = StepFlow.Jobs.create_job(job_params)
      assert result == :ok
      {result, _job} = StepFlow.Jobs.create_job(job_params)
      assert result == :ok

      # Abort workflow
      {status, _headers, _body} =
        conn(:post, "/workflows/#{workflow.id}/events", %{event: "delete"})
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "POST /workflows/:id/events event: stop live workflow processing with authorized user" do
      workflow_id =
        workflow_fixture_live()
        |> Map.get(:id)
        |> Integer.to_string()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["abort"]
          }
        ]
      })

      Workflows.Status.set_workflow_status(workflow_id, :processing)

      {status, _headers, _body} =
        conn(:post, "/workflows/" <> workflow_id <> "/events", %{event: "stop"})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
    end

    test "POST /workflows/:id/events event: stop live workflow pending with authorized user" do
      workflow_id =
        workflow_fixture_live()
        |> Map.get(:id)
        |> Integer.to_string()

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["abort"]
          }
        ]
      })

      Workflows.Status.set_workflow_status(workflow_id, :pending)

      {status, _headers, _body} =
        conn(:post, "/workflows/" <> workflow_id <> "/events", %{event: "stop"})
        |> assign(:current_user, %{
          roles: ["toto"]
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 500
    end
  end
end
