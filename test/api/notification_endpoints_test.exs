defmodule StepFlow.Api.NotificationEndpointsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.NotificationHooks.NotificationEndpoints
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router

  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)

    :ok
  end

  describe "notification_endpoints" do
    @administrator %{
      name: "administrator",
      rights: [%Right{entity: "notification_endpoint::*", action: ["*"]}]
    }
    @unauthorized_user %{
      name: "unauthorized",
      rights: [%Right{entity: "notification_endpoint::*", action: ["view"]}]
    }

    defp role_fixture(role) do
      {:ok, role} = Roles.create_role(role)

      role
    end

    test "GET /notification_endpoints" do
      notification_endpoint_1 = %{
        endpoint_placeholder: "notification_endpoint_1",
        endpoint_url: "test_1"
      }

      notification_endpoint_2 = %{
        endpoint_placeholder: "notification_endpoint_2",
        endpoint_url: "test_2"
      }

      {result, _} = NotificationEndpoints.create_notification_endpoint(notification_endpoint_1)
      assert result == :ok

      {result, _} = NotificationEndpoints.create_notification_endpoint(notification_endpoint_2)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, body} =
        conn(:get, "/notification_endpoints")
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      assert body |> Jason.decode!() |> StepFlow.Map.get_by_key_or_atom(:total) == 2
    end

    test "SHOW /notification_endpoints/:endpoint_placeholder valid" do
      notification_endpoint_1 = %{
        endpoint_placeholder: "notification_endpoint_1",
        endpoint_url: "test_1"
      }

      {result, _} = NotificationEndpoints.create_notification_endpoint(notification_endpoint_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, body} =
        conn(:get, "/notification_endpoints/notification_endpoint_1")
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200

      response = body |> Jason.decode!() |> StepFlow.Map.get_by_key_or_atom(:data)

      assert response
             |> StepFlow.Map.get_by_key_or_atom(:endpoint_placeholder) ==
               "notification_endpoint_1"

      assert response
             |> StepFlow.Map.get_by_key_or_atom(:endpoint_url) == "test_1"
    end

    test "SHOW /notification_endpoints/:endpoint_placeholder not valid" do
      {status, _headers, _body} =
        conn(:get, "/notification_endpoints/notification_endpoint_1")
        |> Router.call(@opts)
        |> sent_resp

      assert status == 404
    end

    test "CREATE /notification_endpoints with authorized user" do
      _role = role_fixture(@administrator)

      {status, _headers, _body} =
        conn(:post, "/notification_endpoints", %{
          endpoint_placeholder: "notification_endpoint_1",
          endpoint_url: "test_1"
        })
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 201

      notification_endpoint =
        NotificationEndpoints.get_notification_endpoint_by_placeholder("notification_endpoint_1")

      assert notification_endpoint |> StepFlow.Map.get_by_key_or_atom(:endpoint_url) == "test_1"
    end

    test "CREATE /notification_endpoints with unauthorized user" do
      _role = role_fixture(@unauthorized_user)

      {status, _headers, _body} =
        conn(:post, "/notification_endpoints", %{
          endpoint_placeholder: "notification_endpoint_1",
          endpoint_url: "test_1"
        })
        |> assign(:current_user, %{
          roles: ["unauthorized"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "DELETE /notification_endpoints/:endpoint_placeholder with authorized user" do
      _role = role_fixture(@administrator)

      notification_endpoint_1 = %{
        endpoint_placeholder: "notification_endpoint_1",
        endpoint_url: "test_1"
      }

      {result, _} = NotificationEndpoints.create_notification_endpoint(notification_endpoint_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:delete, "/notification_endpoints/notification_endpoint_1")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 204
    end

    test "DELETE /notification_endpoints/:endpoint_placeholder with unauthorized user" do
      _role = role_fixture(@unauthorized_user)

      notification_endpoint_1 = %{
        endpoint_placeholder: "notification_endpoint_1",
        endpoint_url: "test_1"
      }

      {result, _} = NotificationEndpoints.create_notification_endpoint(notification_endpoint_1)
      assert result == :ok

      :timer.sleep(500)

      {status, _headers, _body} =
        conn(:delete, "/notification_endpoints/notification_endpoint_1")
        |> assign(:current_user, %{
          roles: ["unauthorized"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end

    test "DELETE /notification_endpoints/:endpoint_placeholder invalid placeholder" do
      _role = role_fixture(@administrator)

      {status, _headers, _body} =
        conn(:delete, "/notification_endpoints/notification_endpoint_1")
        |> assign(:current_user, %{
          roles: ["administrator"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 404
    end
  end
end
