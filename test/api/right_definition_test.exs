defmodule StepFlow.Api.RightDefinitionTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Router
  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  test "GET /right_definitions" do
    {status, _headers, body} =
      conn(:get, "/right_definitions")
      |> Router.call(@opts)
      |> sent_resp

    assert status == 200

    assert %{
             "endpoints" => [
               "endpoint::jobs",
               "endpoint::live_workers",
               "endpoint::metrics",
               "endpoint::notification_endpoint",
               "endpoint::notification_template",
               "endpoint::statistics",
               "endpoint::workers",
               "endpoint::worker_definitions",
               "endpoint::workflow",
               "endpoint::workflow_definition"
             ],
             "rights" => ["abort", "create", "delete", "retry", "stop", "update", "view"]
           } = body |> Jason.decode!()
  end
end
