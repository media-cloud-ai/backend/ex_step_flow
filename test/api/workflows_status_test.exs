defmodule StepFlow.Api.WorkflowsStatusTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router

  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  defp role_fixture(role) do
    {:ok, role} = Roles.create_role(role)

    role
  end

  test "GET /workflows_status 200" do
    role_fixture(%{
      name: "can_see",
      rights: [
        %Right{entity: "workflow::*", action: ["view"]}
      ]
    })

    {status, _headers, body} =
      conn(:get, "/workflows_status")
      |> assign(:current_user, %{
        roles: ["can_see"]
      })
      |> Router.call(@opts)
      |> sent_resp

    expected_status = [
      "pending",
      "skipped",
      "processing",
      "retrying",
      "stopped",
      "error",
      "completed",
      "pausing",
      "paused"
    ]

    assert status == 200
    assert body |> Jason.decode!() == expected_status
  end
end
