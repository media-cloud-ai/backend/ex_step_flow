defmodule StepFlow.Api.JobsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Router
  alias StepFlow.Workflows
  doctest StepFlow

  @opts Router.init([])

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  describe "jobs" do
    @unauthorized_user %{
      roles: []
    }

    def workflow_fixture do
      {:ok, workflow} =
        Workflows.create_workflow(%{
          schema_version: "1.9",
          identifier: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
          reference: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
          version_major: 1,
          version_minor: 2,
          version_micro: 3
        })

      workflow
    end

    defp role_fixture(role) do
      {:ok, role} = Roles.create_role(role)

      role
    end

    test "GET /jobs" do
      workflow = workflow_fixture()
      step_id = 0
      action = "job_test"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, _} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      :timer.sleep(1000)

      {status, _headers, body} =
        conn(:get, "/jobs")
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      assert body |> Jason.decode!() == %{"data" => [], "total" => 0}

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["view"]
          }
        ]
      })

      {status, _headers, body} =
        conn(:get, "/jobs")
        |> assign(:current_user, %{
          roles: ["toto"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      assert body |> Jason.decode!() |> Map.get("total") == 1

      role_fixture(%{
        name: "admin",
        rights: [
          %Right{
            entity: "*",
            action: ["*"]
          }
        ]
      })

      {status, _headers, body} =
        conn(:get, "/jobs")
        |> assign(:current_user, %{
          roles: ["admin"],
          uuid: "hyper_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200
      assert body |> Jason.decode!() |> Map.get("total") == 1
    end

    test "SHOW /jobs/:id with authorized user" do
      workflow = workflow_fixture()
      step_id = 0
      action = "job_test"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job} = StepFlow.Jobs.create_job(job_params)

      job_id =
        job.id
        |> Integer.to_string()

      assert result == :ok

      :timer.sleep(1000)

      role_fixture(%{
        name: "toto",
        rights: [
          %Right{
            entity: "workflow::9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
            action: ["view"]
          }
        ]
      })

      {status, _headers, body} =
        conn(:get, "/jobs/" <> job_id)
        |> assign(:current_user, %{
          roles: ["toto"],
          uuid: "super_toto"
        })
        |> Router.call(@opts)
        |> sent_resp

      assert status == 200

      data =
        body
        |> Jason.decode!()
        |> Map.get("data")

      name =
        data
        |> Map.get("name")

      assert name == "job_test"
    end

    test "SHOW /jobs/:id with unauthorized user" do
      workflow = workflow_fixture()
      step_id = 0
      action = "job_test"

      job_params = %{
        name: action,
        step_id: step_id,
        workflow_id: workflow.id,
        parameters: []
      }

      {result, job} = StepFlow.Jobs.create_job(job_params)

      assert result == :ok

      :timer.sleep(1000)

      job_id =
        job.id
        |> Integer.to_string()

      {status, _headers, _body} =
        conn(:get, "/jobs/" <> job_id)
        |> assign(:current_user, @unauthorized_user)
        |> Router.call(@opts)
        |> sent_resp

      assert status == 403
    end
  end
end
