defmodule StepFlow.RightViewTest do
  use ExUnit.Case
  use Plug.Test

  alias StepFlow.Rights.Right

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  test "render an Right" do
    right = %Right{entity: "workflow::toto", action: ["view", "create"]}

    assert render(StepFlow.RightView, "show.json", %{right: right}) == %{
             data: %{
               entity: right.entity,
               action: right.action
             }
           }
  end

  test "render many Rights" do
    rights = [
      %Right{entity: "workflow::toto", action: ["view", "create"]},
      %Right{entity: "workflow::tata", action: ["view"]}
    ]

    assert render(StepFlow.RightView, "index.json", %{rights: rights}) == %{
             data: [
               %{
                 entity: "workflow::toto",
                 action: ["view", "create"]
               },
               %{
                 entity: "workflow::tata",
                 action: ["view"]
               }
             ]
           }
  end
end
