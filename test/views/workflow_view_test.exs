defmodule StepFlow.WorkflowViewTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox

  # Bring render/3 and render_to_string/3 for testing custom views
  import Phoenix.View

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    user_uuid: "super_toto",
    steps: []
  }

  test "render a Workflow" do
    {:ok, workflow} = StepFlow.Workflows.create_workflow(@workflow)
    workflow = StepFlow.Repo.preload(workflow, [:artifacts, :jobs])

    assert render(StepFlow.WorkflowView, "show.json", %{workflow: workflow, mode: "full"}) == %{
             data: %{
               schema_version: "1.9",
               id: workflow.id,
               artifacts: [],
               created_at: workflow.inserted_at,
               jobs: [],
               notification_hooks: [],
               tags: [],
               identifier: "id",
               version_major: 6,
               version_minor: 5,
               version_micro: 4,
               is_live: false,
               deleted: false,
               reference: "some id",
               user_uuid: "super_toto",
               parameters: [],
               steps: [],
               parent_id: nil
             }
           }
  end

  test "render many Workflows" do
    {:ok, workflow} = StepFlow.Workflows.create_workflow(@workflow)
    workflow = StepFlow.Repo.preload(workflow, [:artifacts, :jobs])

    assert render(StepFlow.WorkflowView, "index.json", %{workflows: %{data: [workflow], total: 1}}) ==
             %{
               data: [
                 %{
                   schema_version: "1.9",
                   id: workflow.id,
                   artifacts: [],
                   created_at: workflow.inserted_at,
                   jobs: [],
                   notification_hooks: [],
                   tags: [],
                   identifier: "id",
                   version_major: 6,
                   version_minor: 5,
                   version_micro: 4,
                   is_live: false,
                   deleted: false,
                   reference: "some id",
                   user_uuid: "super_toto",
                   parameters: [],
                   steps: [],
                   parent_id: nil
                 }
               ],
               total: 1
             }
  end

  test "render a Launch of a Workflow" do
    {:ok, workflow} = StepFlow.Workflows.create_workflow(@workflow)
    workflow = StepFlow.Repo.preload(workflow, [:artifacts, :jobs])

    assert render(StepFlow.WorkflowView, "created.json", %{workflow: workflow}) == %{
             data: %{
               id: workflow.id,
               tags: [],
               identifier: "id",
               version_major: 6,
               version_minor: 5,
               version_micro: 4
             }
           }
  end
end
