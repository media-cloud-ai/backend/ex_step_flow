defmodule StepFlow.Amqp.WorkerStatusConsumerTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Amqp.Connection
  alias StepFlow.Jobs
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    steps: []
  }

  test "consume well formed message with existing job" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id
      })

    message = %{
      job: %{
        job_id: job.id,
        status: "processing"
      },
      worker: %{
        system_info: %{
          docker_container_id: "1234"
        }
      }
    }

    message = message |> Jason.encode!()

    options = [priority: 0]

    result =
      Connection.publish(
        "worker_status",
        message,
        options,
        "worker_response"
      )

    :timer.sleep(1000)

    assert result == :ok
  end
end
