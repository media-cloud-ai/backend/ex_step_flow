defmodule StepFlow.Amqp.WorkerCreatedConsumerTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.Jobs
  alias StepFlow.Jobs.Status
  alias StepFlow.LiveWorkers
  alias StepFlow.Workers.WorkerStatuses
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    steps: []
  }

  test "consume well formed message with existing job" do
    {_, workflow} = Workflows.create_workflow(@workflow)
    StepFlow.HelpersTest.admin_role_fixture()

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        is_live: true,
        step_id: 0,
        workflow_id: workflow.id,
        parameters: [
          %{
            id: "direct_messaging_queue_name",
            type: "string",
            value: "job_live"
          }
        ]
      })

    Status.set_job_status(job.id, :queued)

    LiveWorkers.create_live_worker(%{
      job_id: job.id,
      direct_messaging_queue_name: "direct_messaging_job_live"
    })

    result =
      CommonEmitter.publish_json(
        "worker_created",
        0,
        %{
          direct_messaging_queue_name: "direct_messaging_job_live"
        },
        "worker_response"
      )

    :timer.sleep(6000)

    assert result == :ok

    assert StepFlow.HelpersTest.get_job_last_status(job.id).state == :ready_to_init
  end

  test "consumer well formed message from worker creation response" do
    instance_id = "a814e4de-f286-48d7-a1ff-35f398357f71"

    message = %{
      description: "This worker is just an example.",
      direct_messaging_queue_name: "direct_messaging_" <> instance_id,
      instance_id: instance_id,
      label: "Example",
      parameters: %{},
      queue_name: "job_example_worker",
      sdk_version: "1.0.0",
      short_description: "An example worker",
      version: "1.2.3"
    }

    result =
      CommonEmitter.publish_json(
        "worker_created",
        0,
        message,
        "worker_response"
      )

    :timer.sleep(6000)

    assert result == :ok

    assert WorkerStatuses.get_worker_status(instance_id).activity == :Idle
  end
end
