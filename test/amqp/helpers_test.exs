defmodule StepFlow.Amqp.HelpersTest do
  @moduledoc false

  use ExUnit.Case
  use Plug.Test

  doctest StepFlow

  alias StepFlow.Amqp.Helpers

  test "get delivery mode from configuration" do
    assert 1 == Helpers.get_amqp_delivery_mode()

    System.put_env("AMQP_DELIVERY_MODE", "2")
    assert 2 == Helpers.get_amqp_delivery_mode()

    System.delete_env("AMQP_DELIVERY_MODE")
  end

  test "get AMQP message options" do
    assert [{:persistent, false}] == Helpers.get_amqp_message_options()

    System.put_env("AMQP_DELIVERY_MODE", "2")
    assert [{:persistent, true}] == Helpers.get_amqp_message_options()

    System.delete_env("AMQP_DELIVERY_MODE")
  end
end
