defmodule StepFlow.Amqp.CompletedConsumerTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.Jobs
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    parameters: [
      %{
        id: "param_1",
        type: "string",
        value: "hello"
      }
    ],
    steps: []
  }

  test "consume well formed message" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id
      })

    Jobs.Status.set_job_status(job.id, :queued)
    Jobs.Status.set_job_status(job.id, :ready_to_init)
    Jobs.Status.set_job_status(job.id, :initializing)
    Jobs.Status.set_job_status(job.id, :ready_to_start)
    Jobs.Status.set_job_status(job.id, :starting)
    Jobs.Status.set_job_status(job.id, :processing)

    Workflows.Status.set_workflow_status(workflow.id, :processing)

    result =
      CommonEmitter.publish_json(
        "job_completed",
        0,
        %{
          job_id: job.id,
          status: "completed"
        },
        "job_response"
      )

    :timer.sleep(1000)

    # TO INTRODUCE IN LATER VERSIONS
    assert result == :ok
    assert StepFlow.HelpersTest.get_job_last_status_id(job.id).state == :completed
    assert StepFlow.Workflows.Status.get_last_workflow_status(workflow).state == :completed
  end

  test "Reject message if status not completed" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id
      })

    Jobs.Status.set_job_status(job.id, :queued)
    Jobs.Status.set_job_status(job.id, :ready_to_init)
    Jobs.Status.set_job_status(job.id, :initializing)
    Jobs.Status.set_job_status(job.id, :ready_to_start)
    Jobs.Status.set_job_status(job.id, :starting)
    Jobs.Status.set_job_status(job.id, :processing)

    Workflows.Status.set_workflow_status(workflow.id, :processing)

    :timer.sleep(1000)

    _result =
      CommonEmitter.publish_json(
        "job_completed",
        0,
        %{
          job_id: job.id,
          status: "error"
        },
        "job_response"
      )

    :timer.sleep(1000)

    # TO INTRODUCE IN LATER VERSIONS
    # assert result == :ok
    # assert StepFlow.HelpersTest.get_job_last_status_id(job.id).state == :processing
    # assert StepFlow.Workflows.Status.get_last_workflow_status(workflow).state == :processing
  end

  test "Update parameter by workers response" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id
      })

    assert length(workflow.parameters) == 1
    assert StepFlow.Map.get_by_key_or_atom(workflow.parameters |> List.first(), :value) == "hello"

    Jobs.Status.set_job_status(job.id, :queued)
    Jobs.Status.set_job_status(job.id, :ready_to_init)
    Jobs.Status.set_job_status(job.id, :initializing)
    Jobs.Status.set_job_status(job.id, :ready_to_start)
    Jobs.Status.set_job_status(job.id, :starting)
    Jobs.Status.set_job_status(job.id, :processing)

    Workflows.Status.set_workflow_status(workflow.id, :processing)

    :timer.sleep(1000)

    _result =
      CommonEmitter.publish_json(
        "job_completed",
        0,
        %{
          job_id: job.id,
          status: "completed",
          parameters: [
            %{
              id: "param_1",
              type: "string",
              value: "world"
            },
            %{
              id: "param_2",
              type: "string",
              value: "hello world!"
            }
          ]
        },
        "job_response"
      )

    :timer.sleep(1000)

    workflow = Workflows.get_workflow!(workflow.id)
    assert length(workflow.parameters) == 2
    assert StepFlow.Map.get_by_key_or_atom(workflow.parameters |> List.first(), :value) == "world"

    assert StepFlow.Map.get_by_key_or_atom(workflow.parameters |> List.last(), :value) ==
             "hello world!"
  end
end
