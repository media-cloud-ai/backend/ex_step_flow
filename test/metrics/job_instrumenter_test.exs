defmodule StepFlow.Metrics.JobInstrumenterTest do
  use ExUnit.Case
  use Plug.Test

  alias Prometheus.Metric
  alias StepFlow.Metrics.JobInstrumenter

  setup do
    Enum.map(["created", "stopped", "error", "completed"], fn x ->
      Metric.Counter.reset(
        name: :step_flow_jobs_status_total,
        labels: ["job_test_metrics_metrics", x]
      )
    end)

    Metric.Gauge.reset(
      name: :step_flow_jobs_processing,
      labels: ["job_test_metrics"]
    )

    :ok
  end

  test "inc :step_flow_jobs_status_total for created status" do
    JobInstrumenter.inc(:step_flow_jobs_status_total, "job_test_metrics", "created")

    assert Metric.Counter.value(
             name: :step_flow_jobs_status_total,
             labels: ["job_test_metrics", "created"]
           ) == 1

    assert Metric.Gauge.value(
             name: :step_flow_jobs_processing,
             labels: ["job_test_metrics"]
           ) == 1
  end

  test "inc :step_flow_jobs_status_total for stopped status" do
    JobInstrumenter.inc(:step_flow_jobs_status_total, "job_test_metrics", "stopped")

    assert Metric.Counter.value(
             name: :step_flow_jobs_status_total,
             labels: ["job_test_metrics", "stopped"]
           ) == 1

    assert Metric.Gauge.value(
             name: :step_flow_jobs_processing,
             labels: ["job_test_metrics"]
           ) == -1
  end

  test "inc :step_flow_jobs_status_total for error status" do
    JobInstrumenter.inc(:step_flow_jobs_status_total, "job_test_metrics", "error")

    assert Metric.Counter.value(
             name: :step_flow_jobs_status_total,
             labels: ["job_test_metrics", "error"]
           ) == 1

    assert Metric.Gauge.value(
             name: :step_flow_jobs_processing,
             labels: ["job_test_metrics"]
           ) == -1
  end

  test "inc :step_flow_jobs_status_total for completed status" do
    JobInstrumenter.inc(:step_flow_jobs_status_total, "job_test_metrics", "completed")

    assert Metric.Counter.value(
             name: :step_flow_jobs_status_total,
             labels: ["job_test_metrics", "completed"]
           ) == 1

    assert Metric.Gauge.value(
             name: :step_flow_jobs_processing,
             labels: ["job_test_metrics"]
           ) == -1
  end
end
