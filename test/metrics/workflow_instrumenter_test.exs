defmodule StepFlow.Metrics.WorkflowInstrumenterTest do
  use ExUnit.Case
  use Plug.Test

  alias Prometheus.Metric
  alias StepFlow.Metrics.WorkflowInstrumenter

  test "inc :step_flow_workflows_status_total for created status" do
    WorkflowInstrumenter.inc(
      :step_flow_workflows_status_total,
      "workflow_test_metrics",
      :created_workflow
    )

    assert Metric.Counter.value(
             name: :step_flow_workflows_status_total,
             labels: ["workflow_test_metrics", "created"]
           ) == 1
  end

  test "inc :step_flow_workflows_status_total for stopped status" do
    WorkflowInstrumenter.inc(:step_flow_workflows_status_total, "workflow_test_metrics", :stopped)

    assert Metric.Counter.value(
             name: :step_flow_workflows_status_total,
             labels: ["workflow_test_metrics", "stopped"]
           ) == 1
  end

  test "inc :step_flow_workflows_status_total for error status" do
    WorkflowInstrumenter.inc(:step_flow_workflows_status_total, "workflow_test_metrics", :error)

    assert Metric.Counter.value(
             name: :step_flow_workflows_status_total,
             labels: ["workflow_test_metrics", "error"]
           ) == 1
  end

  test "inc :step_flow_workflows_status_total for completed status" do
    WorkflowInstrumenter.inc(
      :step_flow_workflows_status_total,
      "workflow_test_metrics",
      :completed
    )

    assert Metric.Counter.value(
             name: :step_flow_workflows_status_total,
             labels: ["workflow_test_metrics", "completed"]
           ) == 1
  end
end
