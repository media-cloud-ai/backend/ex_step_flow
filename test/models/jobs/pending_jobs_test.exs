defmodule StepFlow.Jobs.PendingTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Jobs
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    steps: [],
    rights: [
      %{
        action: "create",
        groups: ["administrator"]
      }
    ]
  }

  test "list pending jobs" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id,
        parameters: [
          %{
            id: "direct_messaging_queue_name",
            type: "string",
            value: "job_live"
          }
        ]
      })

    pending_jobs = StepFlow.Jobs.get_pending_jobs_by_type()

    expected_pending_jobs = [%{name: job.name, value: 1}]
    assert pending_jobs == expected_pending_jobs
  end

  test "list pending jobs with job skipped" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    Jobs.create_skipped_job(workflow, 0, "skipped")

    pending_jobs = StepFlow.Jobs.get_pending_jobs_by_type()

    expected_pending_jobs = []
    assert pending_jobs == expected_pending_jobs
  end

  test "list pending jobs with job on error" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    Jobs.create_error_job(workflow, 0, "error", "This is an error")

    pending_jobs = StepFlow.Jobs.get_pending_jobs_by_type()

    expected_pending_jobs = []
    assert pending_jobs == expected_pending_jobs
  end

  test "list pending jobs with job completed" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    Jobs.create_completed_job(workflow, 0, "completed")

    pending_jobs = StepFlow.Jobs.get_pending_jobs_by_type()

    expected_pending_jobs = []
    assert pending_jobs == expected_pending_jobs
  end
end
