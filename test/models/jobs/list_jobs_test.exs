defmodule StepFlow.Jobs.ListJobsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Jobs
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    steps: [],
    rights: [
      %{
        action: "create",
        groups: ["administrator"]
      }
    ]
  }

  test "list jobs with direct message queue" do
    {_, workflow} = Workflows.create_workflow(@workflow)
    StepFlow.HelpersTest.admin_role_fixture()

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id,
        parameters: [
          %{
            id: "direct_messaging_queue_name",
            type: "string",
            value: "job_live"
          }
        ]
      })

    job_query =
      StepFlow.Jobs.list_jobs(%{
        "direct_messaging_queue_name" => "direct_messaging_job_live",
        "roles" => ["administrator"]
      })
      |> Map.get(:data)
      |> List.first()

    assert job_query.id == job.id
  end

  test "list jobs with date filters" do
    {_, workflow} = Workflows.create_workflow(@workflow)
    StepFlow.HelpersTest.admin_role_fixture()

    {_, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow.id,
        parameters: [
          %{
            id: "direct_messaging_queue_name",
            type: "string",
            value: "job_live"
          }
        ]
      })

    today =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.to_date()

    today_start =
      today
      |> NaiveDateTime.new!(Time.new!(0, 0, 0))
      |> NaiveDateTime.to_iso8601()

    today_end =
      today
      |> NaiveDateTime.new!(Time.new!(23, 59, 59))
      |> NaiveDateTime.to_iso8601()

    empty_job_list = %{data: [], page: 0, size: 10, total: 0}

    assert empty_job_list ==
             StepFlow.Jobs.list_jobs(%{
               "end_date" => today_start,
               "roles" => ["administrator"]
             })

    assert empty_job_list ==
             StepFlow.Jobs.list_jobs(%{
               "start_date" => today_end,
               "roles" => ["administrator"]
             })

    found_job =
      StepFlow.Jobs.list_jobs(%{
        "end_date" => today_end,
        "start_date" => today_start,
        "roles" => ["administrator"]
      })
      |> Map.get(:data)
      |> List.first()

    assert found_job.id == job.id
  end
end
