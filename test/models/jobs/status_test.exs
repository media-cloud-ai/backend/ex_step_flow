defmodule StepFlow.Jobs.StatusTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Controllers.Jobs
  alias StepFlow.Jobs.Status
  alias StepFlow.Workflows

  doctest StepFlow

  def workflow_fixture do
    {:ok, workflow} =
      Workflows.create_workflow(%{
        schema_version: "1.9",
        identifier: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
        reference: "9A9F48E4-5585-4E8E-9199-CEFECF85CE14",
        version_major: 1,
        version_minor: 2,
        version_micro: 3
      })

    workflow
  end

  test "test datetime field" do
    :ok = Sandbox.checkout(StepFlow.Repo)
    Sandbox.mode(StepFlow.Repo, {:shared, self()})

    workflow = workflow_fixture()

    job_params = %{
      name: "job_test",
      step_id: 0,
      workflow_id: workflow.id
    }

    {result, job} = StepFlow.Jobs.create_job(job_params)
    assert result == :ok

    now_timestamp = DateTime.utc_now()
    payload = %{description: %{"datetime" => now_timestamp}, job_id: 71, state: "completed"}
    payload2 = %{datetime: now_timestamp, job_id: 71, state: "completed"}
    {:ok, job_status} = Status.set_job_status(job.id, :queued, payload, now_timestamp)
    assert now_timestamp == job_status.datetime
    {:ok, job_status} = Status.set_job_status(job.id, :processing, payload)
    assert now_timestamp == job_status.datetime
    {:ok, job_status} = Status.set_job_status(job.id, :processing, payload2)
    assert now_timestamp == job_status.datetime
  end

  test "get queued state enum label" do
    assert "queued" == Status.state_enum_label(:queued)
    assert "queued" == Status.state_enum_label(0)
  end

  test "get skipped state enum label" do
    assert "skipped" == Status.state_enum_label(:skipped)
    assert "skipped" == Status.state_enum_label(1)
  end

  test "get processing state enum label" do
    assert "processing" == Status.state_enum_label(:processing)
    assert "processing" == Status.state_enum_label(2)
  end

  test "get retrying state enum label" do
    assert "retrying" == Status.state_enum_label(:retrying)
    assert "retrying" == Status.state_enum_label(3)
  end

  test "get error state enum label" do
    assert "error" == Status.state_enum_label(:error)
    assert "error" == Status.state_enum_label(4)
  end

  test "get completed state enum label" do
    assert "completed" == Status.state_enum_label(:completed)
    assert "completed" == Status.state_enum_label(5)
  end

  test "get ready_to_init state enum label" do
    assert "ready_to_init" == Status.state_enum_label(:ready_to_init)
    assert "ready_to_init" == Status.state_enum_label(6)
  end

  test "get ready_to_start state enum label" do
    assert "ready_to_start" == Status.state_enum_label(:ready_to_start)
    assert "ready_to_start" == Status.state_enum_label(7)
  end

  test "get update state enum label" do
    assert "update" == Status.state_enum_label(:update)
    assert "update" == Status.state_enum_label(8)
  end

  test "get stopped state enum label" do
    assert "stopped" == Status.state_enum_label(:stopped)
    assert "stopped" == Status.state_enum_label(9)
  end

  test "get initializing state enum label" do
    assert "initializing" == Status.state_enum_label(:initializing)
    assert "initializing" == Status.state_enum_label(10)
  end

  test "get starting state enum label" do
    assert "starting" == Status.state_enum_label(:starting)
    assert "starting" == Status.state_enum_label(11)
  end

  test "get updating state enum label" do
    assert "updating" == Status.state_enum_label(:updating)
    assert "updating" == Status.state_enum_label(12)
  end

  test "get unknown state enum label" do
    assert "unknown" == Status.state_enum_label(:unknown)
    assert "unknown" == Status.state_enum_label(:other)
    assert "unknown" == Status.state_enum_label(13)
    assert "unknown" == Status.state_enum_label(nil)
    assert "unknown" == Status.state_enum_label(19)
  end

  test "get paused state enum label" do
    assert "paused" == Status.state_enum_label(:paused)
    assert "paused" == Status.state_enum_label(14)
  end

  test "get dropped state enum label" do
    assert "dropped" == Status.state_enum_label(:dropped)
    assert "dropped" == Status.state_enum_label(18)
  end

  test "get deleting state enum label" do
    assert "deleting" == Status.state_enum_label(:deleting)
    assert "deleting" == Status.state_enum_label(17)
  end

  test "get last status" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:17:32],
        datetime: ~N[2020-01-14 15:17:32],
        job_id: 123,
        state: :skipped,
        updated_at: ~N[2020-01-14 15:17:32]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    last_status = Jobs.get_last_status(status_list)

    assert :skipped == last_status.state
  end

  test "get last status with unordered timestamps" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:01],
        datetime: ~N[2020-01-14 15:15:01],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:01]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:15:00]
      }
    ]

    last_status = Jobs.get_last_status(status_list)

    assert :processing == last_status.state
  end

  test "get create action" do
    status_list = [
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:17:00],
        datetime: ~N[2020-01-14 15:17:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:17:00]
      },
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    action =
      Jobs.get_last_status(status_list)
      |> Jobs.get_action()

    assert "create" == action
  end

  test "get init action" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:17:32],
        datetime: ~N[2020-01-14 15:17:32],
        job_id: 123,
        state: :ready_to_init,
        updated_at: ~N[2020-01-14 15:17:32]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    action =
      Jobs.get_last_status(status_list)
      |> Jobs.get_action()

    assert "init_process" == action
  end

  test "get start action" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:17:32],
        datetime: ~N[2020-01-14 15:17:32],
        job_id: 123,
        state: :ready_to_start,
        updated_at: ~N[2020-01-14 15:17:32]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    action =
      Jobs.get_last_status(status_list)
      |> Jobs.get_action()

    assert "start_process" == action
  end

  test "get delete action" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:17:32],
        datetime: ~N[2020-01-14 15:17:32],
        job_id: 123,
        state: :stopped,
        updated_at: ~N[2020-01-14 15:17:32]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    action =
      Jobs.get_last_status(status_list)
      |> Jobs.get_action()

    assert "delete" == action
  end

  test "get action parameter" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:17:32],
        datetime: ~N[2020-01-14 15:17:32],
        job_id: 123,
        state: :stopped,
        updated_at: ~N[2020-01-14 15:17:32]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    action =
      Jobs.get_last_status(status_list)
      |> Jobs.get_action_parameter()

    assert [%{"id" => "action", "type" => "string", "value" => "delete"}] == action
  end

  test "completed priority over other status" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 460,
        inserted_at: ~N[2020-01-14 15:16:40],
        datetime: ~N[2020-01-14 15:16:40],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:16:40]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:15:32],
        datetime: ~N[2020-01-14 15:15:32],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:15:32]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :error,
        updated_at: ~N[2020-01-14 15:16:03]
      },
      %{
        description: %{},
        id: 462,
        inserted_at: ~N[2020-01-14 15:17:32],
        datetime: ~N[2020-01-14 15:17:32],
        job_id: 123,
        state: :completed,
        updated_at: ~N[2020-01-14 15:17:32]
      },
      %{
        description: %{},
        id: 459,
        inserted_at: ~N[2020-01-14 15:16:30],
        datetime: ~N[2020-01-14 15:16:30],
        job_id: 123,
        state: :retrying,
        updated_at: ~N[2020-01-14 15:16:30]
      },
      %{
        description: %{},
        id: 461,
        inserted_at: ~N[2020-01-14 15:17:33],
        datetime: ~N[2020-01-14 15:17:33],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:17:33]
      }
    ]

    status = Jobs.get_last_status(status_list)

    assert %{
             description: %{},
             id: 462,
             inserted_at: ~N[2020-01-14 15:17:32],
             datetime: ~N[2020-01-14 15:17:32],
             job_id: 123,
             state: :completed,
             updated_at: ~N[2020-01-14 15:17:32]
           } ==
             status
  end

  test "status starting from last retrying" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 459,
        inserted_at: ~N[2020-01-14 15:16:30],
        datetime: ~N[2020-01-14 15:16:30],
        job_id: 123,
        state: :retrying,
        updated_at: ~N[2020-01-14 15:16:30]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:15:32],
        datetime: ~N[2020-01-14 15:15:32],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:15:32]
      },
      %{
        description: %{},
        id: 461,
        inserted_at: ~N[2020-01-14 15:17:33],
        datetime: ~N[2020-01-14 15:17:33],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:17:33]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :error,
        updated_at: ~N[2020-01-14 15:16:03]
      },
      %{
        description: %{},
        id: 460,
        inserted_at: ~N[2020-01-14 15:16:40],
        datetime: ~N[2020-01-14 15:16:40],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:16:40]
      }
    ]

    status = Jobs.get_last_status(status_list)

    assert %{
             description: %{},
             id: 461,
             inserted_at: ~N[2020-01-14 15:17:33],
             datetime: ~N[2020-01-14 15:17:33],
             job_id: 123,
             state: :processing,
             updated_at: ~N[2020-01-14 15:17:33]
           } ==
             status
  end

  test "retrying as last status" do
    status_list = [
      %{
        description: %{},
        id: 456,
        inserted_at: ~N[2020-01-14 15:15:00],
        datetime: ~N[2020-01-14 15:15:00],
        job_id: 123,
        state: :queued,
        updated_at: ~N[2020-01-14 15:15:00]
      },
      %{
        description: %{},
        id: 459,
        inserted_at: ~N[2020-01-14 15:16:30],
        datetime: ~N[2020-01-14 15:16:30],
        job_id: 123,
        state: :retrying,
        updated_at: ~N[2020-01-14 15:16:30]
      },
      %{
        description: %{},
        id: 457,
        inserted_at: ~N[2020-01-14 15:15:32],
        datetime: ~N[2020-01-14 15:15:32],
        job_id: 123,
        state: :processing,
        updated_at: ~N[2020-01-14 15:15:32]
      },
      %{
        description: %{},
        id: 458,
        inserted_at: ~N[2020-01-14 15:16:03],
        datetime: ~N[2020-01-14 15:16:03],
        job_id: 123,
        state: :error,
        updated_at: ~N[2020-01-14 15:16:03]
      }
    ]

    status = Jobs.get_last_status(status_list)

    assert %{
             description: %{},
             id: 459,
             inserted_at: ~N[2020-01-14 15:16:30],
             datetime: ~N[2020-01-14 15:16:30],
             job_id: 123,
             state: :retrying,
             updated_at: ~N[2020-01-14 15:16:30]
           } ==
             status
  end
end
