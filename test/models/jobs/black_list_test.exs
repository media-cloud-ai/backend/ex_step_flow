defmodule StepFlow.Jobs.BlackListTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Controllers.BlackList
  alias StepFlow.Jobs
  alias StepFlow.Jobs.Status
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    steps: [],
    rights: [
      %{
        action: "create",
        groups: ["administrator"]
      }
    ]
  }

  defp create_job(workflow_id) do
    {:ok, job} =
      Jobs.create_job(%{
        name: "job_test",
        step_id: 0,
        workflow_id: workflow_id,
        parameters: []
      })

    {:ok, _status} = Status.set_job_status(job.id, :queued)

    {:ok, job}
  end

  test "get black list" do
    {_, workflow} = Workflows.create_workflow(@workflow)

    {_, job_1} = create_job(workflow.id)

    assert !BlackList.contains?(job_1.id)
    assert [job_1.id] == BlackList.add!(job_1.id)
    assert BlackList.contains?(job_1.id)
    assert [job_1.id] == BlackList.add!(job_1.id)

    {_, job_2} = create_job(workflow.id)
    BlackList.add!(job_2.id)
    {_, job_3} = create_job(workflow.id)
    BlackList.add!(job_3.id)

    black_list = BlackList.get!()
    assert [job_1.id, job_2.id, job_3.id] == black_list

    assert Poison.encode!(%{type: "sync_black_list"}) == BlackList.get_json_sync_message()
    assert Poison.encode!(%{type: "get_black_list"}) == BlackList.get_json_request_message()

    assert [job_1.id, job_3.id] == BlackList.delete!(job_2.id)
    assert [job_1.id, job_3.id] == BlackList.delete!(job_2.id)
    assert [job_1.id] == BlackList.delete!(job_3.id)
    assert [] == BlackList.delete!(job_1.id)
  end
end
