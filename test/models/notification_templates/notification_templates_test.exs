defmodule StepFlow.Amqp.ListNotificationTemplatesTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox

  alias StepFlow.NotificationHooks.NotificationTemplates

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  test "test notification template creation" do
    {_, notification_template} =
      NotificationTemplates.create_notification_template(%{
        template_name: "notification_template_test",
        template_headers: "{}",
        template_body: "{}"
      })

    {:error, _} =
      NotificationTemplates.create_notification_template(%{
        template_name: "notification_template_test",
        template_headers: "{}",
        template_body: "{}"
      })

    {_, _notification_template} =
      NotificationTemplates.create_notification_template(%{
        template_name: "a_notification_template_test_2",
        template_headers: "{}",
        template_body: "{}"
      })

    notification_template_query =
      NotificationTemplates.list_notification_templates()
      |> Map.get(:data)

    assert length(notification_template_query) == 2

    first_notification_template = notification_template_query |> List.first()

    assert first_notification_template == notification_template
  end

  test "test notification template deletion" do
    {_, _notification_template} =
      NotificationTemplates.create_notification_template(%{
        template_name: "notification_template_test",
        template_headers: "{}",
        template_body: "{}"
      })

    {_, notification_template_2} =
      NotificationTemplates.create_notification_template(%{
        template_name: "notification_template_test_2",
        template_headers: "{}",
        template_body: "{}"
      })

    {result, _} = NotificationTemplates.delete_notification_template(notification_template_2)
    assert result == :ok

    notification_template_query =
      NotificationTemplates.list_notification_templates()
      |> Map.get(:data)

    assert length(notification_template_query) == 1

    catch_error(NotificationTemplates.delete_notification_template(notification_template_2))
  end

  test "test notification template get" do
    {_, notification_template} =
      NotificationTemplates.create_notification_template(%{
        template_name: "notification_template_test",
        template_headers: "{}",
        template_body: "{}"
      })

    notification_template_fetched =
      NotificationTemplates.get_notification_template!(notification_template.id)

    assert notification_template == notification_template_fetched

    notification_template_fetched =
      NotificationTemplates.get_notification_template_by_name("notification_template_test")

    assert notification_template == notification_template_fetched
  end

  test "test notification template update" do
    {_, notification_template} =
      NotificationTemplates.create_notification_template(%{
        template_name: "notification_template_test",
        template_headers: "{}",
        template_body: "{}"
      })

    update_notification_template_params = %{
      template_name: "notification_template_test",
      template_headers: "{\'test\' : \'test\'}",
      template_body: "{\'test\': \'test\'}"
    }

    {:ok, updated_template} =
      NotificationTemplates.update_notification_template(
        notification_template,
        update_notification_template_params
      )

    assert updated_template.template_headers ==
             update_notification_template_params.template_headers

    assert updated_template.template_body == update_notification_template_params.template_body
  end
end
