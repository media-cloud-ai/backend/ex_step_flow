defmodule StepFlow.WorkflowDefinitionsTest do
  use ExUnit.Case, async: true

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.WorkflowDefinitions

  doctest StepFlow.WorkflowDefinitions

  setup do
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  describe "workflow_definitions" do
    @administrator %{
      name: "administrator",
      rights: [%Right{entity: "workflow::*", action: ["view"]}]
    }

    defp role_fixture(role) do
      {:ok, role} = Roles.create_role(role)

      role
    end

    test "list_workflow_definitions/0 returns all workflow_definitions" do
      role_fixture(@administrator)

      assert %{
               data: [_, _, workflow, _],
               page: 0,
               size: 10,
               total: 4
             } = WorkflowDefinitions.list_workflow_definitions(%{roles: ["administrator"]})

      assert 5 == length(workflow.steps)
      assert 3 == length(workflow.start_parameters)
      assert 1 == length(workflow.parameters)
    end

    test "list_workflow_definitions/0 returns workflow_definitions with version 0.0.1" do
      role_fixture(@administrator)

      result =
        WorkflowDefinitions.list_workflow_definitions(%{
          "versions" => ["0.0.1"],
          roles: ["administrator"]
        })

      assert %{
               data: [workflow],
               page: 0,
               size: 10,
               total: 1
             } = result

      assert 0 == workflow.version_major
      assert 0 == workflow.version_minor
      assert 1 == workflow.version_micro
    end

    test "list_workflow_definitions/0 returns workflow_definitions with latest version" do
      role_fixture(@administrator)

      result =
        WorkflowDefinitions.list_workflow_definitions(%{
          "versions" => ["latest"],
          roles: ["administrator"]
        })

      assert %{
               data: [_, _, workflow],
               page: 0,
               size: 10,
               total: 3
             } = result

      assert 0 == workflow.version_major
      assert 1 == workflow.version_minor
      assert 0 == workflow.version_micro
    end

    test "list_workflow_definitions/0 returns workflow_definitions with label matching 'ranscript' with missing 'T'" do
      role_fixture(@administrator)

      result =
        WorkflowDefinitions.list_workflow_definitions(%{
          "search" => "ranscript",
          roles: ["administrator"]
        })

      assert %{
               data: [_ | _],
               page: 0,
               size: 10,
               total: 2
             } = result
    end

    test "list_workflow_definitions/0 returns workflow_definitions with simple mode" do
      role_fixture(@administrator)

      result =
        WorkflowDefinitions.list_workflow_definitions(%{
          "mode" => "simple",
          roles: ["administrator"]
        })

      assert %{
               data: [workfklow | _],
               page: 0,
               size: 10,
               total: 4
             } = result

      assert Map.has_key?(workfklow, :identifier)
      assert not Map.has_key?(workfklow, :steps)
    end

    test "list_workflow_definitions/0 returns workflow_definitions with complex query" do
      role_fixture(@administrator)

      result =
        WorkflowDefinitions.list_workflow_definitions(%{
          "mode" => "simple",
          "search" => "Transcript",
          "versions" => ["0.0.1"],
          roles: ["administrator"]
        })

      assert %{
               data: [workflow],
               page: 0,
               size: 10,
               total: 1
             } = result

      assert not Map.has_key?(workflow, :steps)
      assert String.starts_with?(workflow.label, "Transcript")
      assert 0 == workflow.version_major
      assert 0 == workflow.version_minor
      assert 1 == workflow.version_micro
    end

    test "load twice same workflow" do
      role_fixture(@administrator)

      WorkflowDefinitions.load_workflows_in_database()

      result =
        WorkflowDefinitions.list_workflow_definitions(%{
          "mode" => "simple",
          "search" => "Transcript",
          "versions" => ["0.0.1"],
          roles: ["administrator"]
        })

      assert %{
               data: [workflow],
               page: 0,
               size: 10,
               total: 1
             } = result

      assert not Map.has_key?(workflow, :steps)
      assert String.starts_with?(workflow.label, "Transcript")
      assert 0 == workflow.version_major
      assert 0 == workflow.version_minor
      assert 1 == workflow.version_micro
    end
  end
end
