defmodule StepFlow.Roles.RolesTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Rights.Right
  alias StepFlow.Roles

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  test "test role creation" do
    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ]
      })

    {_, _role} =
      Roles.create_role(%{
        name: "role_test2",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: []
          },
          %Right{
            entity: "backend::user",
            action: []
          }
        ]
      })

    role_query =
      StepFlow.Roles.list_roles()
      |> Map.get(:data)

    assert length(role_query) == 2

    first_role = role_query |> List.first()

    assert first_role == role
  end

  test "test role deletion" do
    {_, _role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ]
      })

    {_, role2} =
      Roles.create_role(%{
        name: "role_test2",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: []
          },
          %Right{
            entity: "backend::user",
            action: []
          }
        ]
      })

    role2 |> Roles.delete_role()

    role_query =
      StepFlow.Roles.list_roles()
      |> Map.get(:data)

    assert length(role_query) == 1
  end

  test "test role update" do
    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ]
      })

    more_rights = %{
      rights: [
        %Right{
          entity: "endpoint::workflow",
          action: ["view", "create"]
        },
        %Right{
          entity: "endpoint::statistics",
          action: ["view", "create"]
        },
        %Right{
          entity: "backend::user",
          action: ["view", "create"]
        }
      ]
    }

    {:ok, updated_role} = role |> Roles.update_role(more_rights)

    role_query =
      StepFlow.Roles.list_roles()
      |> Map.get(:data)
      |> List.first()

    assert role_query == updated_role
  end

  test "test role get" do
    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ]
      })

    role_fetched = Roles.get_role(role.id)

    assert role == role_fetched

    {_, role2} =
      Roles.create_role(%{
        name: "role_test2",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ],
        is_unused: false
      })

    role_fetched = Roles.get_roles(["role_test", "role_test2"])

    assert [role2, role] == role_fetched
  end

  test "test role delete" do
    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ]
      })

    {:ok, _} = Roles.delete_role(role)

    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          }
        ],
        is_unused: false
      })

    {:error, _} = Roles.delete_role(role)
  end

  test "test check rights" do
    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "endpoint::workflow",
            action: ["view", "create"]
          },
          %Right{
            entity: "backend::user",
            action: ["view", "create"]
          },
          %Right{
            entity: "endpoint::worker",
            action: ["*"]
          },
          %Right{
            entity: "*",
            action: ["update", "delete"]
          }
        ]
      })

    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint", "view") == false
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint", "create") == false

    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint", "update")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint", "delete")

    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::workflow", "create")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::workflow", "view")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::workflow", "update")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::workflow", "delete")

    assert StepFlow.Controllers.Roles.has_right?([role], "backend", "view") == false
    assert StepFlow.Controllers.Roles.has_right?([role], "backend", "create") == false

    assert StepFlow.Controllers.Roles.has_right?([role], "backend", "update")
    assert StepFlow.Controllers.Roles.has_right?([role], "backend", "delete")

    assert StepFlow.Controllers.Roles.has_right?([role], "backend::user", "create")
    assert StepFlow.Controllers.Roles.has_right?([role], "backend::user", "view")

    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::worker", "view")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::worker", "create")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::worker", "update")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::worker", "delete")
    assert StepFlow.Controllers.Roles.has_right?([role], "endpoint::worker", "whatever")

    assert StepFlow.Controllers.Roles.has_right?([role], "whatever", "update")
    assert StepFlow.Controllers.Roles.has_right?([role], "whatever", "delete")

    result =
      StepFlow.Controllers.Roles.get_rights_for_entity_type_and_action(
        [role],
        "endpoint",
        "create"
      )

    assert result == [
             %StepFlow.Rights.Right{action: ["view", "create"], entity: "endpoint::workflow"},
             %StepFlow.Rights.Right{action: ["*"], entity: "endpoint::worker"}
           ]

    result =
      StepFlow.Controllers.Roles.get_rights_for_entity_type_and_action([role], "worker", "update")

    assert result == [
             %StepFlow.Rights.Right{action: ["update", "delete"], entity: "*"}
           ]
  end

  test "test check rights wilcards after entity" do
    {_, role} =
      Roles.create_role(%{
        name: "role_test",
        rights: [
          %Right{
            entity: "workflow::*",
            action: ["create", "update", "delete"]
          }
        ]
      })

    assert StepFlow.Controllers.Roles.has_right?([role], "workflow::toto", "create")
    assert StepFlow.Controllers.Roles.has_right?([role], "workflow::tata", "create")

    assert !StepFlow.Controllers.Roles.has_right?([role], "endpoint", "create")
    assert !StepFlow.Controllers.Roles.has_right?([role], "workflow::toto", "view")
  end
end
