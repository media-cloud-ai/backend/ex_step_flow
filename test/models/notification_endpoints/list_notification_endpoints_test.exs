defmodule StepFlow.Amqp.ListNotificationEndpointsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox

  alias StepFlow.NotificationHooks.NotificationEndpoints

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {conn, _channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.close_amqp_connection(conn)
    end)
  end

  test "test notification endpoint creation" do
    {_, notification_endpoint} =
      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "notification_endpoint_test",
        endpoint_url: "test_value"
      })

    {:error, _} =
      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "notification_endpoint_test",
        endpoint_url: "other_test_value"
      })

    {_, _notification_endpoint} =
      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "a_notification_endpoint_test_2",
        endpoint_url: "test_value",
        endpoint_credentials: "test_credentials"
      })

    notification_endpoint_query =
      NotificationEndpoints.list_notification_endpoints()
      |> Map.get(:data)

    assert length(notification_endpoint_query) == 2

    first_notification_endpoint = notification_endpoint_query |> List.first()

    assert first_notification_endpoint == notification_endpoint
  end

  test "test notification endpoint deletion" do
    {_, _notification_endpoint} =
      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "notification_endpoint_test",
        endpoint_url: "test_value"
      })

    {_, notification_endpoint_2} =
      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "notification_endpoint_test_2",
        endpoint_url: "test_value",
        endpoint_credentials: ""
      })

    {result, _} = NotificationEndpoints.delete_notification_endpoint(notification_endpoint_2)
    assert result == :ok

    notification_endpoint_query =
      NotificationEndpoints.list_notification_endpoints()
      |> Map.get(:data)

    assert length(notification_endpoint_query) == 1

    catch_error(NotificationEndpoints.delete_notification_endpoint(notification_endpoint_2))
  end

  test "test notification endpoint get" do
    {_, notification_endpoint} =
      NotificationEndpoints.create_notification_endpoint(%{
        endpoint_placeholder: "notification_endpoint_test",
        endpoint_url: "test_value"
      })

    notification_endpoint_fetched =
      NotificationEndpoints.get_notification_endpoint!(notification_endpoint.id)

    assert notification_endpoint == notification_endpoint_fetched

    notification_endpoint_fetched =
      NotificationEndpoints.get_notification_endpoint_by_placeholder("notification_endpoint_test")

    assert notification_endpoint == notification_endpoint_fetched
  end
end
