defmodule StepFlow.RunWorkflows.AllowFailureWorkflowTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Step

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 8)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "Two steps workflow",
      tags: ["test"],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          allow_failure: true,
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["coucou.mov"]
            }
          ]
        },
        %{
          id: 1,
          name: "job_test",
          icon: "step_icon",
          label: "My second step",
          parent_ids: [0],
          required_to_start: [0],
          parameters: []
        }
      ],
      parameters: []
    }

    @workflow_definition_multiple %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "Two steps workflow",
      tags: ["test"],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          allow_failure: true,
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["coucou.mov", "hello.mov"]
            }
          ]
        },
        %{
          id: 1,
          name: "job_test",
          icon: "step_icon",
          label: "My second step",
          allow_failure: true,
          parent_ids: [0],
          required_to_start: [0],
          parameters: []
        },
        %{
          id: 2,
          name: "job_test",
          icon: "step_icon",
          label: "My third step",
          parent_ids: [1],
          required_to_start: [1],
          parameters: []
        }
      ],
      parameters: []
    }

    test "run simple workflow with 2 steps" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 1)
      StepFlow.HelpersTest.check(workflow.id, 0, 1)
      StepFlow.HelpersTest.change_job_status(workflow, 0, :error)

      {:ok, "started"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 2)
      StepFlow.HelpersTest.check(workflow.id, 0, 1)
      StepFlow.HelpersTest.check(workflow.id, 1, 1)
      StepFlow.HelpersTest.complete_jobs(workflow.id, 1)

      {:ok, "completed"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 2)
    end

    test "run simple workflow with 2 steps with multiple inputs" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition_multiple)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 2)
      StepFlow.HelpersTest.check(workflow.id, 0, 2)

      StepFlow.HelpersTest.get_jobs(workflow.id, 0)
      |> List.first()
      |> StepFlow.HelpersTest.error_job()

      {:ok, "still_processing"} = Step.start_next(workflow)

      StepFlow.HelpersTest.get_jobs(workflow.id, 0)
      |> List.last()
      |> StepFlow.HelpersTest.complete_job()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 4)
      StepFlow.HelpersTest.check(workflow.id, 0, 2)
      StepFlow.HelpersTest.check(workflow.id, 1, 2)

      StepFlow.HelpersTest.get_jobs(workflow.id, 1)
      |> List.first()
      |> StepFlow.HelpersTest.error_job()

      {:ok, "still_processing"} = Step.start_next(workflow)

      StepFlow.HelpersTest.get_jobs(workflow.id, 1)
      |> List.last()
      |> StepFlow.HelpersTest.error_job()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 6)
      StepFlow.HelpersTest.check(workflow.id, 0, 2)
      StepFlow.HelpersTest.check(workflow.id, 1, 2)
      StepFlow.HelpersTest.check(workflow.id, 2, 2)

      StepFlow.HelpersTest.get_jobs(workflow.id, 2)
      |> List.first()
      |> StepFlow.HelpersTest.error_job()

      {:ok, "still_processing"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 6)
    end
  end
end
