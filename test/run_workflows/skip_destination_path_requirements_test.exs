defmodule StepFlow.RunWorkflows.SkipDestinationPathRequirementsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Step

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 3)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.9",
      identifier: "workflow_with_skipped_destination_path",
      version_major: 0,
      version_minor: 1,
      version_micro: 3,
      reference: "some id_bis",
      icon: "custom_icon",
      label: "Jobs with skipped path",
      tags: ["test"],
      parameters: [],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "custom_icon",
          label: "Job step",
          skip_destination_path: true,
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: [
                "myfile.mov"
              ]
            },
            %{
              id: "destination_path",
              type: "template",
              value: "{workflow_id}/{date_time}/filename.mov"
            }
          ]
        },
        %{
          id: 1,
          name: "job_test",
          icon: "step_icon",
          parent_ids: [0],
          required_to_start: [0],
          label: "Job step n2",
          parameters: [
            %{
              id: "destination_filename",
              type: "array_of_templates",
              value: [
                "filename_v2.mov"
              ]
            }
          ]
        },
        %{
          id: 2,
          name: "job_test",
          icon: "step_icon",
          parent_ids: [1],
          required_to_start: [1],
          label: "Job step n3",
          parameters: []
        }
      ]
    }

    test "run a workflow with three steps to check if requirements paths are correct" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition)

      _step =
        workflow.steps
        |> List.first()

      # work_directory = Step.Helpers.get_work_directory(step)

      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 1)
      StepFlow.HelpersTest.check(workflow.id, 0, 1)
      StepFlow.HelpersTest.complete_jobs(workflow.id, 0)

      jobs = StepFlow.HelpersTest.get_jobs(workflow.id, 0)

      requirements =
        jobs
        |> List.first()
        |> Map.get(:parameters)
        |> Enum.filter(fn param -> param["id"] == "requirements" end)
        |> List.first()
        |> Map.get("value")
        |> Map.get("paths")

      assert requirements == []

      {:ok, "started"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 2)
      StepFlow.HelpersTest.check(workflow.id, 1, 1)
      StepFlow.HelpersTest.complete_jobs(workflow.id, 1)

      jobs = StepFlow.HelpersTest.get_jobs(workflow.id, 1)

      requirements =
        jobs
        |> List.first()
        |> Map.get(:parameters)
        |> Enum.filter(fn param -> param["id"] == "requirements" end)
        |> List.first()
        |> Map.get("value")
        |> Map.get("paths")

      assert requirements == []

      {:ok, "started"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 3)
      StepFlow.HelpersTest.check(workflow.id, 2, 1)
      StepFlow.HelpersTest.complete_jobs(workflow.id, 2)

      jobs = StepFlow.HelpersTest.get_jobs(workflow.id, 2)

      requirements =
        jobs
        |> List.first()
        |> Map.get(:parameters)
        |> Enum.filter(fn param -> param["id"] == "requirements" end)
        |> List.first()
        |> Map.get("value")
        |> Map.get("paths")

      assert requirements == []

      {:ok, "completed"} = Step.start_next(workflow)
    end
  end
end
