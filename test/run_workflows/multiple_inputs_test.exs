defmodule StepFlow.RunWorkflows.MultipleInputsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.Step

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 9)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "Multiple inputs",
      tags: ["test"],
      parameters: [],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: [
                "my_file_1.mov",
                "my_file_2.mov",
                "my_file_3.mov"
              ]
            }
          ]
        },
        %{
          id: 1,
          name: "job_test",
          icon: "step_icon",
          label: "My second step",
          mode: "one_for_one",
          parameters: [
            %{
              id: "requirements",
              type: "requirements",
              value: %{
                paths: []
              }
            }
          ],
          required_to_start: [
            0
          ],
          parent_ids: [
            0
          ]
        },
        %{
          id: 2,
          name: "job_test",
          icon: "step_icon",
          label: "My third step",
          mode: "one_for_one",
          parameters: [
            %{
              id: "requirements",
              type: "requirements",
              value: %{
                paths: []
              }
            }
          ],
          required_to_start: [
            1
          ],
          parent_ids: [
            1
          ]
        }
      ]
    }

    @workflow_definition_skip %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "Multiple inputs",
      tags: ["test"],
      parameters: [],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          skip_destination_path: true,
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: [
                "my_file_1.mov",
                "my_file_2.mov",
                "my_file_3.mov"
              ]
            }
          ]
        },
        %{
          id: 1,
          name: "job_test",
          icon: "step_icon",
          label: "My second step",
          mode: "one_for_one",
          skip_destination_path: true,
          parameters: [
            %{
              id: "requirements",
              type: "requirements",
              value: %{
                paths: []
              }
            }
          ],
          required_to_start: [
            0
          ],
          parent_ids: [
            0
          ]
        },
        %{
          id: 2,
          name: "job_test",
          icon: "step_icon",
          label: "My third step",
          mode: "one_for_one",
          parameters: [
            %{
              id: "requirements",
              type: "requirements",
              value: %{
                paths: []
              }
            }
          ],
          required_to_start: [
            1
          ],
          parent_ids: [
            1
          ]
        }
      ]
    }

    test "run simple workflow with 3 step" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 3)
      StepFlow.HelpersTest.check(workflow.id, 0, 3)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 0)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 6)
      StepFlow.HelpersTest.check(workflow.id, 1, 3)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 1)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 9)
      StepFlow.HelpersTest.check(workflow.id, 2, 3)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 2)

      {:ok, "completed"} = Step.start_next(workflow)
    end

    test "run simple workflow with 3 step with error" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 3)
      StepFlow.HelpersTest.check(workflow.id, 0, 3)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 0)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 6)
      StepFlow.HelpersTest.check(workflow.id, 1, 3)

      [job | jobs] = StepFlow.HelpersTest.get_jobs(workflow.id, 1)

      StepFlow.HelpersTest.complete_job(job)

      [job | jobs] = jobs

      StepFlow.HelpersTest.complete_job(job)

      [job | _] = jobs

      # Simulate phantom job status
      StepFlow.HelpersTest.processing_job(job, false)

      {:ok, "still_processing"} = Step.start_next(workflow)

      StepFlow.HelpersTest.error_job(job)

      {:ok, "still_processing"} = Step.start_next(workflow)
    end

    test "run simple workflow with 3 step with skip destination path" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition_skip)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 3)
      StepFlow.HelpersTest.check(workflow.id, 0, 3)

      StepFlow.HelpersTest.processing_jobs(workflow.id, 0)

      :timer.sleep(200)

      [job | jobs] = StepFlow.HelpersTest.get_jobs(workflow.id, 0)

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_1.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      [job | jobs] = jobs

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_2.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      [job | _] = jobs

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_3.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)
      {:ok, "still_processing"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 6)
      StepFlow.HelpersTest.check(workflow.id, 1, 3)
      StepFlow.HelpersTest.processing_jobs(workflow.id, 1)

      [job | jobs] = StepFlow.HelpersTest.get_jobs(workflow.id, 1)

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_1.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      [job | jobs] = jobs

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_2.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      [job | _] = jobs

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_3.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      {:ok, "still_processing"} = Step.start_next(workflow)
      StepFlow.HelpersTest.check(workflow.id, 9)
      StepFlow.HelpersTest.check(workflow.id, 2, 3)

      StepFlow.HelpersTest.processing_jobs(workflow.id, 2)

      {:ok, "still_processing"} = Step.start_next(workflow)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 2)

      {:ok, "completed"} = Step.start_next(workflow)
    end

    test "run simple workflow with 3 step with error and skip destination path" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition_skip)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 3)
      StepFlow.HelpersTest.check(workflow.id, 0, 3)

      StepFlow.HelpersTest.processing_jobs(workflow.id, 0)

      :timer.sleep(200)

      [job | jobs] = StepFlow.HelpersTest.get_jobs(workflow.id, 0)

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_1.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      [job | jobs] = jobs

      _result =
        CommonEmitter.publish_json(
          "job_completed",
          0,
          %{
            job_id: job.id,
            status: "completed",
            description: %{
              "id" => "message",
              "type" => "string",
              "value" => "Job completed"
            },
            destination_paths: [
              "my_file_2.mov"
            ]
          },
          "job_response"
        )

      :timer.sleep(500)

      [job | _] = jobs

      CommonEmitter.publish_json(
        "job_completed",
        0,
        %{
          job_id: job.id,
          status: "completed",
          description: %{
            "id" => "message",
            "type" => "string",
            "value" => "Job completed"
          },
          destination_paths: [
            "my_file_3.mov"
          ]
        },
        "job_response"
      )

      :timer.sleep(500)
      {:ok, "still_processing"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 6)
      StepFlow.HelpersTest.check(workflow.id, 1, 3)

      [job | jobs] = StepFlow.HelpersTest.get_jobs(workflow.id, 1)
      StepFlow.HelpersTest.processing_job(job)

      CommonEmitter.publish_json(
        "job_completed",
        0,
        %{
          job_id: job.id,
          status: "completed",
          description: %{
            "id" => "message",
            "type" => "string",
            "value" => "Job completed"
          },
          destination_paths: [
            "my_file_1.mov"
          ]
        },
        "job_response"
      )

      :timer.sleep(500)

      [job | jobs] = jobs
      StepFlow.HelpersTest.error_job(job)

      [job | _] = jobs
      StepFlow.HelpersTest.processing_job(job)

      CommonEmitter.publish_json(
        "job_completed",
        0,
        %{
          job_id: job.id,
          status: "completed",
          description: %{
            "id" => "message",
            "type" => "string",
            "value" => "Job completed"
          },
          destination_paths: [
            "my_file_3.mov"
          ]
        },
        "job_response"
      )

      :timer.sleep(500)

      {:ok, "still_processing"} = Step.start_next(workflow)
    end
  end
end
