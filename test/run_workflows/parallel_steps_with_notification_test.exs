defmodule StepFlow.RunWorkflows.ParallelStepsWithNotificationTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Step

  alias StepFlow.NotificationHooks.NotificationEndpoints
  alias StepFlow.NotificationHooks.NotificationTemplates

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 4)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.13",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "Parallel steps with notification",
      tags: ["test"],
      parameters: [],
      notification_hooks: [
        %{
          label: "Teams Alerte",
          endpoint: "TEST_NOTIFICATION_URL",
          conditions: %{
            workflow_completed: "template_workflow_completed"
          }
        }
      ],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: [
                "my_file_1.mov"
              ]
            }
          ]
        },
        %{
          id: 1,
          required_to_start: [0],
          parent_ids: [0],
          name: "job_test",
          icon: "step_icon",
          label: "First parallel step",
          parameters: []
        },
        %{
          id: 2,
          required_to_start: [0],
          parent_ids: [0],
          name: "job_test",
          icon: "step_icon",
          label: "Second parallel step",
          parameters: []
        },
        %{
          id: 3,
          required_to_start: [1, 2],
          parent_ids: [1, 2],
          name: "job_test",
          icon: "step_icon",
          label: "Join last step",
          parameters: []
        }
      ]
    }

    test "run parallel steps on a same workflow" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition)
      StepFlow.HelpersTest.admin_role_fixture()

      notification_endpoint_1 = %{
        endpoint_placeholder: "TEST_NOTIFICATION_URL",
        endpoint_url: "test_1"
      }

      {result, _} = NotificationEndpoints.create_notification_endpoint(notification_endpoint_1)
      assert result == :ok

      notification_template_1 = %{
        template_name: "template_workflow_completed",
        template_headers: "{\'test\' : \'test\'}",
        template_body: "{\'test\': \'test\'}"
      }

      {result, _} = NotificationTemplates.create_notification_template(notification_template_1)
      assert result == :ok

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 1)
      StepFlow.HelpersTest.check(workflow.id, 0, 1)
      StepFlow.HelpersTest.complete_jobs(workflow.id, 0)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 1, 1)
      StepFlow.HelpersTest.check(workflow.id, 2, 1)

      StepFlow.HelpersTest.create_progression(workflow, 1, 0)

      StepFlow.HelpersTest.create_progression(workflow, 1, 75)

      {:ok, "still_processing"} = Step.start_next(workflow)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 1)

      StepFlow.HelpersTest.check(workflow.id, 1, 1)
      StepFlow.HelpersTest.check(workflow.id, 2, 1)

      # Force job status
      StepFlow.HelpersTest.change_job_status(workflow, 2, :retrying)
      StepFlow.HelpersTest.change_job_status(workflow, 2, :queued)
      StepFlow.HelpersTest.change_job_status(workflow, 2, :processing)

      StepFlow.HelpersTest.create_progression(workflow, 2, 25)

      StepFlow.HelpersTest.create_progression(workflow, 2, 75)

      StepFlow.HelpersTest.complete_jobs(workflow.id, 2)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 0, 1)
      StepFlow.HelpersTest.check(workflow.id, 1, 1)
      StepFlow.HelpersTest.check(workflow.id, 2, 1)
      StepFlow.HelpersTest.check(workflow.id, 3, 1)
      StepFlow.HelpersTest.check(workflow.id, 4)
    end
  end
end
