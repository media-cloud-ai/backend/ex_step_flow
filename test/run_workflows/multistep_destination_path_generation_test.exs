defmodule StepFlow.RunWorkflows.MultistepDestinationPathGenerationTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Step

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 3)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.9",
      identifier: "three_steps_destination_paths_generation",
      label: "Check destination filemane template",
      tags: ["test"],
      icon: "custom_icon",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some-identifier",
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["my_file.mov"]
            }
          ]
        },
        %{
          id: 1,
          name: "job_test",
          icon: "step_icon",
          parent_ids: [0],
          required_to_start: [0],
          label: "My second step",
          parameters: [
            %{
              id: "destination_filename",
              type: "template",
              value: "my_movie.wav"
            }
          ]
        },
        %{
          id: 2,
          name: "job_test",
          icon: "step_icon",
          parent_ids: [1],
          required_to_start: [1],
          label: "My second step",
          parameters: [
            %{
              id: "destination_path",
              type: "template",
              value: "{workflow_id}/{workflow_id}/my_movie_2.wav"
            }
          ]
        }
      ],
      parameters: []
    }

    test "run destination filename and destination path in template and check whether destination_path is properly generated" do
      workflow = StepFlow.HelpersTest.workflow_fixture(@workflow_definition)
      StepFlow.HelpersTest.admin_role_fixture()

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 1)
      StepFlow.HelpersTest.check(workflow.id, 0, 1)

      destination_path =
        StepFlow.Jobs.list_jobs(%{
          "step_id" => 0,
          "workflow_id" => workflow.id |> Integer.to_string(),
          "size" => 50,
          "roles" => ["administrator"]
        })
        |> Map.get(:data)
        |> List.first()
        |> Map.get(:parameters)
        |> Enum.filter(fn parameter -> Map.get(parameter, "id") == "destination_path" end)
        |> List.first()
        |> Map.get("value")

      assert destination_path ==
               "/test_work_dir/" <> Integer.to_string(workflow.id) <> "/my_file.mov"

      StepFlow.HelpersTest.complete_jobs(workflow.id, 0)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 2)
      StepFlow.HelpersTest.check(workflow.id, 1, 1)

      destination_path =
        StepFlow.Jobs.list_jobs(%{
          "step_id" => 1,
          "workflow_id" => workflow.id |> Integer.to_string(),
          "size" => 50,
          "roles" => ["administrator"]
        })
        |> Map.get(:data)
        |> List.first()
        |> Map.get(:parameters)
        |> Enum.filter(fn parameter -> Map.get(parameter, "id") == "destination_path" end)
        |> List.first()
        |> Map.get("value")

      assert destination_path ==
               "/test_work_dir/" <> Integer.to_string(workflow.id) <> "/my_movie.wav"

      StepFlow.HelpersTest.complete_jobs(workflow.id, 1)

      {:ok, "started"} = Step.start_next(workflow)

      StepFlow.HelpersTest.check(workflow.id, 3)
      StepFlow.HelpersTest.check(workflow.id, 2, 1)

      destination_path =
        StepFlow.Jobs.list_jobs(%{
          "step_id" => 2,
          "workflow_id" => workflow.id |> Integer.to_string(),
          "size" => 50,
          "roles" => ["administrator"]
        })
        |> Map.get(:data)
        |> List.first()
        |> Map.get(:parameters)
        |> Enum.filter(fn parameter -> Map.get(parameter, "id") == "destination_path" end)
        |> List.first()
        |> Map.get("value")

      assert destination_path ==
               Integer.to_string(workflow.id) <>
                 "/" <> Integer.to_string(workflow.id) <> "/my_movie_2.wav"

      StepFlow.HelpersTest.complete_jobs(workflow.id, 2)

      {:ok, "completed"} = Step.start_next(workflow)
    end
  end
end
