defmodule StepFlow.RunWorkflows.NotificationHooks.ErrorNotification do
  use ExUnit.Case
  use Plug.Test
  import FakeServer

  alias Ecto.Adapters.SQL.Sandbox
  alias FakeServer.Response
  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.NotificationHooks.NotificationEndpoints
  alias StepFlow.NotificationHooks.NotificationTemplates
  alias StepFlow.Step
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 2)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.11",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "One step workflow",
      tags: ["test"],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["coucou.mov"]
            }
          ]
        }
      ],
      parameters: []
    }

    @workflow_definition_allow_failure %{
      schema_version: "1.11",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "One step workflow",
      tags: ["test"],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          allow_failure: true,
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["coucou.mov"]
            }
          ]
        }
      ],
      parameters: []
    }
  end

  def workflow_fixture(attrs \\ %{}) do
    {:ok, workflow} =
      attrs
      |> Enum.into(@workflow_definition)
      |> Workflows.create_workflow()

    workflow
  end

  def workflow_allow_fixture(attrs \\ %{}) do
    {:ok, workflow} =
      attrs
      |> Enum.into(@workflow_definition_allow_failure)
      |> Workflows.create_workflow()

    workflow
  end

  @tag capture_log: true
  test_with_server "notifying workflow in error" do
    route("/notification/test_hook", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    route("/notification/test_hook2", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook",
      endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
    })

    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook2",
      endpoint_url: "http://#{FakeServer.address()}/notification/test_hook2"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template",
      template_body: "{
        \"test\": {}
      }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook",
            label: "label_test",
            conditions: %{
              "workflow_error" => "test_template"
            }
          },
          %{
            endpoint: "test_hook2",
            label: "label_test2",
            conditions: %{
              "job_error" => "test_template"
            }
          }
        ]
      )

    StepFlow.HelpersTest.admin_role_fixture()

    {:ok, "started"} = Step.start_next(workflow)

    job =
      StepFlow.HelpersTest.get_jobs(workflow.id, 0)
      |> List.first()

    result =
      CommonEmitter.publish_json(
        "job_error",
        0,
        %{
          job_id: job.id,
          parameters: [%{"id" => "message", "type" => "string", "value" => "Job has crashed"}],
          status: "error"
        },
        "job_response"
      )

    assert result == :ok

    :timer.sleep(1000)

    workflow = Workflows.get_workflow!(workflow.id)

    timestamp =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.add(-1)
      |> NaiveDateTime.to_string()
      |> NaiveDateTime.from_iso8601!()
      |> NaiveDateTime.truncate(:second)
      |> NaiveDateTime.to_iso8601()

    assert [
             %{
               "conditions" => %{
                 "workflow_error" => "test_template"
               },
               "endpoint" => "test_hook",
               "label" => "label_test",
               "status" => [
                 %{
                   "condition" => "workflow_error",
                   "response" => 200,
                   "timestamp" => timestamp
                 }
               ]
             },
             %{
               "conditions" => %{
                 "job_error" => "test_template"
               },
               "endpoint" => "test_hook2",
               "label" => "label_test2",
               "status" => [
                 %{
                   "condition" => "job_error_#{job.id}",
                   "response" => 200,
                   "timestamp" => timestamp
                 }
               ]
             }
           ] == workflow.notification_hooks
  end

  @tag capture_log: true
  test_with_server "notifying workflow in error with allow_failure" do
    route("/notification/test_hook", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    route("/notification/test_hook2", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook",
      endpoint_url: "http://#{FakeServer.address()}/notification/test_hook"
    })

    NotificationEndpoints.create_notification_endpoint(%{
      endpoint_placeholder: "test_hook2",
      endpoint_url: "http://#{FakeServer.address()}/notification/test_hook2"
    })

    NotificationTemplates.create_notification_template(%{
      template_name: "test_template",
      template_body: "{
        \"test\": {}
      }",
      template_headers: "[{\"content-type\" : \"application/json\"}]"
    })

    workflow =
      workflow_allow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook",
            label: "label_test",
            conditions: %{
              "workflow_completed" => "test_template",
              "workflow_error" => "test_template"
            }
          },
          %{
            endpoint: "test_hook2",
            label: "label_test2",
            conditions: %{
              "job_error" => "test_template"
            }
          }
        ]
      )

    StepFlow.HelpersTest.admin_role_fixture()

    {:ok, "started"} = Step.start_next(workflow)

    job =
      StepFlow.HelpersTest.get_jobs(workflow.id, 0)
      |> List.first()

    result =
      CommonEmitter.publish_json(
        "job_error",
        0,
        %{
          job_id: job.id,
          parameters: [%{"id" => "message", "type" => "string", "value" => "Job has crashed"}],
          status: "error"
        },
        "job_response"
      )

    assert result == :ok

    :timer.sleep(1000)

    workflow = Workflows.get_workflow!(workflow.id)

    timestamp =
      NaiveDateTime.utc_now()
      |> NaiveDateTime.add(-1)
      |> NaiveDateTime.to_string()
      |> NaiveDateTime.from_iso8601!()
      |> NaiveDateTime.truncate(:second)
      |> NaiveDateTime.to_iso8601()

    assert [
             %{
               "conditions" => %{
                 "workflow_completed" => "test_template",
                 "workflow_error" => "test_template"
               },
               "endpoint" => "test_hook",
               "label" => "label_test",
               "status" => [
                 %{
                   "condition" => "workflow_completed",
                   "response" => 200,
                   "timestamp" => timestamp
                 }
               ]
             },
             %{
               "conditions" => %{
                 "job_error" => "test_template"
               },
               "endpoint" => "test_hook2",
               "label" => "label_test2",
               "status" => [
                 %{
                   "condition" => "job_error_#{job.id}",
                   "response" => 200,
                   "timestamp" => timestamp
                 }
               ]
             }
           ] == workflow.notification_hooks
  end
end
