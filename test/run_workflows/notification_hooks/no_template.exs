defmodule StepFlow.RunWorkflows.NotificationHooks.NoTemplate do
  use ExUnit.Case
  use Plug.Test
  import FakeServer

  alias Ecto.Adapters.SQL.Sandbox
  alias FakeServer.Response
  alias StepFlow.Amqp.CommonEmitter
  alias StepFlow.Step
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
    {_conn, channel} = StepFlow.HelpersTest.get_amqp_connection()

    on_exit(fn ->
      StepFlow.HelpersTest.consume_messages(channel, "job_test", 1)
    end)

    :ok
  end

  describe "workflows" do
    @workflow_definition %{
      schema_version: "1.11",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      icon: "custom_icon",
      label: "One step workflow",
      tags: ["test"],
      steps: [
        %{
          id: 0,
          name: "job_test",
          icon: "step_icon",
          label: "My first step",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["coucou.mov"]
            }
          ]
        }
      ],
      parameters: []
    }
  end

  def workflow_fixture(attrs \\ %{}) do
    {:ok, workflow} =
      attrs
      |> Enum.into(@workflow_definition)
      |> Workflows.create_workflow()

    workflow
  end

  @tag capture_log: true
  test_with_server "notifying workflow in error" do
    route("/notification/test_hook", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    route("/notification/test_hook2", fn query ->
      if Map.get(query.headers, "content-type") == "application/json" do
        Response.ok!(~s({"status": "ok"}))
      else
        Response.no_content!()
      end
    end)

    workflow =
      workflow_fixture(
        notification_hooks: [
          %{
            endpoint: "test_hook",
            label: "label_test",
            conditions: %{
              "workflow_error" => "test_template"
            }
          },
          %{
            endpoint: "test_hook2",
            label: "label_test2",
            conditions: %{
              "job_error" => "test_template"
            }
          }
        ]
      )

    StepFlow.HelpersTest.admin_role_fixture()

    {:ok, "started"} = Step.start_next(workflow)

    job =
      StepFlow.HelpersTest.get_jobs(workflow.id, 0)
      |> List.first()

    result =
      CommonEmitter.publish_json(
        "job_error",
        0,
        %{
          job_id: job.id,
          parameters: [%{"id" => "message", "type" => "string", "value" => "Job has crashed"}],
          status: "error"
        },
        "job_response"
      )

    assert result == :ok

    :timer.sleep(1000)

    workflow = Workflows.get_workflow!(workflow.id)

    assert [
             %{
               "conditions" => %{
                 "workflow_error" => "test_template"
               },
               "endpoint" => "test_hook",
               "label" => "label_test"
             },
             %{
               "conditions" => %{
                 "job_error" => "test_template"
               },
               "endpoint" => "test_hook2",
               "label" => "label_test2"
             }
           ] == workflow.notification_hooks
  end
end
