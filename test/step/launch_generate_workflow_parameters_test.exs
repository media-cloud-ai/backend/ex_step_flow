defmodule StepFlow.Step.LaunchGenerateWorkflowParametersTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Repo
  alias StepFlow.Step.Helpers
  alias StepFlow.Step.Launch
  alias StepFlow.Step.LaunchWorkflows
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  describe "launch_generate_workflow_parameters_test" do
    @workflow_definition_with_child_workflow %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      parameters: [
        %{
          id: "source_file",
          type: "string",
          value: "my_file_1.mov"
        },
        %{
          id: "other_input",
          type: "string",
          value: "other.txt"
        }
      ],
      steps: [
        %{
          id: 0,
          name: "job_step",
          mode: "workflow_one_for_one",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_templates",
              value: [
                "{source_file}"
              ]
            },
            %{
              id: "identifier",
              type: "string",
              value: "one_step_workflow"
            },
            %{
              id: "version_major",
              type: "integer",
              value: 6
            },
            %{
              id: "version_minor",
              type: "integer",
              value: 5
            },
            %{
              id: "version_micro",
              type: "integer",
              value: 4
            },
            %{
              id: "parameters",
              type: "extended",
              value: [
                %{
                  id: "mov_input_path",
                  type: "template",
                  value: "{source_path}"
                },
                %{
                  id: "source_file",
                  type: "template",
                  value: "{source_file}"
                },
                %{
                  id: "other_input",
                  type: "template",
                  value: "{other_input}"
                }
              ]
            }
          ]
        }
      ]
    }

    @workflow_definition_with_child_workflows_one_for_one %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      parameters: [
        %{
          id: "other_input",
          type: "string",
          value: "other.txt"
        }
      ],
      steps: [
        %{
          id: 0,
          name: "job_step",
          mode: "workflow_one_for_one",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: [
                "my_file_1.mov",
                "my_file_2.wav"
              ]
            },
            %{
              id: "identifier",
              type: "string",
              value: "one_step_workflow"
            },
            %{
              id: "version_major",
              type: "integer",
              value: 6
            },
            %{
              id: "version_minor",
              type: "integer",
              value: 5
            },
            %{
              id: "version_micro",
              type: "integer",
              value: 4
            },
            %{
              id: "parameters",
              type: "extended",
              value: [
                %{
                  id: "source_file",
                  type: "template",
                  value: "{source_path}"
                },
                %{
                  id: "other_input",
                  type: "template",
                  value: "{other_input}"
                }
              ]
            }
          ]
        }
      ]
    }

    @workflow_definition_with_child_workflow_one_for_many %{
      schema_version: "1.9",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      parameters: [
        %{
          id: "mov_input_path",
          type: "string",
          value: "my_file_1.mov"
        },
        %{
          id: "wav_input_path",
          type: "string",
          value: "my_file_2.wav"
        },
        %{
          id: "other_input",
          type: "string",
          value: "other.txt"
        }
      ],
      steps: [
        %{
          id: 0,
          name: "job_step",
          mode: "workflow_one_for_many",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: [
                "my_file_1.mov",
                "my_file_2.wav"
              ]
            },
            %{
              id: "identifier",
              type: "string",
              value: "one_step_workflow"
            },
            %{
              id: "version_major",
              type: "integer",
              value: 6
            },
            %{
              id: "version_minor",
              type: "integer",
              value: 5
            },
            %{
              id: "version_micro",
              type: "integer",
              value: 4
            },
            %{
              id: "parameters",
              type: "extended",
              value: [
                %{
                  id: "mov_input_path",
                  type: "template",
                  value: "{mov_input_path}"
                },
                %{
                  id: "wav_input_path",
                  type: "template",
                  value: "{wav_input_path}"
                },
                %{
                  id: "other_input",
                  type: "template",
                  value: "{other_input}"
                }
              ]
            }
          ]
        }
      ]
    }

    def workflow_fixture(workflow, attrs \\ %{}) do
      {:ok, workflow} =
        attrs
        |> Enum.into(workflow)
        |> Workflows.create_workflow()

      workflow
    end

    test "generate workflow parameters one for one from step" do
      workflow =
        workflow_fixture(@workflow_definition_with_child_workflow)
        |> Repo.preload([:artifacts, :jobs])

      step =
        @workflow_definition_with_child_workflow.steps
        |> List.first()

      dates = Helpers.get_dates()

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == ["my_file_1.mov"]

      {:ok, job} = LaunchWorkflows.generate_child_workflow_job(workflow, step)

      message =
        LaunchWorkflows.generate_child_workflow_params(
          workflow,
          step,
          job,
          dates,
          "my_file_1.mov"
        )

      assert message.parent_id == job.id
      assert message.reference == "[AUTOGENERATED] Child from workflow #{workflow.id}"

      assert message.parameters == [
               %{id: "mov_input_path", type: "string", value: "my_file_1.mov"},
               %{id: "source_file", type: "string", value: "my_file_1.mov"},
               %{id: "other_input", type: "string", value: "other.txt"}
             ]
    end

    test "generate workflows parameters one for one from step" do
      workflow =
        workflow_fixture(@workflow_definition_with_child_workflows_one_for_one)
        |> Repo.preload([:artifacts, :jobs])

      step =
        @workflow_definition_with_child_workflows_one_for_one.steps
        |> List.first()

      dates = Helpers.get_dates()

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == ["my_file_1.mov", "my_file_2.wav"]

      {:ok, job_1} = LaunchWorkflows.generate_child_workflow_job(workflow, step)

      message_1 =
        LaunchWorkflows.generate_child_workflow_params(
          workflow,
          step,
          job_1,
          dates,
          "my_file_1.mov"
        )

      assert message_1.parent_id == job_1.id
      assert message_1.reference == "[AUTOGENERATED] Child from workflow #{workflow.id}"

      assert message_1.parameters == [
               %{id: "source_file", type: "string", value: "my_file_1.mov"},
               %{id: "other_input", type: "string", value: "other.txt"}
             ]

      {:ok, job_2} = LaunchWorkflows.generate_child_workflow_job(workflow, step)

      message_2 =
        LaunchWorkflows.generate_child_workflow_params(
          workflow,
          step,
          job_2,
          dates,
          "my_file_2.wav"
        )

      assert message_2.parent_id == job_2.id
      assert message_2.reference == "[AUTOGENERATED] Child from workflow #{workflow.id}"

      assert message_2.parameters == [
               %{id: "source_file", type: "string", value: "my_file_2.wav"},
               %{id: "other_input", type: "string", value: "other.txt"}
             ]
    end

    test "generate workflow parameters one for many from step" do
      workflow =
        workflow_fixture(@workflow_definition_with_child_workflow_one_for_many)
        |> Repo.preload([:artifacts, :jobs])

      step =
        @workflow_definition_with_child_workflow_one_for_many.steps
        |> List.first()

      dates = Helpers.get_dates()

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == ["my_file_1.mov", "my_file_2.wav"]

      {:ok, job} = LaunchWorkflows.generate_child_workflow_job(workflow, step)

      message =
        LaunchWorkflows.generate_child_workflow_params(workflow, step, job, dates, source_paths)

      assert message.parent_id == job.id
      assert message.reference == "[AUTOGENERATED] Child from workflow #{workflow.id}"

      assert message.parameters == [
               %{id: "mov_input_path", type: "string", value: "my_file_1.mov"},
               %{id: "wav_input_path", type: "string", value: "my_file_2.wav"},
               %{id: "other_input", type: "string", value: "other.txt"}
             ]
    end
  end
end
