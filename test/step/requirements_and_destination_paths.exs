defmodule StepFlow.Step.RequirementsAndDestinationPathsTest do
  use ExUnit.Case
  use Plug.Test

  alias StepFlow.Step.Helpers
  alias StepFlow.Step.Launch

  doctest StepFlow

  describe "simple requirements and destination paths" do
    test "build with first file same as source path" do
      base_directory = "/path/to/directory/"
      source_path = "/input/source_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          [],
          [],
          nil,
          nil,
          nil,
          base_directory,
          source_path,
          source_path
        )

      assert required_paths == []
      assert destination_path == "/path/to/directory/source_file.ext"
    end

    test "build with first file different from source path" do
      base_directory = "/path/to/directory/"
      source_path = "/input/source_file.ext"
      first_file = "/input/first_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          [],
          [],
          nil,
          nil,
          nil,
          base_directory,
          source_path,
          first_file
        )

      assert required_paths == "/path/to/directory/first_file.ext"
      assert destination_path == "/path/to/directory/source_file.ext"
    end

    test "build with first file different from source path and in a work dir subdirectory" do
      base_directory = "/path/to/directory/"
      source_path = "/input/source_file.ext"
      first_file = "/path/to/directory/subdir/first_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          [],
          [],
          nil,
          nil,
          nil,
          base_directory,
          source_path,
          first_file
        )

      assert required_paths == "/path/to/directory/subdir/first_file.ext"
      assert destination_path == "/path/to/directory/source_file.ext"
    end
  end

  describe "requirements and destination paths with destination path templates" do
    test "build with first file same as source path" do
      destination_path_templates = ["{work_directory}/output_file.ext"]

      workflow = %{
        id: 123,
        reference: "123 this is a workflow!",
        parameters: []
      }

      step = %{
        name: "job_step"
      }

      dates = Helpers.get_dates()

      base_directory = Helpers.get_base_directory(workflow, step)
      assert base_directory == "/test_work_dir/123/"

      source_path = "/input/source_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          destination_path_templates,
          [],
          workflow,
          step,
          dates,
          base_directory,
          source_path,
          source_path
        )

      assert required_paths == []
      assert destination_path == "/test_work_dir/output_file.ext"
    end

    test "build with first file different from source path" do
      destination_path_templates = ["{work_directory}/output_file.ext"]

      workflow = %{
        id: 123,
        reference: "123 this is a workflow!",
        parameters: []
      }

      step = %{
        name: "job_step"
      }

      dates = Helpers.get_dates()

      base_directory = Helpers.get_base_directory(workflow, step)
      assert base_directory == "/test_work_dir/123/"

      source_path = "/input/source_file.ext"
      first_file = "/input/first_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          destination_path_templates,
          [],
          workflow,
          step,
          dates,
          base_directory,
          source_path,
          first_file
        )

      assert required_paths == "/test_work_dir/123/first_file.ext"
      assert destination_path == "/test_work_dir/output_file.ext"
    end

    test "build with first file different from source path and in a work dir subdirectory" do
      destination_path_templates = ["{work_directory}/output_file.ext"]

      workflow = %{
        id: 123,
        reference: "123 this is a workflow!",
        parameters: []
      }

      step = %{
        name: "job_step"
      }

      dates = Helpers.get_dates()

      base_directory = Helpers.get_base_directory(workflow, step)
      assert base_directory == "/test_work_dir/123/"

      source_path = "/input/source_file.ext"
      first_file = "/test_work_dir/123/subdir/first_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          destination_path_templates,
          [],
          workflow,
          step,
          dates,
          base_directory,
          source_path,
          first_file
        )

      assert required_paths == "/test_work_dir/123/subdir/first_file.ext"
      assert destination_path == "/test_work_dir/output_file.ext"
    end
  end

  describe "requirements and destination paths with destination filename templates" do
    test "build with first file same as source path" do
      destination_filename_templates = ["output_file-{workflow_id}.ext"]

      workflow = %{
        id: 123,
        reference: "123 this is a workflow!",
        parameters: []
      }

      step = %{
        name: "job_step"
      }

      dates = Helpers.get_dates()

      base_directory = Helpers.get_base_directory(workflow, step)
      assert base_directory == "/test_work_dir/123/"

      source_path = "/input/source_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          [],
          destination_filename_templates,
          workflow,
          step,
          dates,
          base_directory,
          source_path,
          source_path
        )

      assert required_paths == []
      assert destination_path == "/test_work_dir/123/output_file-123.ext"
    end

    test "build with first file different from source path" do
      destination_filename_templates = ["output_file-{workflow_id}.ext"]

      workflow = %{
        id: 123,
        reference: "123 this is a workflow!",
        parameters: []
      }

      step = %{
        name: "job_step"
      }

      dates = Helpers.get_dates()

      base_directory = Helpers.get_base_directory(workflow, step)
      assert base_directory == "/test_work_dir/123/"

      source_path = "/input/source_file.ext"
      first_file = "/input/first_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          [],
          destination_filename_templates,
          workflow,
          step,
          dates,
          base_directory,
          source_path,
          first_file
        )

      assert required_paths == "/test_work_dir/123/first_file.ext"
      assert destination_path == "/test_work_dir/123/output_file-123.ext"
    end

    test "build with first file different from source path and in a work dir subdirectory" do
      destination_filename_templates = ["output_file-{workflow_id}.ext"]

      workflow = %{
        id: 123,
        reference: "123 this is a workflow!",
        parameters: []
      }

      step = %{
        name: "job_step"
      }

      dates = Helpers.get_dates()

      base_directory = Helpers.get_base_directory(workflow, step)
      assert base_directory == "/test_work_dir/123/"

      source_path = "/input/source_file.ext"
      first_file = "/test_work_dir/123/subdir/first_file.ext"

      {required_paths, destination_path} =
        Launch.build_requirements_and_destination_path(
          [],
          destination_filename_templates,
          workflow,
          step,
          dates,
          base_directory,
          source_path,
          first_file
        )

      assert required_paths == "/test_work_dir/123/subdir/first_file.ext"
      assert destination_path == "/test_work_dir/123/output_file-123.ext"
    end
  end

  describe "get requirement paths" do
    test "get required paths" do
      assert [] ==
               Launch.get_required_paths(
                 "/input/source_path.ext",
                 "/input/source_path.ext",
                 "/base_directory/"
               )

      assert "/base_directory/first_file.ext" ==
               Launch.get_required_paths(
                 "/input/source_path.ext",
                 "/input/first_file.ext",
                 "/base_directory/"
               )

      assert "/base_directory/first_file.ext" ==
               Launch.get_required_paths(
                 "/input/source_path.ext",
                 "/input/subdir/first_file.ext",
                 "/base_directory/"
               )

      assert "/base_directory/subdir/first_file.ext" ==
               Launch.get_required_paths(
                 "/input/source_path.ext",
                 "/base_directory/subdir/first_file.ext",
                 "/base_directory/"
               )
    end
  end
end
