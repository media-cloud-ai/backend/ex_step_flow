defmodule StepFlow.Step.StepGetJobsDestinationPathsTest do
  use ExUnit.Case
  use Plug.Test

  alias StepFlow.Step.Helpers

  #  doctest StepFlow

  describe "helpers_get_jobs_destination_paths_test" do
    test "get jobs destination paths" do
      jobs = [
        %{
          name: "job_test_1",
          step_id: 0,
          workflow_id: 0,
          parameters: [
            %{
              id: "destination_path",
              type: "string",
              value: "/path/to/destination_1"
            }
          ]
        },
        %{
          name: "job_test_2",
          step_id: 0,
          workflow_id: 0,
          parameters: [
            %{
              id: "destination_paths",
              type: "array_of_strings",
              value: [
                "/path/to/destination_1",
                "/path/to/destination_2",
                "/path/to/destination_3"
              ]
            }
          ]
        },
        %{
          name: "job_test_3",
          step_id: 0,
          workflow_id: 0,
          parameters: []
        }
      ]

      destination_paths = Helpers.get_jobs_destination_paths(jobs)

      assert destination_paths == [
               "/path/to/destination_1",
               "/path/to/destination_2",
               "/path/to/destination_3"
             ]
    end
  end
end
