defmodule StepFlow.Step.LaunchGetSourcePathsTest do
  use ExUnit.Case
  use Plug.Test

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Repo
  alias StepFlow.Step.Helpers
  alias StepFlow.Step.Launch
  alias StepFlow.Step.LaunchJobs
  alias StepFlow.Step.LaunchParams
  alias StepFlow.Workflows

  doctest StepFlow

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  describe "launch_get_source_paths_test" do
    @workflow_definition %{
      schema_version: "1.13",
      identifier: "id",
      version_major: 6,
      version_minor: 5,
      version_micro: 4,
      reference: "some id",
      steps: [
        %{
          id: 0,
          name: "step_1",
          parameters: [
            %{
              id: "source_paths",
              type: "array_of_strings",
              value: ["/path/to/source_path_1"]
            },
            %{
              id: "destination_paths",
              type: "string",
              value: "/path/to/destination_path_1"
            }
          ]
        },
        %{
          id: 1,
          name: "step_2",
          parent_ids: [0],
          parameters: [
            %{
              id: "destination_paths",
              type: "array_of_strings",
              value: [
                "/path/to/destination_path_2.1",
                "/path/to/destination_path_2.2",
                "/path/to/destination_path_2.3",
                "/path/to/destination_path_2.4",
                "/path/to/destination_path_2.5",
                "/path/to/destination_path_2.6",
                "/path/to/destination_path_2.7",
                "/path/to/destination_path_2.8",
                "/path/to/destination_path_2.9",
                "/path/to/destination_path_2.10",
                "/path/to/destination_path_2.11",
                "/path/to/destination_path_2.12",
                "/path/to/destination_path_2.13",
                "/path/to/destination_path_2.14",
                "/path/to/destination_path_2.15"
              ]
            }
          ]
        },
        %{
          id: 2,
          name: "step_3",
          parent_ids: [1],
          mode: "one_for_many",
          parameters: [
            %{
              id: "destination_paths",
              type: "array_of_strings",
              value: [
                "/path/to/destination_path_3"
              ]
            }
          ]
        },
        %{
          id: 3,
          name: "step_4",
          parent_ids: [2],
          parameters: [
            %{
              id: "destination_paths",
              type: "array_of_strings",
              value: [
                "/path/to/destination_path_4"
              ]
            }
          ]
        }
      ]
    }

    def workflow_fixture(workflow, attrs \\ %{}) do
      {:ok, workflow} =
        attrs
        |> Enum.into(workflow)
        |> Workflows.create_workflow()

      workflow
    end

    test "get step source paths" do
      workflow =
        workflow_fixture(@workflow_definition)
        |> Repo.preload([:artifacts, :jobs])

      dates = Helpers.get_dates()

      #
      # STEP 1
      #

      # One for one mode
      step =
        @workflow_definition.steps
        |> Enum.at(0)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == ["/path/to/source_path_1"]

      first_file =
        source_paths
        |> Enum.sort()
        |> List.first()

      launch_params = LaunchParams.new(workflow, step, dates, first_file)

      {:ok, job} = LaunchJobs.generate_job_one_for_one(first_file, launch_params)

      destination_paths = Helpers.get_jobs_destination_paths([job])

      assert destination_paths == [
               "/test_work_dir/" <> Integer.to_string(workflow.id) <> "/source_path_1",
               "/path/to/destination_path_1"
             ]

      #
      # STEP 2
      #

      # Reload workflow
      workflow = Workflows.get_workflow!(workflow.id)

      # One for one mode
      step =
        @workflow_definition.steps
        |> Enum.at(1)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == destination_paths

      first_file =
        source_paths
        |> Enum.sort()
        |> List.first()

      launch_params = LaunchParams.new(workflow, step, dates, first_file)

      jobs =
        source_paths
        |> Enum.map(fn source_path ->
          {:ok, job} = LaunchJobs.generate_job_one_for_one(source_path, launch_params)
          job
        end)

      destination_paths = Helpers.get_jobs_destination_paths(jobs)

      assert destination_paths == [
               "/test_work_dir/" <> Integer.to_string(workflow.id) <> "/source_path_1",
               "/path/to/destination_path_2.1",
               "/path/to/destination_path_2.2",
               "/path/to/destination_path_2.3",
               "/path/to/destination_path_2.4",
               "/path/to/destination_path_2.5",
               "/path/to/destination_path_2.6",
               "/path/to/destination_path_2.7",
               "/path/to/destination_path_2.8",
               "/path/to/destination_path_2.9",
               "/path/to/destination_path_2.10",
               "/path/to/destination_path_2.11",
               "/path/to/destination_path_2.12",
               "/path/to/destination_path_2.13",
               "/path/to/destination_path_2.14",
               "/path/to/destination_path_2.15",
               "/test_work_dir/" <> Integer.to_string(workflow.id) <> "/destination_path_1"
             ]

      #
      # STEP 3
      #

      # Reload workflow
      workflow = Workflows.get_workflow!(workflow.id)

      # One for many mode
      step =
        @workflow_definition.steps
        |> Enum.at(2)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == destination_paths

      launch_params = LaunchParams.new(workflow, step, dates)

      {:ok, job} = LaunchJobs.generate_job_one_for_many(source_paths, launch_params)

      destination_paths = Helpers.get_jobs_destination_paths([job])

      assert destination_paths == [
               "/path/to/destination_path_3"
             ]

      #
      # STEP 4
      #

      # Reload workflow
      workflow = Workflows.get_workflow!(workflow.id)

      # One for one mode
      step =
        @workflow_definition.steps
        |> Enum.at(3)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == destination_paths

      first_file =
        source_paths
        |> Enum.sort()
        |> List.first()

      launch_params = LaunchParams.new(workflow, step, dates, first_file)

      jobs =
        source_paths
        |> Enum.map(fn source_path ->
          {:ok, job} = LaunchJobs.generate_job_one_for_one(source_path, launch_params)
          job
        end)

      destination_paths = Helpers.get_jobs_destination_paths(jobs)

      assert destination_paths == [
               "/test_work_dir/" <> Integer.to_string(workflow.id) <> "/destination_path_3",
               "/path/to/destination_path_4"
             ]
    end

    test "get step source paths skipping destination paths" do
      workflow =
        workflow_fixture(@workflow_definition)
        |> Repo.preload([:artifacts, :jobs])

      dates = Helpers.get_dates()

      #
      # STEP 1
      #

      # One for one mode
      step =
        @workflow_definition.steps
        |> Enum.at(0)
        |> Map.put(:skip_destination_path, true)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == ["/path/to/source_path_1"]

      first_file =
        source_paths
        |> Enum.sort()
        |> List.first()

      launch_params = LaunchParams.new(workflow, step, dates, first_file)

      {:ok, job} = LaunchJobs.generate_job_one_for_one(first_file, launch_params)

      destination_paths = Helpers.get_jobs_destination_paths([job])

      assert destination_paths == [
               "/path/to/destination_path_1"
             ]

      #
      # STEP 2
      #

      # Reload workflow
      workflow = Workflows.get_workflow!(workflow.id)

      # One for one mode
      step =
        @workflow_definition.steps
        |> Enum.at(1)
        |> Map.put(:skip_destination_path, true)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == destination_paths

      first_file =
        source_paths
        |> Enum.sort()
        |> List.first()

      launch_params = LaunchParams.new(workflow, step, dates, first_file)

      jobs =
        source_paths
        |> Enum.map(fn source_path ->
          {:ok, job} = LaunchJobs.generate_job_one_for_one(source_path, launch_params)
          job
        end)

      destination_paths = Helpers.get_jobs_destination_paths(jobs)

      assert destination_paths == [
               "/path/to/destination_path_2.1",
               "/path/to/destination_path_2.2",
               "/path/to/destination_path_2.3",
               "/path/to/destination_path_2.4",
               "/path/to/destination_path_2.5",
               "/path/to/destination_path_2.6",
               "/path/to/destination_path_2.7",
               "/path/to/destination_path_2.8",
               "/path/to/destination_path_2.9",
               "/path/to/destination_path_2.10",
               "/path/to/destination_path_2.11",
               "/path/to/destination_path_2.12",
               "/path/to/destination_path_2.13",
               "/path/to/destination_path_2.14",
               "/path/to/destination_path_2.15"
             ]

      #
      # STEP 3
      #

      # Reload workflow
      workflow = Workflows.get_workflow!(workflow.id)

      # One for many mode
      step =
        @workflow_definition.steps
        |> Enum.at(2)
        |> Map.put(:skip_destination_path, true)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == destination_paths

      launch_params = LaunchParams.new(workflow, step, dates)

      {:ok, job} = LaunchJobs.generate_job_one_for_many(source_paths, launch_params)

      destination_paths = Helpers.get_jobs_destination_paths([job])

      assert destination_paths == [
               "/path/to/destination_path_3"
             ]

      #
      # STEP 4
      #

      # Reload workflow
      workflow = Workflows.get_workflow!(workflow.id)

      # One for one mode
      step =
        @workflow_definition.steps
        |> Enum.at(3)
        |> Map.put(:skip_destination_path, true)

      source_paths = Launch.get_source_paths(workflow, step, dates)

      assert source_paths == destination_paths

      first_file =
        source_paths
        |> Enum.sort()
        |> List.first()

      launch_params = LaunchParams.new(workflow, step, dates, first_file)

      jobs =
        source_paths
        |> Enum.map(fn source_path ->
          {:ok, job} = LaunchJobs.generate_job_one_for_one(source_path, launch_params)
          job
        end)

      destination_paths = Helpers.get_jobs_destination_paths(jobs)

      assert destination_paths == [
               "/path/to/destination_path_4"
             ]
    end
  end
end
