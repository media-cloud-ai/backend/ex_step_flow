defmodule StepFlow.Statistics.DurationsTest do
  use ExUnit.Case
  use Plug.Test

  import Ecto.Query, warn: false
  alias StepFlow.Repo

  alias Ecto.Adapters.SQL.Sandbox
  alias StepFlow.Controllers.Statistics.Durations
  alias StepFlow.Jobs
  alias StepFlow.Jobs.Job
  alias StepFlow.Jobs.Status
  alias StepFlow.Progressions
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Statistics.JobsDurations
  alias StepFlow.Workflows

  setup do
    # Explicitly get a connection before each test
    :ok = Sandbox.checkout(StepFlow.Repo)
    # Setting the shared mode
    Sandbox.mode(StepFlow.Repo, {:shared, self()})
  end

  @workflow %{
    schema_version: "1.9",
    identifier: "id",
    version_major: 6,
    version_minor: 5,
    version_micro: 4,
    reference: "some id",
    user_uuid: "super_toto",
    steps: []
  }

  @administrator %{
    name: "administrator",
    rights: [%Right{entity: "*", action: ["*"]}]
  }

  defp role_fixture(role) do
    {:ok, role} = Roles.create_role(role)

    role
  end

  test "list job durations" do
    role_fixture(@administrator)

    populate_with_jobs_with_durations()
    populate_with_jobs_with_durations()

    # no filter
    durations = Durations.list_durations_for_jobs(%{"roles" => ["administrator"]})

    # cannot check data, since the order is not ensured
    assert durations.page == 0
    assert durations.size == 10
    assert durations.total == 16
  end

  test "list job durations per workflow" do
    role_fixture(@administrator)

    _workflow_1_jobs = populate_with_jobs_with_durations()
    workflow_2_jobs = populate_with_jobs_with_durations()

    workflow_id = workflow_2_jobs.not_started_job.workflow_id

    durations =
      Durations.list_durations_for_jobs(%{
        "workflow_id" => workflow_id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: workflow_2_jobs.not_started_job.id,
                 workflow_id: workflow_2_jobs.not_started_job.workflow_id,
                 order_pending: 0,
                 processing: 0,
                 response_pending: 0,
                 total: 0
               },
               %{
                 job_id: workflow_2_jobs.just_started_job.id,
                 workflow_id: workflow_2_jobs.just_started_job.workflow_id,
                 order_pending: 123,
                 processing: 0,
                 response_pending: 0,
                 total: 123
               },
               %{
                 job_id: workflow_2_jobs.processing_job.id,
                 workflow_id: workflow_2_jobs.processing_job.workflow_id,
                 order_pending: 123,
                 processing: 60,
                 response_pending: 0,
                 total: 183
               },
               %{
                 job_id: workflow_2_jobs.completed_job.id,
                 workflow_id: workflow_2_jobs.completed_job.workflow_id,
                 order_pending: 123,
                 processing: 90,
                 response_pending: 6,
                 total: 219
               },
               %{
                 job_id: workflow_2_jobs.completed_job_with_execution_duration.id,
                 workflow_id: workflow_2_jobs.completed_job_with_execution_duration.workflow_id,
                 order_pending: 123,
                 processing: 89.765,
                 response_pending: 7,
                 total: 219.765
               },
               %{
                 job_id: workflow_2_jobs.failed_job.id,
                 workflow_id: workflow_2_jobs.failed_job.workflow_id,
                 order_pending: 123,
                 processing: 76,
                 response_pending: 8,
                 total: 207
               },
               %{
                 job_id: workflow_2_jobs.expired_job.id,
                 workflow_id: workflow_2_jobs.expired_job.workflow_id,
                 order_pending: 89,
                 processing: 0,
                 response_pending: 0,
                 total: 89
               },
               %{
                 job_id: workflow_2_jobs.skipped_job.id,
                 workflow_id: workflow_2_jobs.skipped_job.workflow_id,
                 order_pending: 0,
                 processing: 0,
                 response_pending: 0,
                 total: 0
               }
             ],
             page: 0,
             size: 10,
             total: 8
           }
  end

  test "list job durations per job" do
    role_fixture(@administrator)

    jobs = populate_with_jobs_with_durations()

    # Job that has not started
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.not_started_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.not_started_job.id,
                 workflow_id: jobs.not_started_job.workflow_id,
                 order_pending: 0,
                 processing: 0,
                 response_pending: 0,
                 total: 0
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job that has just started
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.just_started_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.just_started_job.id,
                 workflow_id: jobs.just_started_job.workflow_id,
                 order_pending: 123,
                 processing: 0,
                 response_pending: 0,
                 total: 123
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job that is processing
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.processing_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.processing_job.id,
                 workflow_id: jobs.processing_job.workflow_id,
                 order_pending: 123,
                 processing: 60,
                 response_pending: 0,
                 total: 183
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job that has completed
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.completed_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.completed_job.id,
                 workflow_id: jobs.completed_job.workflow_id,
                 order_pending: 123,
                 processing: 90,
                 response_pending: 6,
                 total: 219
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job that has completed with the execution duration set into status descriptions
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.completed_job_with_execution_duration.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.completed_job_with_execution_duration.id,
                 workflow_id: jobs.completed_job_with_execution_duration.workflow_id,
                 order_pending: 123,
                 processing: 89.765,
                 response_pending: 7,
                 total: 219.765
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job that has failed
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.failed_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.failed_job.id,
                 workflow_id: jobs.failed_job.workflow_id,
                 order_pending: 123,
                 processing: 76,
                 response_pending: 8,
                 total: 207
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job which TTL has expired
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.expired_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.expired_job.id,
                 workflow_id: jobs.expired_job.workflow_id,
                 order_pending: 89,
                 processing: 0,
                 response_pending: 0,
                 total: 89
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }

    # Job that is queued or was skipped
    durations =
      Durations.list_durations_for_jobs(%{
        "job_id" => jobs.skipped_job.id,
        "roles" => ["administrator"]
      })

    assert durations == %{
             data: [
               %{
                 job_id: jobs.skipped_job.id,
                 workflow_id: jobs.skipped_job.workflow_id,
                 order_pending: 0,
                 processing: 0,
                 response_pending: 0,
                 total: 0
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }
  end

  test "list workflows durations" do
    role_fixture(@administrator)

    workflow_1_jobs = populate_with_jobs_with_durations()
    workflow_2_jobs = populate_with_jobs_with_durations()

    # no filter
    durations = Durations.list_durations_for_workflows(%{roles: ["administrator"]})

    assert durations == %{
             data: [
               %{
                 workflow_id: workflow_1_jobs.not_started_job.workflow_id,
                 order_pending: 704,
                 processing: 315.765,
                 response_pending: 21,
                 total: 419
               },
               %{
                 workflow_id: workflow_2_jobs.not_started_job.workflow_id,
                 order_pending: 704,
                 processing: 315.765,
                 response_pending: 21,
                 total: 419
               }
             ],
             page: 0,
             size: 10,
             total: 2
           }
  end

  test "list workflows durations per workflow" do
    role_fixture(@administrator)

    _workflow_1_jobs = populate_with_jobs_with_durations()
    workflow_2_jobs = populate_with_jobs_with_durations()
    _workflow_3_jobs = populate_with_jobs_with_durations()

    durations =
      Durations.list_durations_for_workflows(%{
        "workflow_id" => workflow_2_jobs.not_started_job.workflow_id
      })

    assert durations == %{
             data: [
               %{
                 workflow_id: workflow_2_jobs.not_started_job.workflow_id,
                 order_pending: 704,
                 processing: 315.765,
                 response_pending: 21,
                 total: 419
               }
             ],
             page: 0,
             size: 1,
             total: 1
           }
  end

  test "get jobs duration statistics empty" do
    role_fixture(@administrator)

    statistics = Durations.get_jobs_duration_statistics(%{"roles" => ["administrator"]})

    assert statistics == %{
             data: [],
             page: 0,
             size: 10,
             total: 0
           }
  end

  test "get jobs duration statistics" do
    role_fixture(@administrator)

    _jobs = populate_with_jobs_with_durations()

    statistics = Durations.get_jobs_duration_statistics(%{"roles" => ["administrator"]})

    # The order of jobs is not deterministic, therefore we need to sort or use a set
    assert MapSet.new(statistics.data) ==
             MapSet.new([
               %{
                 durations: %{
                   average: %{
                     order_pending: 123.0,
                     processing: 60.0,
                     response_pending: 0.0,
                     total: 183.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 123.0,
                     processing: 60.0,
                     response_pending: 0.0,
                     total: 183.0
                   },
                   min: %{
                     order_pending: 123.0,
                     processing: 60.0,
                     response_pending: 0.0,
                     total: 183.0
                   }
                 },
                 name: "processing_job"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 123.0,
                     processing: 89.765,
                     response_pending: 7.0,
                     total: 219.765
                   },
                   count: 1,
                   max: %{
                     order_pending: 123.0,
                     processing: 89.765,
                     response_pending: 7.0,
                     total: 219.765
                   },
                   min: %{
                     order_pending: 123.0,
                     processing: 89.765,
                     response_pending: 7.0,
                     total: 219.765
                   }
                 },
                 name: "completed_job_with_execution_duration_description"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 89.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 89.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 89.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 89.0
                   },
                   min: %{
                     order_pending: 89.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 89.0
                   }
                 },
                 name: "ttl_expired_job"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 123.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 123.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 123.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 123.0
                   },
                   min: %{
                     order_pending: 123.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 123.0
                   }
                 },
                 name: "just_started_job"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 0.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 0.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 0.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 0.0
                   },
                   min: %{
                     order_pending: 0.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 0.0
                   }
                 },
                 name: "not_started_job"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 123.0,
                     processing: 90.0,
                     response_pending: 6.0,
                     total: 219.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 123.0,
                     processing: 90.0,
                     response_pending: 6.0,
                     total: 219.0
                   },
                   min: %{
                     order_pending: 123.0,
                     processing: 90.0,
                     response_pending: 6.0,
                     total: 219.0
                   }
                 },
                 name: "completed_job"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 123.0,
                     processing: 76.0,
                     response_pending: 8.0,
                     total: 207.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 123.0,
                     processing: 76.0,
                     response_pending: 8.0,
                     total: 207.0
                   },
                   min: %{
                     order_pending: 123.0,
                     processing: 76.0,
                     response_pending: 8.0,
                     total: 207.0
                   }
                 },
                 name: "failed_job"
               },
               %{
                 durations: %{
                   average: %{
                     order_pending: 0.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 0.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 0.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 0.0
                   },
                   min: %{
                     order_pending: 0.0,
                     processing: 0.0,
                     response_pending: 0.0,
                     total: 0.0
                   }
                 },
                 name: "skipped_job"
               }
             ])

    assert statistics.page == 0
    assert statistics.size == 10
    assert statistics.total == 8
  end

  test "get workflows duration statistics empty" do
    role_fixture(@administrator)

    statistics = Durations.get_workflows_duration_statistics(%{"roles" => ["administrator"]})

    assert statistics == %{
             data: [],
             page: 0,
             size: 10,
             total: 0
           }
  end

  test "get workflows duration statistics" do
    role_fixture(@administrator)

    workflow = populate_with_jobs_with_durations()

    statistics =
      Durations.get_workflows_duration_statistics(%{
        "roles" => ["administrator"],
        "ids" => [Integer.to_string(workflow.not_started_job.workflow_id)]
      })

    assert statistics == %{
             data: [
               %{
                 durations: %{
                   average: %{
                     order_pending: 704.0,
                     processing: 315.765,
                     response_pending: 21.0,
                     total: 419.0
                   },
                   count: 1,
                   max: %{
                     order_pending: 704.0,
                     processing: 315.765,
                     response_pending: 21.0,
                     total: 419.0
                   },
                   min: %{
                     order_pending: 704.0,
                     processing: 315.765,
                     response_pending: 21.0,
                     total: 419.0
                   }
                 },
                 name: "id",
                 version: "6.5.4"
               }
             ],
             page: 0,
             size: 10,
             total: 1
           }
  end

  @tag capture_log: true
  test "duplicated job duration" do
    role_fixture(@administrator)

    {_, workflow} = Workflows.create_workflow(@workflow)

    start_time = NaiveDateTime.add(DateTime.utc_now(), -1800, :second)
    inserted_at = NaiveDateTime.add(start_time, 23, :second)
    completed_job = add_completed_job(workflow, inserted_at, 123, 90)

    {_, job_duration} = JobsDurations.set_job_durations(completed_job.id)

    duration = %{
      total_duration: job_duration.total_duration,
      response_pending_duration: job_duration.response_pending_duration,
      processing_duration: job_duration.processing_duration,
      order_pending_duration: job_duration.order_pending_duration,
      job_end: job_duration.job_end,
      job_start: job_duration.job_start,
      job_id: job_duration.job_id
    }

    JobsDurations.create_job_durations(duration)

    {_, _job_duration} = JobsDurations.set_job_durations(completed_job.id)

    # data = Durations.list_durations_for_jobs(%{"job_id" => completed_job.id})
  end

  defp populate_with_jobs_with_durations do
    {_, workflow} = Workflows.create_workflow(@workflow)

    start_time = NaiveDateTime.add(DateTime.utc_now(), -1800, :second)

    # Job that has not started
    not_started_job = add_not_started_job(workflow, start_time)

    # Job that has just started
    inserted_at = NaiveDateTime.add(start_time, 1, :second)
    just_started_job = add_just_started_job(workflow, inserted_at, 123)

    # Job that is processing
    inserted_at = NaiveDateTime.add(start_time, 12, :second)
    processing_job = add_processing_job(workflow, inserted_at, 123, 60)

    # Job that has completed
    inserted_at = NaiveDateTime.add(start_time, 23, :second)
    completed_job = add_completed_job(workflow, inserted_at, 123, 90)

    # Job that has completed with the execution duration set into status descriptions
    inserted_at = NaiveDateTime.add(start_time, 30, :second)

    completed_job_with_execution_duration =
      add_completed_job_with_execution_duration_description(
        workflow,
        inserted_at,
        123,
        90,
        89.765
      )

    # Job that has failed
    inserted_at = NaiveDateTime.add(start_time, 220, :second)
    failed_job = add_failed_job(workflow, inserted_at, 123, 76)

    # Job message TTL expired
    inserted_at = NaiveDateTime.add(start_time, 250, :second)
    expired_job = add_expired_job(workflow, inserted_at, 89)

    # Job that is queued or was skipped
    inserted_at = NaiveDateTime.add(start_time, 110, :second)
    skipped_job = add_skipped_job(workflow, inserted_at)

    %{
      not_started_job: not_started_job,
      just_started_job: just_started_job,
      processing_job: processing_job,
      completed_job: completed_job,
      completed_job_with_execution_duration: completed_job_with_execution_duration,
      failed_job: failed_job,
      expired_job: expired_job,
      skipped_job: skipped_job
    }
  end

  defp insert_job(workflow, job_name, inserted_at) do
    {_, job} =
      Jobs.create_job(%{
        name: job_name,
        step_id: 0,
        workflow_id: workflow.id,
        parameters: []
      })

    query = from(j in Job, where: j.id == ^job.id)

    query
    |> Repo.update_all(
      set: [
        inserted_at: inserted_at
      ]
    )

    query
    |> Repo.update_all(
      set: [
        updated_at: inserted_at
      ]
    )

    job
  end

  defp add_not_started_job(workflow, inserted_at) do
    job = insert_job(workflow, "not_started_job", inserted_at)
    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_queued_job(workflow, job_name, inserted_at) do
    job = insert_job(workflow, job_name, inserted_at)

    insert_status_for_job(job.id, :queued, inserted_at)

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_just_started_job(workflow, inserted_at, time_before_job_starts) do
    job = add_queued_job(workflow, "just_started_job", inserted_at)

    add_status_for_job(job.id, "processing", inserted_at, time_before_job_starts)

    add_progression_for_job(job.id, inserted_at, time_before_job_starts, 0)

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_processing_job(workflow, inserted_at, time_before_job_starts, time_since_job_started) do
    job = add_queued_job(workflow, "processing_job", inserted_at)

    add_status_for_job(job.id, "processing", inserted_at, time_before_job_starts)

    add_progression_for_job(job.id, inserted_at, time_before_job_starts, 0)

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + time_since_job_started,
      20
    )

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_completed_job(workflow, inserted_at, time_before_job_starts, time_since_job_started) do
    job = add_queued_job(workflow, "completed_job", inserted_at)

    response_pending = 6

    add_status_for_job(job.id, "processing", inserted_at, time_before_job_starts)

    add_progression_for_job(job.id, inserted_at, time_before_job_starts, 0)

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + round(time_since_job_started / 2),
      50
    )

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + time_since_job_started,
      100
    )

    add_status_for_job(
      job.id,
      "completed",
      inserted_at,
      time_before_job_starts + time_since_job_started + response_pending
    )

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_completed_job_with_execution_duration_description(
         workflow,
         inserted_at,
         time_before_job_starts,
         time_since_job_started,
         execution_duration
       ) do
    job =
      add_queued_job(workflow, "completed_job_with_execution_duration_description", inserted_at)

    response_pending = 7

    add_status_for_job(job.id, "processing", inserted_at, time_before_job_starts)

    add_progression_for_job(job.id, inserted_at, time_before_job_starts, 0)

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + round(time_since_job_started / 2),
      50
    )

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + time_since_job_started,
      100
    )

    add_status_for_job(
      job.id,
      "completed",
      inserted_at,
      time_before_job_starts + time_since_job_started + response_pending,
      %{"execution_duration" => execution_duration}
    )

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_failed_job(workflow, inserted_at, time_before_job_starts, time_since_job_started) do
    job = add_queued_job(workflow, "failed_job", inserted_at)

    response_pending = 8

    add_status_for_job(job.id, "processing", inserted_at, time_before_job_starts)

    add_progression_for_job(job.id, inserted_at, time_before_job_starts, 0)

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + round(time_since_job_started / 2),
      20
    )

    add_progression_for_job(
      job.id,
      inserted_at,
      time_before_job_starts + time_since_job_started,
      40
    )

    add_status_for_job(
      job.id,
      "error",
      inserted_at,
      time_before_job_starts + time_since_job_started + response_pending
    )

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_expired_job(workflow, inserted_at, ttl_duration) do
    job = add_queued_job(workflow, "ttl_expired_job", inserted_at)

    add_status_for_job(job.id, "processing", inserted_at, 0)
    add_status_for_job(job.id, "error", inserted_at, ttl_duration)

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  defp add_skipped_job(workflow, inserted_at) do
    # same case as for queued status
    job = add_queued_job(workflow, "skipped_job", inserted_at)

    Status.set_job_status(job.id, "skipped")

    {_, _job_duration} = JobsDurations.set_job_durations(job.id)

    job
  end

  def insert_status_for_job(job_id, state, %NaiveDateTime{} = datetime, description \\ %{}) do
    {:ok, status} = Status.set_job_status(job_id, state, description)

    query = from(s in Status, where: s.id == ^status.id)

    query
    |> Repo.update_all(
      set: [
        inserted_at: datetime,
        updated_at: datetime
      ]
    )
  end

  def add_status_for_job(job_id, state, start, offset_in_seconds, description \\ %{}) do
    datetime = NaiveDateTime.add(start, offset_in_seconds, :second)

    insert_status_for_job(job_id, state, datetime, description)
  end

  defp add_progression_for_job(job_id, start, offset_in_seconds, progression) do
    datetime = NaiveDateTime.add(start, offset_in_seconds, :second)

    {_, progression} =
      Progressions.create_progression(%{
        datetime: datetime,
        docker_container_id: "container_id",
        progression: progression,
        job_id: job_id
      })

    query = from(p in Progressions.Progression, where: p.id == ^progression.id)

    query
    |> Repo.update_all(
      set: [
        inserted_at: datetime,
        updated_at: datetime
      ]
    )
  end
end
