defmodule StepFlow.HelpersTest do
  use ExUnit.Case
  use Plug.Test

  require Logger
  alias StepFlow.Amqp.Helpers
  alias StepFlow.Controllers.WorkflowDefinitions
  alias StepFlow.Jobs
  alias StepFlow.Jobs.Status
  alias StepFlow.Progressions
  alias StepFlow.Repo
  alias StepFlow.Rights.Right
  alias StepFlow.Roles
  alias StepFlow.Updates
  alias StepFlow.Workflows

  doctest StepFlow.Step.Helpers

  def port_format(port) when is_integer(port) do
    Integer.to_string(port)
  end

  def port_format(port) do
    port
  end

  defp clean_queue(channel, queue) do
    case AMQP.Basic.get(channel, queue) do
      {:ok, _, %{delivery_tag: delivery_tag}} ->
        AMQP.Basic.ack(channel, delivery_tag)
        clean_queue(channel, queue)

      _ ->
        :ok
    end
  end

  def get_amqp_connection do
    url = Helpers.get_amqp_connection_url()
    {:ok, connection} = AMQP.Connection.open(url)
    {:ok, channel} = AMQP.Channel.open(connection)

    AMQP.Queue.declare(channel, "job_test", durable: false)

    AMQP.Queue.bind(channel, "job_test", "job_submit", routing_key: "job_test")

    clean_queue(channel, "job_test")

    AMQP.Queue.declare(channel, "job_ffmpeg", durable: false)

    AMQP.Queue.bind(channel, "job_ffmpeg", "job_submit", routing_key: "job_ffmpeg")

    clean_queue(channel, "job_ffmpeg")

    AMQP.Queue.declare(channel, "job_transfer", durable: false)

    AMQP.Queue.bind(channel, "job_transfer", "job_submit", routing_key: "job_transfer")

    clean_queue(channel, "job_transfer")

    AMQP.Queue.declare(channel, "job_speech_to_text", durable: false)

    AMQP.Queue.bind(channel, "job_speech_to_text", "job_submit", routing_key: "job_speech_to_text")

    clean_queue(channel, "job_speech_to_text")

    AMQP.Queue.declare(channel, "job_file_system", durable: false)

    AMQP.Queue.bind(channel, "job_file_system", "job_submit", routing_key: "job_file_system")

    clean_queue(channel, "job_file_system")

    AMQP.Queue.declare(channel, "job_worker_manager", durable: false)

    AMQP.Queue.bind(channel, "job_worker_manager", "job_submit", routing_key: "job_worker_manager")

    clean_queue(channel, "job_worker_manager")

    AMQP.Queue.declare(channel, "direct_messaging_job_live", durable: false)

    AMQP.Queue.bind(channel, "direct_messaging_job_live", "direct_messaging")

    clean_queue(channel, "direct_messaging_job_live")

    clean_queue(channel, "job_queue_not_found")
    clean_queue(channel, "job_progression")
    clean_queue(channel, "worker_created")
    clean_queue(channel, "worker_initialized")
    clean_queue(channel, "worker_started")
    clean_queue(channel, "worker_status")
    clean_queue(channel, "worker_terminated")
    clean_queue(channel, "worker_updated")

    {connection, channel}
  end

  def close_amqp_connection(conn) do
    AMQP.Connection.close(conn)
  end

  def validate_message_format(%{"job_id" => job_id, "parameters" => parameters})
      when is_integer(job_id) and is_list(parameters) do
    Enum.map(parameters, fn parameter -> validate_parameter(parameter) end)
    |> Enum.filter(fn x -> x == false end)
    |> length == 0
  end

  def validate_message_format(%{job_id: job_id, parameters: parameters})
      when is_integer(job_id) and is_list(parameters) do
    Enum.map(parameters, fn parameter -> validate_parameter(parameter) end)
    |> Enum.filter(fn x -> x == false end)
    |> length == 0
  end

  def validate_message_format(_), do: false

  defp validate_parameter(%{"id" => id, "type" => "string", "value" => value})
       when is_bitstring(id) and is_bitstring(value) do
    true
  end

  defp validate_parameter(%{id: id, type: "string", value: value})
       when is_bitstring(id) and is_bitstring(value) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "credential", "value" => value})
       when is_bitstring(id) and is_bitstring(value) do
    true
  end

  defp validate_parameter(%{id: id, type: "credential", value: value})
       when is_bitstring(id) and is_bitstring(value) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "boolean", "value" => value})
       when is_bitstring(id) and is_boolean(value) do
    true
  end

  defp validate_parameter(%{id: id, type: "boolean", value: value})
       when is_bitstring(id) and is_boolean(value) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "integer", "value" => value})
       when is_bitstring(id) and is_integer(value) do
    true
  end

  defp validate_parameter(%{id: id, type: "integer", value: value})
       when is_bitstring(id) and is_integer(value) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "requirements", "value" => %{"paths" => paths}})
       when is_bitstring(id) and is_list(paths) do
    true
  end

  defp validate_parameter(%{id: id, type: "requirements", value: %{"paths" => paths}})
       when is_bitstring(id) and is_list(paths) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "requirements", "value" => %{}})
       when is_bitstring(id) do
    true
  end

  defp validate_parameter(%{id: id, type: "requirements", value: %{}})
       when is_bitstring(id) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "requirements", "value" => []})
       when is_bitstring(id) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "array_of_strings", "value" => paths})
       when is_bitstring(id) and is_list(paths) do
    true
  end

  defp validate_parameter(%{id: id, type: "array_of_strings", value: paths})
       when is_bitstring(id) and is_list(paths) do
    true
  end

  defp validate_parameter(%{"id" => id, "type" => "array_of_integers", "value" => paths})
       when is_bitstring(id) and is_list(paths) do
    true
  end

  defp validate_parameter(%{id: id, type: "array_of_integers", value: paths})
       when is_bitstring(id) and is_list(paths) do
    true
  end

  defp validate_parameter(%{
         "id" => id,
         "type" => "filter",
         "value" => %{"ends_with" => ends_with}
       })
       when is_bitstring(id) and is_bitstring(ends_with) do
    true
  end

  defp validate_parameter(_) do
    false
  end

  def check(workflow_id, total) do
    all_jobs =
      StepFlow.Jobs.internal_list_jobs(%{
        "workflow_id" => workflow_id |> Integer.to_string(),
        "size" => 50
      })
      |> Map.get(:data)

    assert length(all_jobs) == total
  end

  def check(workflow_id, step_id, total) do
    all_jobs = get_jobs(workflow_id, step_id)

    assert length(all_jobs) == total
  end

  def error_job(job) do
    Status.set_job_status(job.id, :queued)
    Status.set_job_status(job.id, :processing)
    Status.set_job_status(job.id, :error)
  end

  def processing_job(job, add_zero_progression \\ true) do
    Status.set_job_status(job.id, :queued)
    Status.set_job_status(job.id, :processing)

    if add_zero_progression do
      add_zero_progression(job)
    end
  end

  def add_zero_progression(job) do
    StepFlow.Progressions.create_progression(%{
      job_id: job.id,
      datetime: NaiveDateTime.utc_now(),
      docker_container_id: "unknown",
      progression: 0
    })
  end

  def processing_jobs(workflow_id, step_id) do
    all_jobs = get_jobs(workflow_id, step_id)

    for job <- all_jobs do
      processing_job(job)
    end

    all_jobs
  end

  def complete_job(job) do
    processing_job(job)
    Status.set_job_status(job.id, :completed)
  end

  def complete_jobs(workflow_id, step_id) do
    all_jobs = get_jobs(workflow_id, step_id)

    for job <- all_jobs do
      complete_job(job)
    end

    all_jobs
  end

  def get_jobs(workflow_id, step_id) do
    StepFlow.Jobs.list_jobs(%{
      "step_id" => step_id,
      "workflow_id" => workflow_id |> Integer.to_string(),
      "size" => 50,
      "roles" => ["administrator"]
    })
    |> Map.get(:data)
  end

  def set_output_files(workflow_id, step_id, paths) do
    all_jobs =
      StepFlow.Jobs.list_jobs(%{
        "step_id" => step_id,
        "workflow_id" => workflow_id |> Integer.to_string(),
        "size" => 50,
        "roles" => ["administrator"]
      })
      |> Map.get(:data)

    for job <- all_jobs do
      params =
        job.parameters ++
          [
            %{
              "id" => "destination_paths",
              "type" => "array_of_strings",
              "value" => paths
            }
          ]

      StepFlow.Jobs.update_job(job, %{parameters: params})
    end
  end

  def consume_messages(channel, queue, count) do
    list =
      Enum.map(1..count, fn _x ->
        case AMQP.Basic.get(channel, queue) do
          {:ok, payload, %{delivery_tag: delivery_tag}} ->
            AMQP.Basic.ack(channel, delivery_tag)
            assert StepFlow.HelpersTest.validate_message_format(Jason.decode!(payload))
            payload |> Jason.decode!()

          {:empty, %{cluster_id: ""}} ->
            nil
        end
      end)

    {:empty, %{cluster_id: ""}} = AMQP.Basic.get(channel, queue)
    list
  end

  def workflow_fixture(workflow_definition, attrs \\ %{}) do
    json_workflow_definition =
      workflow_definition
      |> Jason.encode!()
      |> Jason.decode!()

    :ok = WorkflowDefinitions.validate(json_workflow_definition)

    {:ok, workflow} =
      attrs
      |> Enum.into(json_workflow_definition)
      |> Workflows.create_workflow()

    workflow
  end

  def admin_role_fixture do
    {:ok, role} =
      Roles.create_role(%{
        name: "administrator",
        rights: [%Right{entity: "workflow::*", action: ["view"]}]
      })

    role
  end

  def create_progression(workflow, step_id, progress \\ 50) do
    workflow_jobs = Repo.preload(workflow, [:jobs]).jobs

    job =
      workflow_jobs
      |> Enum.filter(fn job -> job.step_id == step_id end)
      |> List.first()

    if progress == 0 do
      {:ok, _status} = Status.set_job_status(job.id, "processing")
    end

    Progressions.create_progression(%{
      job_id: job.id,
      datetime: NaiveDateTime.utc_now(),
      docker_container_id: "unknown",
      progression: progress
    })
  end

  def create_update(workflow, step_id, update) do
    workflow_jobs = Repo.preload(workflow, [:jobs]).jobs

    job =
      workflow_jobs
      |> Enum.filter(fn job -> job.step_id == step_id end)
      |> List.first()

    Updates.update_parameters(job, update)
  end

  def change_job_status(workflow, step_id, status) do
    workflow_jobs = Repo.preload(workflow, [:jobs]).jobs

    job =
      workflow_jobs
      |> Enum.filter(fn job -> job.step_id == step_id end)
      |> List.first()

    Status.set_job_status(job.id, status)
  end

  def get_job_count_status(workflow, step_id) do
    jobs =
      Repo.preload(workflow, jobs: [:status, :progressions])
      |> Map.get(:jobs)

    step =
      StepFlow.Map.get_by_key_or_atom(workflow, :steps)
      |> StepFlow.Controllers.Workflows.get_steps_with_status(jobs)
      |> Enum.filter(fn step -> step["id"] == step_id end)
      |> List.first()

    step.jobs
  end

  def get_job_last_status(job_id) do
    Repo.preload(Jobs.get_job(job_id), [:status]).status
    |> StepFlow.Controllers.Jobs.get_last_status()
  end

  def get_job_last_status(workflow, step_id) do
    workflow_jobs = Repo.preload(workflow, [:jobs]).jobs

    job =
      workflow_jobs
      |> Enum.filter(fn job -> job.step_id == step_id end)
      |> List.first()

    case get_job_last_status(job.id) do
      nil -> nil
      status -> status
    end
  end

  def get_job_last_status_id(job_id) do
    Repo.preload(Jobs.get_job(job_id), [:status]).status
    |> StepFlow.Controllers.Jobs.get_last_status_id()
  end

  def get_parameter_value_list(workflow, step_id) do
    StepFlow.Jobs.list_jobs(%{
      "step_id" => step_id,
      "workflow_id" => workflow.id |> Integer.to_string(),
      "size" => 50,
      "roles" => ["administrator"]
    })
    |> Map.get(:data)
    |> List.first()
    |> Map.get(:parameters)
    |> Enum.map(fn x -> x["value"] end)
  end

  def convert_map_string_keys_to_atom(map) when is_map(map) do
    map
    |> Map.new(fn {k, v} ->
      case k do
        k when is_bitstring(k) -> {String.to_atom(k), convert_map_string_keys_to_atom(v)}
        k when is_atom(k) -> {k, convert_map_string_keys_to_atom(v)}
      end
    end)
  end

  def convert_map_string_keys_to_atom(list) when is_list(list) do
    list
    |> Enum.map(fn item -> convert_map_string_keys_to_atom(item) end)
  end

  def convert_map_string_keys_to_atom(obj), do: obj
end
