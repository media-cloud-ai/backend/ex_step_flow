# Jobs State Machine

## Legend

- In blue, states where the state machine can begin.
- In green, states where the state machine can end.
- When a state is in both colours.

## Map

![jobs state machine](./assets/StateMachine-Workflows.svg)
