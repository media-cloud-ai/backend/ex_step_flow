import Config

config :step_flow, StepFlow.Endpoint,
  http: [port: 4000],
  debug_errors: true,
  code_reloader: true,
  check_origin: false,
  pubsub_server: StepFlow.PubSub

config :step_flow, StepFlow.Endpoint,
  live_reload: [
    patterns: [
      ~r{lib/step_flow/.*(ex)$},
      ~r{lib/step_flow/*/.*(ex)$}
    ]
  ],
  reloadable_compilers: [:gettext, :phoenix, :elixir]

# config :step_flow, StepFlow.Repo,
#   migration_source: "some_other_table_for_schema_migrations",
#   migration_repo: AnotherRepoForSchemaMigrations

config :step_flow, StepFlow.Repo,
  hostname: "localhost",
  username: "postgres",
  password: "postgres",
  database: "step_flow_dev",
  runtime_poll_size: 10

config :step_flow, StepFlow.Amqp,
  scheme: {:system, "AMQP_SCHEME"},
  hostname: "localhost",
  port: 5672,
  username: "guest",
  password: "guest",
  virtual_host: "",
  delivery_mode: {:system, "AMQP_DELIVERY_MODE"},
  ssl_cacertfile: {:system, "AMQP_SSL_CACERT_FILE"},
  ssl_certfile: {:system, "AMQP_SSL_CERT_FILE"},
  ssl_keyfile: {:system, "AMQP_SSL_KEY_FILE"},
  ssl_depth: {:system, "AMQP_SSL_DEPTH"},
  ssl_verify: {:system, "AMQP_SSL_VERIFY"},
  ssl_fail_if_no_peer_cert: {:system, "AMQP_SSL_FAIL_IF_NO_PEER_CERT"},
  server_configuration: "standalone"

config :logger, :console, format: "[$level] $message\n"
config :logger, level: :debug

config :step_flow, StepFlow.WorkflowDefinitions.ExternalLoader,
  specification_folder: "/Users/marco/dev/mcai/media-cloud-ai.github.com"

config :open_api_spex, :cache_adapter, OpenApiSpex.Plug.NoneCache

if File.exists?("config/dev.secret.exs") do
  import_config "dev.secret.exs"
end
