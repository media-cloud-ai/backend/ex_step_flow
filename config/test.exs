import Config

config :logger, :console, format: "[$level] $message\n"
config :logger, level: :error

config :plug, :validate_header_keys_during_test, true

config :step_flow, StepFlow,
  workers_work_directory: "/test_work_dir",
  workflow_definition: "./test/definitions",
  automatic_restart_live_workflows: true,
  enable_metrics: true

config :step_flow, StepFlow.Metrics,
  scale: "day",
  delta: -1

config :step_flow, StepFlow.Repo,
  hostname: "postgres",
  port: 5432,
  username: "postgres",
  password: "postgres",
  database: "step_flow_test",
  pool_size: 10,
  pool: Ecto.Adapters.SQL.Sandbox

config :step_flow, StepFlow.Amqp,
  hostname: "rabbitmq",
  port: 5672,
  username: "guest",
  password: "guest",
  virtual_host: "",
  delivery_mode: {:system, "AMQP_DELIVERY_MODE"},
  server_configuration: "standalone"

config :step_flow, StepFlow.Workflows, time_interval: 1

if File.exists?("config/test.secret.exs") do
  import_config "test.secret.exs"
end
